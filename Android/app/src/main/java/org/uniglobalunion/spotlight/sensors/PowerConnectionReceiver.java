package org.uniglobalunion.spotlight.sensors;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.BatteryManager;
import android.os.Build;

import org.uniglobalunion.spotlight.R;
import org.uniglobalunion.spotlight.Utils;
import org.uniglobalunion.spotlight.model.StudyEvent;
import org.uniglobalunion.spotlight.model.StudyRepository;
import org.uniglobalunion.spotlight.service.TrackingService;

import java.util.Date;

/**
 * Created by n8fr8 on 10/31/17.
 */

public class PowerConnectionReceiver extends BroadcastReceiver {


    @Override
    public void onReceive(Context context, Intent intent) {

        // Can't use intent.getIntExtra(BatteryManager.EXTRA_STATUS), as the extra is not provided.
        // The code example at
        // https://developer.android.com/training/monitoring-device-state/battery-monitoring.html
        // is wrong
        // see https://stackoverflow.com/questions/10211609/problems-with-action-power-connected

        // explicitly check the intent action
        // avoids lint issue UnsafeProtectedBroadcastReceiver
        if(intent.getAction() == null) return;
        switch(intent.getAction()){
            case Intent.ACTION_POWER_CONNECTED:
                break;
            case Intent.ACTION_POWER_DISCONNECTED:
                break;
            default:
                return;
        }

        TrackingService.logEvent(context, StudyEvent.EVENT_TYPE_DEVICE_POWER,getBatteryStatus(context));

    }

    //Ref: https://developer.android.com/training/monitoring-device-state/battery-monitoring.html

    private String getBatteryStatus(Context context) {
        IntentFilter ifilter = new IntentFilter(Intent.ACTION_BATTERY_CHANGED);
        Intent batteryStatus = context.registerReceiver(null, ifilter);
        String battStatus;
        int chargePlug = batteryStatus.getIntExtra(BatteryManager.EXTRA_PLUGGED, -1);
        boolean usbCharge = chargePlug == BatteryManager.BATTERY_PLUGGED_USB;
        boolean acCharge = chargePlug == BatteryManager.BATTERY_PLUGGED_AC;
        boolean wirelessCharge = false;

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1)
            wirelessCharge = chargePlug == BatteryManager.BATTERY_PLUGGED_WIRELESS;


        if (usbCharge)
            battStatus = "USB";
        else if (acCharge)
            battStatus = "A/C";
        else if (wirelessCharge)
            battStatus = "Wireless";
        else
            battStatus = "Disconnected";


        return battStatus;
    }
}
