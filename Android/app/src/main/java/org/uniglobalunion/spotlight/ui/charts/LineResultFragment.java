package org.uniglobalunion.spotlight.ui.charts;

import android.content.Context;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.fragment.app.Fragment;

import com.github.mikephil.charting.animation.Easing;
import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;
import com.github.mikephil.charting.data.PieEntry;
import com.github.mikephil.charting.formatter.PercentFormatter;
import com.github.mikephil.charting.highlight.Highlight;
import com.github.mikephil.charting.listener.OnChartValueSelectedListener;
import com.github.mikephil.charting.utils.ColorTemplate;
import com.github.mikephil.charting.utils.MPPointF;

import org.uniglobalunion.spotlight.R;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link LineResultFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link LineResultFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class LineResultFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM_TITLE = "title";
    private static final String ARG_PARAM_LABELS = "labels";
    private static final String ARG_PARAM_VALUES_X = "xvalues";
    private static final String ARG_PARAM_VALUES_Y = "yvalues";


    private OnFragmentInteractionListener mListener;

    private LineChart mChart;
    private String mChartTitle = "Results";
    private String mLabel;
    private long[] mXvals;
    private float[] mYvals;

    public LineResultFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param title Parameter 1.
     * @param label Parameter 2.
     * @param xvalues Parameter 3
     * @param yvalues Parameter 3
     *
     * @return A new instance of fragment PieResultFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static LineResultFragment newInstance(String title, String label, long[] xvalues, float[] yvalues) {
        LineResultFragment fragment = new LineResultFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM_TITLE, title);
        args.putString(ARG_PARAM_LABELS, label);
        args.putLongArray(ARG_PARAM_VALUES_X, xvalues);
        args.putFloatArray(ARG_PARAM_VALUES_Y, yvalues);

        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mChartTitle = getArguments().getString(ARG_PARAM_TITLE);
            mLabel = getArguments().getString(ARG_PARAM_LABELS);
            mXvals = getArguments().getLongArray(ARG_PARAM_VALUES_X);
            mYvals = getArguments().getFloatArray(ARG_PARAM_VALUES_Y);


        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_line_chart_result, container, false);

        mChart = view.findViewById(R.id.chartLine);
        initChart();
        setData(mLabel, mXvals, mYvals);

        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
       //     throw new RuntimeException(context.toString()
         //           + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }

    private void initChart ()
    {

        mChart.getDescription().setEnabled(false);
        mChart.setExtraOffsets(5, 10, 5, 5);

        mChart.setDragDecelerationFrictionCoef(0.95f);

      //  mChart.setCenterTextTypeface(tfLight);
       // mChart.setCenterText(generateCenterSpannableText());

        mChart.setHighlightPerTapEnabled(true);

        // mChart.setUnit(" €");
        // mChart.setDrawUnitsInChart(true);

        // add a selection listener
        mChart.setOnChartValueSelectedListener(new OnChartValueSelectedListener() {
            @Override
            public void onValueSelected(Entry e, Highlight h) {

            }

            @Override
            public void onNothingSelected() {

            }
        });


        mChart.animateY(1400, Easing.EaseInOutQuad);
        // mChart.spin(2000, 0, 360);

        Legend l = mChart.getLegend();
        l.setVerticalAlignment(Legend.LegendVerticalAlignment.TOP);
        l.setHorizontalAlignment(Legend.LegendHorizontalAlignment.RIGHT);
        l.setOrientation(Legend.LegendOrientation.VERTICAL);
        l.setDrawInside(false);
        l.setXEntrySpace(7f);
        l.setYEntrySpace(0f);
        l.setYOffset(0f);

    }

    private void setData(String label, long[] xVal, float[] yVal) {
        ArrayList<Entry> entries = new ArrayList<>();

        // NOTE: The order of the entries when being added to the entries array determines their position around the center of
        // the chart.
        for (int i = 0; i < xVal.length; i++) {
            entries.add(new Entry(xVal[i],yVal[i]));
        }
        LineDataSet dataSet = new LineDataSet(entries, label); // add entries to dataset

        dataSet.setDrawIcons(false);

        // add a lot of colors

        ArrayList<Integer> colors = new ArrayList<>();

        for (int c : ColorTemplate.MATERIAL_COLORS)
            colors.add(c);

        dataSet.setColors(colors);
        //dataSet.setSelectionShift(0f);

        LineData lineData = new LineData(dataSet);
        lineData.setValueTextSize(11f);
        lineData.setValueTextColor(Color.WHITE);
    //    data.setValueTypeface(tfLight);
        mChart.setData(lineData);

        // undo all highlights
        mChart.highlightValues(null);

        mChart.invalidate();
    }
}
