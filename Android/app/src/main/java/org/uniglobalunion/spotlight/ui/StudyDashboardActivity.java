/*
 * Copyright 2017 Google Inc. All Rights Reserved.
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.uniglobalunion.spotlight.ui;

import android.Manifest;
import android.app.AppOpsManager;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.provider.Settings;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewTreeObserver;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.FragmentTransaction;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;
import com.schibstedspain.leku.LocationPickerActivity;

import org.uniglobalunion.spotlight.Constants;
import org.uniglobalunion.spotlight.R;
import org.uniglobalunion.spotlight.SpotlightApp;
import org.uniglobalunion.spotlight.model.StudyEvent;
import org.uniglobalunion.spotlight.model.StudyListing;
import org.uniglobalunion.spotlight.model.StudyViewModel;
import org.uniglobalunion.spotlight.service.TrackingService;
import org.uniglobalunion.spotlight.ui.charts.LineResultFragment;
import org.uniglobalunion.spotlight.ui.charts.PieResultFragment;
import org.uniglobalunion.spotlight.util.UnitLocale;

import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

import static android.app.AppOpsManager.MODE_ALLOWED;
import static android.app.AppOpsManager.OPSTR_GET_USAGE_STATS;
import static android.os.Process.myUid;
import static com.schibstedspain.leku.LocationPickerActivityKt.LATITUDE;
import static com.schibstedspain.leku.LocationPickerActivityKt.LOCATION_ADDRESS;
import static com.schibstedspain.leku.LocationPickerActivityKt.LONGITUDE;
import static com.schibstedspain.leku.LocationPickerActivityKt.ZIPCODE;
import static org.uniglobalunion.spotlight.service.TrackingService.ACTION_ENVIRON_SNAPSHOT;
import static org.uniglobalunion.spotlight.service.TrackingService.getRepository;

public class StudyDashboardActivity extends AppCompatActivity {

    protected static final String TAG = "MainActivity";

    private Context mContext;


    private String studyTitle = "Study";
    private String studyId = "study1";

    private StudyListing studyListing = null;

    private StudyViewModel mStudyViewModel;
    private FragmentAppList mFragApps;

    boolean showActivities = false;
    boolean showEnviron = false;
    boolean showApps = false;
    boolean showGeo = false;
    boolean showGeoFence = false;
    boolean showEvent = false;

    Date todayStart, todayEnd;

    Handler mHandler = new Handler();

    GoogleMap mGoogleMap;
    boolean mapSetup = false;

    @SuppressWarnings("unchecked")
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.study_dashboard_activity);

        if (getIntent().hasExtra("title")) {
            studyTitle = getIntent().getStringExtra("title");
            setTitle(studyTitle);
        }

        if (getIntent().hasExtra("id")) {
            studyId = getIntent().getStringExtra("id");
            studyListing = ((SpotlightApp)getApplication()).getStudy(studyId);
        }


        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.HOUR_OF_DAY, 8);
        cal.set(Calendar.MINUTE, 0);

        todayStart = cal.getTime();

        cal.set(Calendar.HOUR_OF_DAY, 18);
        todayEnd = cal.getTime();



        mContext = this;

        showActivities = getIntent().getBooleanExtra("activity", false);
        showEnviron = getIntent().getBooleanExtra("environ", false);
        showApps = getIntent().getBooleanExtra("apps", false);
        showGeo = getIntent().getBooleanExtra("geo", false);
        showEvent = getIntent().getBooleanExtra("event", false);

        showGeoFence = showApps && showGeo;

        mStudyViewModel = ViewModelProviders.of(this).get(StudyViewModel.class);
        mStudyViewModel.getStudyEvents().observe(this, new Observer<List<StudyEvent>>() {
            @Override
            public void onChanged(@Nullable final List<StudyEvent> studyEvents) {
                // Update the cached copy of the words in the adapter.
                //adapter.setWords(words);
                //updateDetectedActivitiesList(studyEvents);


            }
        });

        initData();

    }

    private void initData () {

        mapSetup = false;

        ((LinearLayout)findViewById(R.id.result_chart_fragment)).removeAllViews();


        CardView cardView = findViewById(R.id.result_chart);
        cardView.setVisibility(showActivities ? View.VISIBLE : View.GONE);

        if (showActivities) {



                getRepository(StudyDashboardActivity.this).getStudyEvents(StudyEvent.EVENT_TYPE_MOTION, todayStart.getTime(), todayEnd.getTime()).observe(this, new Observer<List<StudyEvent>>() {
                    @Override
                    public void onChanged(List<StudyEvent> studyEvents) {

                        long minTotal = TimeUnit.MINUTES.convert(todayEnd.getTime() - todayStart.getTime(), TimeUnit.MILLISECONDS); //total minutes in a day

                        String txtTotalTime = "0 mins";
                        if (studyEvents.size() > 1) {
                            minTotal = studyEvents.get(0).getTimestamp() - studyEvents.get(studyEvents.size() - 1).getTimestamp();
                            minTotal = TimeUnit.MINUTES.convert(minTotal, TimeUnit.MILLISECONDS);
                            txtTotalTime = minTotal + " mins";
                        }

                        int minMoved = 0;
                        long lastTs = 0;

                        for (StudyEvent event: studyEvents)
                        {
                            long diff = lastTs - event.getTimestamp();

                            if (diff > 60000)
                                minMoved++;

                            lastTs = event.getTimestamp();

                        }

                        long minStill = minTotal - minMoved;

                        float percStill = (((float) minStill) / ((float) minTotal));

                        //  CardView cardView = findViewById(R.id.result_chart);
                        String[] labels = {getString(R.string.label_still), getString(R.string.label_moving)};
                        float[] values = {percStill, 1.0f - percStill};
                        PieResultFragment fragPie = PieResultFragment.newInstance("Study Results", labels, values);

                        // Begin the transaction
                        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
                        // Replace the contents of the container with the new fragment
                        ft.add(R.id.result_chart_fragment, fragPie);
                        // or ft.add(R.id.your_placeholder, new FooFragment());
                        // Complete the changes added above
                        ft.commit();
                        //findViewById(R.id.detected_activities).setVisibility(showActivities ? View.VISIBLE : View.GONE);


                    }
                });


        }

        if (showEnviron) {
            findViewById(R.id.detected_environment_action_button).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (checkEnvironPermissions())
                        sendAction(ACTION_ENVIRON_SNAPSHOT);
                }
            });


            getRepository(StudyDashboardActivity.this).getStudyEvents(StudyEvent.EVENT_TYPE_TEMPERATURE, todayStart.getTime(), todayEnd.getTime()).observe(this, new Observer<List<StudyEvent>>() {
                @Override
                public void onChanged(List<StudyEvent> studyEvents) {

                    float value = 0;
                    for (StudyEvent event : studyEvents)
                        value += Float.valueOf(event.getEventValue());

                    if (value > 0) {
                        value = value / studyEvents.size();

                    }
                }
            });

            getRepository(StudyDashboardActivity.this).getStudyEvents(StudyEvent.EVENT_TYPE_LIGHT, todayStart.getTime(), todayEnd.getTime()).observe(this, new Observer<List<StudyEvent>>() {
                @Override
                public void onChanged(List<StudyEvent> studyEvents) {

                    float value = 0;
                    for (StudyEvent event : studyEvents)
                        value += Float.valueOf(event.getEventValue());

                    value = value / studyEvents.size();

                    if (value > 0) {
                        value = value / studyEvents.size();

                     //   tv.setText(value + " lx (illuminance)");
                    } else {

                    }


                }
            });
        }
        else if (showEvent)
        {
            getRepository(StudyDashboardActivity.this).getStudyEvents(StudyEvent.EVENT_TYPE_REPORT, todayStart.getTime(), todayEnd.getTime()).observe(this, new Observer<List<StudyEvent>>() {
                @Override
                public void onChanged(List<StudyEvent> studyEvents) {

                  //  tv.setText(studyEvents.size() + " incidents(s) reported");
                }
            });

        }

        getRepository(StudyDashboardActivity.this).getStudyEvents("environ_sound_max_amplitude", todayStart.getTime(), todayEnd.getTime()).observe(this, new Observer<List<StudyEvent>>() {
            @Override
            public void onChanged(List<StudyEvent> studyEvents) {

                float value = 0;
                for (StudyEvent event : studyEvents)
                    value += Float.valueOf(event.getEventValue());

                value = value / studyEvents.size();

                if (value > 0) {
                    value = value / studyEvents.size();

//                    tv.setText(value + " db");
                }



                String label = "sound";
                long[] xvals = new long[studyEvents.size()];
                float[] yvals = new float[studyEvents.size()];

                int i = 0;
                for (StudyEvent event: studyEvents)
                {
                    xvals[i] = event.getTimestamp();
                    yvals[i] = Float.valueOf(event.getEventValue());
                    i++;
                }

                LineResultFragment fragLine = LineResultFragment.newInstance("Sound", label, xvals, yvals);

                // Begin the transaction
                FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
                // Replace the contents of the container with the new fragment
                ft.add(R.id.result_chart_fragment, fragLine);
                // or ft.add(R.id.your_placeholder, new FooFragment());
                // Complete the changes added above
                ft.commit();


            }
        });

        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.study_status_menu, menu);
        return true;
    }

    private void setupMap ()
    {
        //showApps means commute tracking
        findViewById(R.id.geocontrols).setVisibility(showGeoFence ? View.VISIBLE : View.GONE);

        final SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);

        mapFragment.getView().getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                mapFragment.getMapAsync(new OnMapReadyCallback() {
                    @Override
                    public void onMapReady(final GoogleMap googleMap) {

                        mGoogleMap = googleMap;

                        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(StudyDashboardActivity.this);

                        if (!mapSetup) {
                            if (showGeoFence) {
                                final LatLngBounds.Builder builder = LatLngBounds.builder();

                                String[] geoKeys = {"home", "work"};
                                boolean hasPoints = false;
                                for (String geoKey : geoKeys) {
                                    if (prefs.getFloat("geo-" + geoKey + "-lat", -1) != -1) {
                                        double geoLat = (double) prefs.getFloat("geo-" + geoKey + "-lat", -1);
                                        double geoLong = (double) prefs.getFloat("geo-" + geoKey + "-long", -1);
                                        // Add a marker in Sydney, Australia,
                                        // and move the map's camera to the same location.
                                        LatLng newLoc = new LatLng(geoLat, geoLong);
                                        mGoogleMap.addMarker(new MarkerOptions().position(newLoc).title(geoKey));
                                        builder.include(newLoc);
                                        hasPoints = true;
                                    }
                                }


                                if (hasPoints) {
                                    mHandler.postDelayed(new Runnable() {
                                        public void run() {
                                            mGoogleMap.moveCamera(CameraUpdateFactory.newLatLngBounds(builder.build(), 80));

                                        }
                                    }, 1000);
                                }

                                mapSetup = true;
                            } else if (showGeo) {
                                getRepository(StudyDashboardActivity.this).getStudyEvents(StudyEvent.EVENT_TYPE_GEO_LOGGING, todayStart.getTime(), todayEnd.getTime()).observe(StudyDashboardActivity.this, new Observer<List<StudyEvent>>() {
                                    @Override
                                    public void onChanged(List<StudyEvent> studyEvents) {

                                        if (studyEvents.size() > 0) {

                                            mapSetup = true;
                                            float totalDistanceInMeters = 0;
                                            Location lastLoc = null;

                                            final LatLngBounds.Builder builder = LatLngBounds.builder();
                                            PolylineOptions pLine = new PolylineOptions();

                                            for (StudyEvent event : studyEvents) {
                                                Location location = new Location("");

                                                String[] locParts = event.getEventValue().split(",");
                                                location.setLatitude(Double.parseDouble(locParts[0]));
                                                location.setLongitude(Double.parseDouble(locParts[1]));

                                                String geoKey = new Date(event.getTimestamp()).toString();

                                                // and move the map's camera to the same location.
                                                LatLng newLoc = new LatLng(location.getLatitude(), location.getLongitude());
                                                mGoogleMap.addMarker(new MarkerOptions().position(newLoc).visible(false));
                                                builder.include(newLoc);

                                                pLine.add(newLoc);

                                                if (lastLoc != null) {
                                                    totalDistanceInMeters += lastLoc.distanceTo(location);
                                                }

                                                lastLoc = location;
                                            }

                                            mGoogleMap.addPolyline(pLine);


                                            mHandler.postDelayed(new Runnable() {
                                                public void run() {
                                                    LatLngBounds lBounds = builder.build();

                                                    mGoogleMap.moveCamera(CameraUpdateFactory.newLatLngBounds(lBounds, 80));

                                                    mGoogleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(lBounds.getCenter(), 10f));

                                                }
                                            }, 1000);

                                            String distanceStr = "";

                                            if (UnitLocale.getDefault() == UnitLocale.Metric) {

                                                if (totalDistanceInMeters < 1000)
                                                    distanceStr = ((int) totalDistanceInMeters) + " meters";
                                                else
                                                    distanceStr = ((int) totalDistanceInMeters / 1000) + "km";
                                            }
                                            else
                                            {
                                                double miles = UnitLocale.getDefault().getMiles(totalDistanceInMeters);
                                                distanceStr = Math.round(miles) + " miles";
                                            }

                                            ((TextView) findViewById(R.id.location_info)).setText(distanceStr);
                                        }
                                    }
                                });
                            }

                        }
                    }
                });
            }
        });


    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if (item.getItemId() == android.R.id.home)
        {
            finish();
            return true;
        }
        else if (item.getItemId() == R.id.menu_report) {

            showData();

        }

        return super.onOptionsItemSelected(item);
    }

    private void showData ()
    {

        Intent intent = new Intent(this, StudyResultsActivity.class);
        intent.putExtra("studyId",studyId);

        startActivity(intent);

    }

    private void enableUsageTracking () {

        if (!checkForPermission(this)) {

            startActivity(new Intent(Settings.ACTION_USAGE_ACCESS_SETTINGS));
        }

    }

    @Override
    protected void onResume() {
        super.onResume();

        mapSetup = false;

        if (mFragApps != null)
            mFragApps.reloadApps(this);

        if (showGeo)
            setupMap();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

    }

    /**
     * Registers for activity recognition updates using
     * Registers success and failure callbacks.
     */
    public void startStudy() {
        Intent intent = new Intent(this, TrackingService.class);
        intent.setAction(TrackingService.ACTION_START_TRACKING);
        intent.putExtra("studyid",studyId);
        ContextCompat.startForegroundService(this,intent);
    }

    /**
     * Registers for activity recognition updates using
     * Registers success and failure callbacks.
     */
    public void sendAction(String action) {
        Intent intent = new Intent(this, TrackingService.class);
        intent.setAction(action);
        intent.putExtra("studyid",studyId);
        ContextCompat.startForegroundService(this,intent);
    }


    /**
     * Removes activity recognition updates using
     * failure callbacks.
     */
    public void stopStudy () {

        Intent intent = new Intent(this,TrackingService.class);
        intent.setAction(TrackingService.ACTION_STOP_TRACKING);
        intent.putExtra("studyid",studyId);
        startService(intent);

    }

    public void resetStudy ()
    {

    }



    /**
     * Retrieves the boolean from SharedPreferences that tracks whether we are requesting activity
     * updates.
     */
    private boolean isStudyEnabled() {
        return PreferenceManager.getDefaultSharedPreferences(this)
                .getBoolean(Constants.KEY_ACTIVITY_UPDATES_REQUESTED + "_" + studyId, false);
    }

    private void setStudyEnabled(boolean enabled) {
        PreferenceManager.getDefaultSharedPreferences(this).edit()
                .putBoolean(Constants.KEY_ACTIVITY_UPDATES_REQUESTED + "_" + studyId, enabled).commit();

        studyListing.isEnabled = enabled;

        if (studyListing.isEnabled && studyListing.trackApps)
        {
            enableUsageTracking();
        }
    }


    private boolean checkForPermission(Context context) {
        AppOpsManager appOps = (AppOpsManager) context.getSystemService(Context.APP_OPS_SERVICE);
        int mode = appOps.checkOpNoThrow(OPSTR_GET_USAGE_STATS,myUid(), context.getPackageName());
        return mode == MODE_ALLOWED;
    }

    public void viewDataClicked (View view)
    {
        showData();
    }

    public void setHomeClicked (View view)
    {
      //  Intent intent = new Intent(this, TrackingService.class);
      //  intent.setAction(TrackingService.ACTION_SET_GEO_HOME);
       // startService(intent);
        initLocationPicker (MAP_HOME_REQUEST_CODE);
    }


    public void setWorkClicked (View view)
    {
        //Intent intent = new Intent(this, TrackingService.class);
        //intent.setAction(TrackingService.ACTION_SET_GEO_WORK);
        //startService(intent);
        initLocationPicker (MAP_WORK_REQUEST_CODE);
    }

    public void chooseAppsClicked (View view)
    {
        startActivityForResult(new Intent(this,AppManagerActivity.class),APPS_REQUEST_CODE);
    }

    private int MAP_HOME_REQUEST_CODE = 1001;
    private int MAP_WORK_REQUEST_CODE = MAP_HOME_REQUEST_CODE+1;
    private int APPS_REQUEST_CODE = MAP_WORK_REQUEST_CODE+1;


    private void initLocationPicker (int requestCode)
    {
        Intent locationPickerIntent = new LocationPickerActivity.Builder()
                .withDefaultLocaleSearchZone()
                .shouldReturnOkOnBackPressed()
                .withSatelliteViewHidden()
                .withGooglePlacesEnabled()
                .withGoogleTimeZoneEnabled()
                .withVoiceSearchHidden()
                .withUnnamedRoadHidden()
                .build(this);


        startActivityForResult(locationPickerIntent, requestCode);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == RESULT_OK)
        {
            if (requestCode == MAP_HOME_REQUEST_CODE || requestCode == MAP_WORK_REQUEST_CODE) {
                double latitude = data.getDoubleExtra(LATITUDE, 0.0);
                double longitude = data.getDoubleExtra(LONGITUDE, 0.0);
                String address = data.getStringExtra(LOCATION_ADDRESS);
                String postalcode = data.getStringExtra(ZIPCODE);

                String geoKey = "home";
                if (requestCode == MAP_WORK_REQUEST_CODE)
                    geoKey = "work";

                SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);

                prefs
                        .edit()
                        .putFloat("geo-" + geoKey + "-lat", (float) latitude)
                        .putFloat("geo-" + geoKey + "-long", (float) longitude)
                        .apply();

            }
            else if (requestCode == APPS_REQUEST_CODE)
            {
                if (mFragApps != null)
                    mFragApps.reloadApps(this);
            }

        }
    }

    private int REQUEST_RECORD_AUDIO = 1234;

    private boolean checkEnvironPermissions ()
    {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.RECORD_AUDIO) != PackageManager.PERMISSION_GRANTED) {

            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.RECORD_AUDIO},
                    REQUEST_RECORD_AUDIO);

            return false;
        }
        else
            return true;
    }

    private boolean checkLocationPermissions ()
    {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                    REQUEST_RECORD_AUDIO);

            return false;
        }
        else
            return true;
    }
}