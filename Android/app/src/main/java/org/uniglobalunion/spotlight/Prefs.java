package org.uniglobalunion.spotlight;

import android.content.Context;
import android.preference.PreferenceManager;

public class Prefs {

    public static float getGeoAccuracyFilter (Context context)
    {

        float geoAccuracyFilter = 30f;

        try {
            geoAccuracyFilter = Float.parseFloat(
                    PreferenceManager.getDefaultSharedPreferences(context).getString("pref_geo_accuracy_filter", "30.0"));
        }
        catch (Exception e){}

        return geoAccuracyFilter;
    }
}
