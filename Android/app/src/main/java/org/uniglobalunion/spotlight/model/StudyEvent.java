package org.uniglobalunion.spotlight.model;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;
import androidx.room.TypeConverters;

@Entity(tableName = "study_event_table")
@TypeConverters({Converters.class})
public class StudyEvent {

    @PrimaryKey(autoGenerate = true)
    private int id;

    @NonNull
    @ColumnInfo(name = "study_id")
    private String mStudyId;

    @NonNull
    @ColumnInfo(name = "study_timestamp")
    private long mTimestamp;

    @NonNull
    @ColumnInfo(name = "study_event_value")
    private String mEventValue;

    public StudyEvent(@NonNull String studyId, @NonNull long timestamp, @NonNull String eventValue) {
        this.mStudyId = studyId;
        this.mTimestamp = timestamp;
        this.mEventValue = eventValue;
    }

    public String getStudyId(){return this.mStudyId;}
    public long getTimestamp(){return this.mTimestamp;}
    public String getEventValue(){return this.mEventValue;}
    public int getId () { return this.id; }


    public void setId(int id) {
        this.id = id;
    }

    public final static String EVENT_TYPE_TEMPERATURE = "environ_temp";
    public final static String EVENT_TYPE_GEO_LOGGING = "geo_logging";
    public final static String EVENT_TYPE_LIGHT = "environ_light";

    public final static String EVENT_TYPE_HUMIDITY = "environ_relative_humidity";
    public final static String EVENT_TYPE_SOUND = "environ_sound_max_amplitude";
    public final static String EVENT_TYPE_GEO_FENCE = "geo_fence";
    public final static String EVENT_TYPE_SCREEN = "screen";
    public final static String EVENT_TYPE_DEVICE_POWER = "device_power";

    public final static String EVENT_TYPE_ACTION_STOP = "study_stop";
    public final static String EVENT_TYPE_ACTION_START = "study_start";

    public final static String EVENT_TYPE_MOTION = "motion";
    public final static String EVENT_TYPE_STEPS = "steps";
    public final static String EVENT_TYPE_COMMUTE = "commute";
    public final static String EVENT_TYPE_TIME = "time";


    public final static String EVENT_TYPE_REPORT = "event";
    public final static String EVENT_TYPE_APP_USAGE = "app_usage";

    public final static String EVENT_TYPE_INSIGHT = "insight";




}
