package org.uniglobalunion.spotlight.service;

import android.app.IntentService;
import android.content.Context;
import android.content.Intent;
import android.text.TextUtils;
import android.util.Log;


import com.google.android.gms.location.Geofence;
import com.google.android.gms.location.GeofencingEvent;

import org.uniglobalunion.spotlight.model.StudyEvent;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static org.uniglobalunion.spotlight.service.TrackingService.timeExitHome;
import static org.uniglobalunion.spotlight.service.TrackingService.timeExitWork;

public class GeofenceTransitionsIntentService extends IntentService {

    private final static String TAG = "GeoF";

    public final static String GEOKEY_HOME = "home";
    public final static String GEOKEY_WORK = "work";

    /**
     * Creates an IntentService.  Invoked by your subclass's constructor.
     *
     * @param name Used to name the worker thread, important only for debugging.
     */
    public GeofenceTransitionsIntentService(String name) {
        super(name);
    }

    public GeofenceTransitionsIntentService() {
        super("Geofence Intent Service");
    }


    protected void onHandleIntent(Intent intent) {
        GeofencingEvent geofencingEvent = GeofencingEvent.fromIntent(intent);
        if (geofencingEvent.hasError()) {
            String errorMessage = "geof error: " + geofencingEvent.getErrorCode();
            Log.e(TAG, errorMessage);
            return;
        }

        // Get the transition type.
        int geofenceTransition = geofencingEvent.getGeofenceTransition();

        // Test that the reported transition was of interest.
        if (geofenceTransition == Geofence.GEOFENCE_TRANSITION_ENTER ||
                geofenceTransition == Geofence.GEOFENCE_TRANSITION_EXIT) {

            // Get the geofences that were triggered. A single event can trigger
            // multiple geofences.
            List<Geofence> triggeringGeofences = geofencingEvent.getTriggeringGeofences();

             // Get the transition details as a String.
             String geofenceTransitionDetails = getGeofenceTransitionDetails(
             geofenceTransition,
             triggeringGeofences
             );

             TrackingService.logEvent(this,StudyEvent.EVENT_TYPE_GEO_FENCE,geofenceTransitionDetails);

            for (Geofence geofence : triggeringGeofences) {

                //set last at home
                if (geofence.getRequestId().equals(GEOKEY_HOME)) {
                    if (geofenceTransition == Geofence.GEOFENCE_TRANSITION_EXIT) {
                        timeExitHome = new Date();
                        TrackingService.atHome = false;
                    }
                    else if (geofenceTransition == Geofence.GEOFENCE_TRANSITION_ENTER)
                    {
                        TrackingService.atHome = true;

                        if (timeExitWork != null) {
                            //if we now have work, and lastAtHome is set, then log a commute
                            addCommute(getApplicationContext(),timeExitWork,new Date());

                            //now remove lastAtHome since we are no longer there
                            timeExitWork = null;
                        }
                    }

                } else if (geofence.getRequestId().equals(GEOKEY_WORK)) {

                    if (geofenceTransition == Geofence.GEOFENCE_TRANSITION_ENTER) {

                        if (timeExitHome != null) {
                            //if we now have work, and lastAtHome is set, then log a commute
                            addCommute(getApplicationContext(),timeExitHome,timeExitWork);

                            //now remove lastAtHome since we are no longer there
                            timeExitHome = null;
                        }

                        TrackingService.atWork = true;
                    }
                    else if (geofenceTransition == Geofence.GEOFENCE_TRANSITION_EXIT) {
                        TrackingService.atWork = false;
                        timeExitWork = new Date();
                    }

                }
            }


        } else {
            // Log the error.
            Log.e(TAG, "Invalid transition: " +
                    geofenceTransition);
        }
    }

    private void addCommute (Context context, Date startTime, Date endTime)
    {
        long commuteTime = endTime.getTime() - startTime.getTime();
        TrackingService.logEvent(context, StudyEvent.EVENT_TYPE_COMMUTE,commuteTime+"");
    }

    /**
     * Gets transition details and returns them as a formatted string.
     *
     * @param geofenceTransition    The ID of the geofence transition.
     * @param triggeringGeofences   The geofence(s) triggered.
     * @return                      The transition details formatted as String.
     */
    private String getGeofenceTransitionDetails(
            int geofenceTransition,
            List<Geofence> triggeringGeofences) {

        String geofenceTransitionString = getTransitionString(geofenceTransition);

        // Get the Ids of each geofence that was triggered.
        ArrayList<String> triggeringGeofencesIdsList = new ArrayList<>();
        for (Geofence geofence : triggeringGeofences) {
            triggeringGeofencesIdsList.add(geofence.getRequestId());
        }
        String triggeringGeofencesIdsString = TextUtils.join(", ",  triggeringGeofencesIdsList);

        return geofenceTransitionString + ": " + triggeringGeofencesIdsString;
    }


    /**
     * Maps geofence transition types to their human-readable equivalents.
     *
     * @param transitionType    A transition type constant defined in Geofence
     * @return                  A String indicating the type of transition
     */
    private String getTransitionString(int transitionType) {
        switch (transitionType) {
            case Geofence.GEOFENCE_TRANSITION_ENTER:
                return GEOFENCE_ENTER;
            case Geofence.GEOFENCE_TRANSITION_EXIT:
                return GEOFENCE_EXIT;
            default:
                return GEOFENCE_UNKNOWN;
        }
    }


    public final static String GEOFENCE_ENTER = "geof-entered";
    public final static String GEOFENCE_EXIT = "geof-exit";
    public final static String GEOFENCE_UNKNOWN = "geof-unknown";



    /**
     *
     <string name="geofence_transition_entered">geof-entered</string>
     <string name="geofence_transition_exited">geof-exit</string>
     <string name="unknown_geofence_transition">geof-unknown</string>
     */
}

