package org.uniglobalunion.spotlight.service;

import android.Manifest;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.location.Location;
import android.os.BatteryManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.PowerManager;
import android.preference.PreferenceManager;
import android.text.TextUtils;
import android.util.Log;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.core.app.NotificationCompat;
import androidx.core.content.ContextCompat;
import androidx.lifecycle.Lifecycle;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.LifecycleService;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.Geofence;
import com.google.android.gms.location.GeofencingClient;
import com.google.android.gms.location.GeofencingRequest;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.rvalerio.fgchecker.AppChecker;

import org.uniglobalunion.spotlight.BuildConfig;
import org.uniglobalunion.spotlight.Constants;
import org.uniglobalunion.spotlight.R;
import org.uniglobalunion.spotlight.SpotlightApp;
import org.uniglobalunion.spotlight.model.Study;
import org.uniglobalunion.spotlight.model.StudyEvent;
import org.uniglobalunion.spotlight.model.StudyListing;
import org.uniglobalunion.spotlight.model.StudyRepository;
import org.uniglobalunion.spotlight.model.StudySummary;
import org.uniglobalunion.spotlight.security.PreferencesSettings;
import org.uniglobalunion.spotlight.sensors.AccelerometerMonitor;
import org.uniglobalunion.spotlight.sensors.AppUsageMonitor;
import org.uniglobalunion.spotlight.sensors.PowerConnectionReceiver;
import org.uniglobalunion.spotlight.sensors.SoundMonitor;
import org.uniglobalunion.spotlight.sensors.StepCountMonitor;
import org.uniglobalunion.spotlight.sensors.TimeTrackingMonitor;
import org.uniglobalunion.spotlight.ui.InsightsActivity;
import org.uniglobalunion.spotlight.ui.LoadingActivity;
import org.uniglobalunion.spotlight.ui.StudyPickerActivity;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Map;
import java.util.StringTokenizer;

import static org.uniglobalunion.spotlight.Constants.PREFS_KEY_APP_ENABLED;
import static org.uniglobalunion.spotlight.Constants.PREF_END_TIME;
import static org.uniglobalunion.spotlight.Constants.PREF_HIGHRES_LOCATION;
import static org.uniglobalunion.spotlight.Constants.PREF_LIMIT_LOG_TIME;
import static org.uniglobalunion.spotlight.Constants.PREF_START_TIME;
import static org.uniglobalunion.spotlight.Constants.PREF_WAYPOINT_SIZE;
import static org.uniglobalunion.spotlight.model.StudyEvent.EVENT_TYPE_APP_USAGE;
import static org.uniglobalunion.spotlight.model.StudyEvent.EVENT_TYPE_COMMUTE;
import static org.uniglobalunion.spotlight.model.StudyEvent.EVENT_TYPE_INSIGHT;
import static org.uniglobalunion.spotlight.model.StudyEvent.EVENT_TYPE_SCREEN;
import static org.uniglobalunion.spotlight.service.GeofenceTransitionsIntentService.GEOFENCE_ENTER;
import static org.uniglobalunion.spotlight.service.GeofenceTransitionsIntentService.GEOFENCE_EXIT;
import static org.uniglobalunion.spotlight.service.GeofenceTransitionsIntentService.GEOKEY_HOME;
import static org.uniglobalunion.spotlight.service.GeofenceTransitionsIntentService.GEOKEY_WORK;

public class TrackingService extends Service implements SensorEventListener {

    public final static String ACTION_START_TRACKING = "starttrack";
    public final static String ACTION_STOP_TRACKING = "stoptrack";
    public final static String ACTION_SET_GEO_HOME = "setgeo-home";
    public final static String ACTION_SET_GEO_WORK = "setgeo-work";
    public final static String ACTION_ENVIRON_SNAPSHOT = "environ.snapshot";
    public final static String ACTION_APP_SNAPSHOT = "app.snapshot";
    public final static String ACTION_UPDATE_PREFS = "updateprefs";
    public final static String ACTION_START_ENABLED_STUDIES = "startenabledstudies";

    public final static String EXTRA_STUDY_ID = "studyid";

    public final static String INTENT_UPDATE_ACTION = "org.uniglobalunion.spotlight.ACTION_UPDATE";

    public final static String FILE_AUTHORITY = BuildConfig.APPLICATION_ID  + ".fileprovider";

//    public final static String KEY_END_OF_DAY = "isEndOfDay";
    public final static String KEY_INSIGHTS = "isInsightTime";
    public final static String KEY_TIME_TRACKING = "timeTrackingOn";

    private ScreenStateReceiver mScreenStateReceiver;

    private PowerConnectionReceiver mPowerReceiver;

    private PendingIntent mGeofencePendingIntent;

    private Location locationHome;
    private Location locationWork;
    public static Date timeExitHome = null;
    public static Date timeExitWork = null;

    public static boolean atWork = false, atHome = false;

    private GeofencingClient mGeofClient;
    private ArrayList<Geofence> mGeoList;

    private AccelerometerMonitor mAccel;
    private StepCountMonitor mSteps;
    private TimeTrackingMonitor mTimeTracker;

    private final static String TAG = "TrackSrv";

    private final static String CHANNEL_ID_FOREGROUND = "spotlight1";
    private final static String CHANNEL_ID_INSIGHTS = "spotlight-insights";

    private String mLastStudyId = "unknown";

    private Handler mHandler = new Handler();

    private SpotlightApp mApp;

    private static int startHour = 0, startMin = 0, endHour = 0, endMin = 0;
    private int mWaypointDistance = 100;

    private SharedPreferences mPrefs;

    private boolean mIsForeground = false;

    @Override
    public void onCreate() {
        super.onCreate();

        mApp = (SpotlightApp)getApplication();

        mPrefs = PreferenceManager.getDefaultSharedPreferences(this);

        createNotificationChannel();

        startEnabledStudies ();
        mPrefs.registerOnSharedPreferenceChangeListener((sharedPreferences, key) -> updatePrefs(sharedPreferences));

        updatePrefs(mPrefs);

    }

    private void startEnabledStudies ()
    {
        Map<String,?> prefs = mPrefs.getAll();

        for (String key : prefs.keySet())
        {
            if (key.startsWith(Constants.KEY_ACTIVITY_UPDATES_REQUESTED))
            {
                if (mPrefs.getBoolean(key,false)) {
                    String studyId = key.substring(key.lastIndexOf("_")+1);
                    StudyListing studyListing = mApp.getStudy(studyId);
                    if (studyListing != null) {
                        studyListing.isEnabled = true;
                        startTracking(studyListing);
                    }
                }
            }
        }

    }

    /**
    PowerManager.WakeLock wakeLock;

    private void startWakelock ()
    {
        PowerManager powerManager = (PowerManager) getSystemService(POWER_SERVICE);
        wakeLock = powerManager.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK,
                "MyApp::MyWakelockTag");
        wakeLock.acquire();


    }**/

    @Override
    public void onDestroy() {
        super.onDestroy();

        stopAllTracking();

      //  if (wakeLock != null)
        //    wakeLock.release();
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    private void createNotificationChannel() {
        // Create the NotificationChannel, but only on API 26+ because
        // the NotificationChannel class is new and not in the support library
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            CharSequence name = getString(R.string.app_name) + ' ' + getString(R.string.title_monitor);
            String description = getString(R.string.channel_description);
            NotificationChannel channel = new NotificationChannel(CHANNEL_ID_FOREGROUND, name, NotificationManager.IMPORTANCE_DEFAULT);
            channel.setDescription(description);
            channel.setVibrationPattern(new long[]{0});
            // Register the channel with the system; you can't change the importance
            // or other notification behaviors after this
            NotificationManager notificationManager = getSystemService(NotificationManager.class);
            notificationManager.createNotificationChannel(channel);

            name = getString(R.string.app_name) + ' ' + getString(R.string.insight_notification);
            description = getString(R.string.channel_description);
            channel = new NotificationChannel(CHANNEL_ID_INSIGHTS, name, NotificationManager.IMPORTANCE_DEFAULT);
            channel.setDescription(description);
                        // Register the channel with the system; you can't change the importance
            // or other notification behaviors after this
                        notificationManager.createNotificationChannel(channel);
        }
    }



    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        super.onStartCommand(intent, flags, startId);
        doForeground();

        StudyListing studyListing = null;

        if (intent != null) {
            if (intent.hasExtra(EXTRA_STUDY_ID))
                mLastStudyId = intent.getStringExtra(EXTRA_STUDY_ID);

            if (!TextUtils.isEmpty(mLastStudyId))
                studyListing = mApp.getStudy(mLastStudyId);

            if (intent.getAction() != null) {
                if (intent.getAction().equals(ACTION_START_TRACKING)) {
                    startTracking(studyListing);

                } else if (intent.getAction().equals(ACTION_STOP_TRACKING)) {
                    if (studyListing != null)
                        stopTracking(studyListing);

                } else if (intent.getAction().equals(ACTION_ENVIRON_SNAPSHOT)) {
                    startTracking(studyListing);
                    doEnvironSnapshot();
                } else if (intent.getAction().equals(ACTION_APP_SNAPSHOT)) {
                    startTracking(studyListing);
                    startAppForegroundCheck();
                } else if (intent.getAction().equals(ACTION_UPDATE_PREFS)) {

                    updatePrefs(PreferenceManager.getDefaultSharedPreferences(this));
                }
                else if (intent.getAction().equals(KEY_INSIGHTS)) {

                    generateDailyInsightsAsync();

                }
                else if (intent.getAction().equals(ACTION_START_ENABLED_STUDIES)) {
                    startEnabledStudies();
                }
            }
        }

        return START_REDELIVER_INTENT;
    }


    private synchronized void doForeground() {

        Intent notificationIntent = new Intent(this, LoadingActivity.class);

        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0,
                notificationIntent, 0);

        Notification notification = new NotificationCompat.Builder(this, CHANNEL_ID_FOREGROUND)
                .setSmallIcon(R.drawable.ic_notifications_active)
                .setContentTitle(getString(R.string.app_name))
                .setContentText(getString(R.string.study_activated))
                .setDefaults(Notification.DEFAULT_LIGHTS)
                //.setVibrate(new long[]{0L}) // Passing null here silently fails
                .setContentIntent(pendingIntent).build();

        startForeground(1337, notification);


    }

    int lastNotificationId = -1;

    private void showNotification (int eventNotifyId, String eventType, String title, String message, Class activityClass, int icon)
    {
        if (activityClass == null)
            activityClass = LoadingActivity.class;

        Intent notificationIntent = new Intent(this, activityClass);

        notificationIntent.putExtra("isNotify",true);
        notificationIntent.putExtra("type",eventType);
        notificationIntent.putExtra("title",title);
        notificationIntent.putExtra("message",message);

        notificationIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK |
            Intent.FLAG_ACTIVITY_SINGLE_TOP);

        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0,
                notificationIntent, PendingIntent.FLAG_UPDATE_CURRENT);

        Notification notification = new NotificationCompat.Builder(this,CHANNEL_ID_INSIGHTS)
                .setSmallIcon(icon)
                .setContentTitle(getString(R.string.app_name) + ": " + title)
                .setContentText(message)
                .setVibrate(new long[]{0L}) // Passing null here silently fails
                .setContentIntent(pendingIntent).build();

        if (eventNotifyId != lastNotificationId) {
            NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
            notificationManager.notify(eventNotifyId, notification);
            lastNotificationId = eventNotifyId;
        }
    }

    private void cancelNotification (int eventNotifyId)
    {

        NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.cancel(eventNotifyId);

    }

    private void updatePrefs (SharedPreferences prefs)
    {

        mWaypointDistance = Integer.parseInt(prefs.getString(PREF_WAYPOINT_SIZE,"30"));

        stopLocationTracking();
        startLocationUpdates();

        String prefStartTime = prefs.getString(PREF_START_TIME, "8:00");
        String prefEndTime = prefs.getString(PREF_END_TIME, "18:00");

        String splitChar = "";
        if (prefStartTime.contains(":"))
            splitChar = ":";
        else if (prefStartTime.contains("."))
            splitChar = "\\.";

        if (splitChar.length() > 0) {

            try {
                String[] startParts = prefStartTime.split(splitChar);
                if (startParts.length >= 2) {
                    startHour = (Integer.parseInt(startParts[0]));
                    startMin = (Integer.parseInt(startParts[1]));
                } else {
                    startHour = Integer.parseInt(startParts[0]);
                    startMin = 0;
                }
            } catch (Exception e) {
                startHour = 8;
                startMin = 0;
            }

            try {
                String[] endParts = prefEndTime.split(splitChar);
                if (endParts.length >= 2) {
                    endHour = (Integer.parseInt(endParts[0]));
                    endMin = (Integer.parseInt(endParts[1]));
                } else {
                    endHour = (Integer.parseInt(endParts[0]));
                }
            } catch (Exception e) {
                endHour = 18;
                endMin = 0;
            }
        } else {
            startHour = 0;
            startMin = 1;

            endHour = 23;
            endMin = 59;
        }

        if (endHour < startHour) {
            endHour = startHour + 8;
        }

        if (startHour == 0 && startMin == 0)
            startMin = 1;

        if (startHour >= 24) {
            startHour = 0;
            startMin = 1;
        }

        if (endHour >= 24) {
            endHour = 23;
            endMin = 59;
        }


        mApp.updateAlarms();
    }

    private void startTracking(StudyListing studyListing) {

        if (studyListing == null)
            return;

        logEvent(StudyEvent.EVENT_TYPE_ACTION_START, studyListing.id + ": " + studyListing.name);

        if (studyListing.trackGeo)
            startGeofencing();

        if (studyListing.trackEnviron)
            startPowerTracking();

        if (studyListing.trackActivities) {
            startAccel();
            startSteps();
        }

        if (studyListing.trackGeo)
            startLocationUpdates();

        if (studyListing.trackApps) {
            startAppForegroundCheck();
            detectScreenState();
        }

        if (studyListing.trackTime) {

            boolean timeTrackingOn = PreferencesSettings.getPrefBool(this,Constants.TIME_TRACKING_STATE);

            if (timeTrackingOn)
                startTimeTracking();

        }
    }


    private void stopTracking(StudyListing studyListing) {

        if (mLastStudyId != null) {
            logEvent(StudyEvent.EVENT_TYPE_ACTION_STOP, mLastStudyId);

        }

        if (studyListing != null) {
            if (studyListing.trackGeo)
                stopGeofencing();

            if (studyListing.trackEnviron)
                stopPowerTracking();

            if (studyListing.trackActivities) {
                stopAccel();
                stopSteps();
            }

            if (studyListing.trackGeo)
                stopLocationTracking();

            if (studyListing.trackApps) {
                stopAppForegroundCheck();
                stopDetectScreenState();
            }

            if (studyListing.trackTime) {
                stopTimeTracking();
            }
        }
    }

    private void stopAllTracking () {

        //stop all
        stopGeofencing();

        stopPowerTracking();

        stopAccel();
        stopPowerTracking();
        stopDetectScreenState();
        stopSteps();

        stopLocationTracking();
        stopAppForegroundCheck();

        stopTimeTracking();

    }

    private void stopLocationTracking ()
    {
        if (mFusedLocationClient != null && mLocationCallback != null)
            mFusedLocationClient.removeLocationUpdates(mLocationCallback);

        mFusedLocationClient = null;
        mLocationCallback = null;
        locationRequest = null;

        if (wakeLock != null && wakeLock.isHeld()) {
            wakeLock.release();
            wakeLock = null;
        }
    }



    private void detectScreenState() {
        IntentFilter intentFilter = new IntentFilter(Intent.ACTION_SCREEN_ON);
        intentFilter.addAction(Intent.ACTION_SCREEN_OFF);
        mScreenStateReceiver = new ScreenStateReceiver();
        registerReceiver(mScreenStateReceiver, intentFilter);
    }

    private void stopDetectScreenState () {

        if (mScreenStateReceiver != null) {
            unregisterReceiver(mScreenStateReceiver);
            mScreenStateReceiver = null;
        }
    }

    private AppChecker mAppChecker = null;


    private synchronized void startAppForegroundCheck () {

        if (mAppChecker == null) {
            mAppChecker = new AppChecker();

            final int timeCheck = 30000;

            mAppChecker.whenAny(new AppChecker.Listener() {
                @Override
                public void onForeground(String packageName) {

                    String tordAppString = mPrefs.getString(PREFS_KEY_APP_ENABLED, "");

                    if (packageName != null
                            && tordAppString != null
                            && tordAppString.contains(packageName))
                    {
                        long now = new Date().getTime();

                        StringBuffer sb = new StringBuffer();
                        sb.append(packageName).append(',');
                        sb.append(timeCheck).append(',');
                        sb.append(now).append(',');
                        sb.append(now).append(',');
                        sb.append(now);

                        logEvent(StudyEvent.EVENT_TYPE_APP_USAGE, sb.toString());


                      //  showNotification("App used: " + packageName,"You just used the app " + packageName,null);


                    }
                }
            }).timeout(timeCheck).start(this);
        }

    }

    private synchronized void stopAppForegroundCheck () {

        if (mAppChecker != null) {
            mAppChecker.stop();
            mAppChecker = null;
        }
    }


    class ScreenStateReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            Date now = new Date();

            String action = intent.getAction();
            if (Intent.ACTION_SCREEN_ON.equals(action)) {
                //code

                logEvent(context, StudyEvent.EVENT_TYPE_SCREEN, "on");

                startAppForegroundCheck();

            } else if (Intent.ACTION_SCREEN_OFF.equals(action)) {
                //code
                logEvent(context, StudyEvent.EVENT_TYPE_SCREEN, "off");

                stopAppForegroundCheck();
            }

        }
    }

    /**
    public static boolean timeInBounds (Date now)
    {
        int timeNow = (now.getHours()*100)+now.getMinutes();
        int timeStart = (startHour *100)+startMin;
        int timeEnd = (endHour * 100)+endMin;
        return timeNow >= timeStart && timeNow <= timeEnd;
    }**/

    private PendingIntent getGeofencePendingIntent() {
        // Reuse the PendingIntent if we already have it.
        if (mGeofencePendingIntent != null) {
            return mGeofencePendingIntent;
        }
        Intent intent = new Intent(this, GeofenceTransitionsIntentService.class);
        // We use FLAG_UPDATE_CURRENT so that we get the same pending intent back when
        // calling addGeofences() and removeGeofences().
        mGeofencePendingIntent = PendingIntent.getService(this, 123, intent, PendingIntent.FLAG_UPDATE_CURRENT);
        return mGeofencePendingIntent;
    }


    private FusedLocationProviderClient mFusedLocationClient;
    private LocationRequest locationRequest;
    private LocationCallback mLocationCallback;
    private final static int UPDATE_INTERVAL = 10*1000;
    private final static int FASTEST_INTERVAL = 3000;
    private final static int MINIMUM_DISTANCE = 3;

    PowerManager.WakeLock wakeLock;
    private final static String WAKELOK_TAG = "Spotlight::MyWakelockTag";

    private synchronized void startLocationUpdates() {


        if (ActivityCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                &&  ActivityCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            //Toast.makeText(this, "You need to enable permissions to display location !", Toast.LENGTH_SHORT).show();
            return;
        }

        if (mFusedLocationClient != null && locationRequest != null)
            return; //already setup

        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this);

        locationRequest = new LocationRequest();

        if (mPrefs.getBoolean(PREF_HIGHRES_LOCATION,true))
        {

            locationRequest.setInterval(UPDATE_INTERVAL);
             locationRequest.setFastestInterval(FASTEST_INTERVAL);
             locationRequest.setSmallestDisplacement(MINIMUM_DISTANCE);
             locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);

             if (wakeLock == null || (!wakeLock.isHeld())) {
                 PowerManager powerManager = (PowerManager) getSystemService(POWER_SERVICE);
                 wakeLock = powerManager.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK,
                         WAKELOK_TAG);
                 wakeLock.acquire();
             }
        }
        else {
            locationRequest.setPriority(LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY);
        }

        mLocationCallback = new LocationCallback() {

            @Override
            public void onLocationResult(LocationResult locationResult) {
                super.onLocationResult(locationResult);
                if (locationResult == null) {
                    return;
                }

                for (Location location : locationResult.getLocations()) {

                    String geoVal = location.getLatitude() + ","
                            + location.getLongitude() + ","
                            + location.getAccuracy() + ","
                            + location.getSpeed() + ","
                            + location.getAltitude() + ","
                            + location.getBearing() + ","
                            + location.getProvider();


                    logEvent(StudyEvent.EVENT_TYPE_GEO_LOGGING,geoVal,location.getTime());

                    if (locationHome != null && locationWork != null) {

                        boolean nearHome = location.distanceTo(locationHome) < mWaypointDistance;
                        boolean nearWork = location.distanceTo(locationWork) < mWaypointDistance;

                        if (nearHome) {
                            if (!atHome) {
                                logEvent(StudyEvent.EVENT_TYPE_GEO_FENCE, GEOFENCE_ENTER + ": home");

                                if (timeExitWork != null) {
                                    long commuteTime = new Date().getTime()-timeExitWork.getTime();
                                    logEvent(StudyEvent.EVENT_TYPE_COMMUTE, commuteTime + "");
                                    timeExitWork = null;
                                }

                                atHome = true;

                            }
                        }
                        else if (atHome)
                        {
                            logEvent(StudyEvent.EVENT_TYPE_GEO_FENCE, GEOFENCE_EXIT + ": home");
                            atHome = false;
                            timeExitHome = new Date();

                        }


                        if (nearWork) {

                            if (!atWork) {
                                //entering work!
                                logEvent(StudyEvent.EVENT_TYPE_GEO_FENCE, GEOFENCE_ENTER + ": work");


                                if (timeExitHome != null) {
                                    long commuteTime = new Date().getTime() - timeExitHome.getTime();
                                    logEvent(StudyEvent.EVENT_TYPE_COMMUTE, commuteTime + "");
                                    timeExitHome = null;
                                }

                                atWork = true;
                            }

                        }
                        else if (atWork)
                        {
                            logEvent(StudyEvent.EVENT_TYPE_GEO_FENCE, GEOFENCE_EXIT + ": work");
                            atWork = false;
                            timeExitWork = new Date();
                        }

                    }

                }

            }
        };

        mFusedLocationClient.requestLocationUpdates(locationRequest, mLocationCallback, null);


    }

    private void startGeofencing () {

        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }

        String geoKey = GEOKEY_HOME;
        if (mPrefs.getFloat("geo-" + geoKey + "-lat", -1) != -1) {

            double geoLatitude = (double) mPrefs.getFloat("geo-" + geoKey + "-lat", -1);
            double geoLong = (double) mPrefs.getFloat("geo-" + geoKey + "-long", -1);

            locationHome = new Location("");
            locationHome.setLatitude(geoLatitude);
            locationHome.setLongitude(geoLong);

            doGeofencing(geoKey, geoLatitude, geoLong, mWaypointDistance);

        }

        geoKey = GEOKEY_WORK;
        if (mPrefs.getFloat("geo-" + geoKey + "-lat", -1) != -1) {
            double geoLatitude = (double) mPrefs.getFloat("geo-" + geoKey + "-lat", -1);
            double geoLong = (double) mPrefs.getFloat("geo-" + geoKey + "-long", -1);

            locationWork = new Location("");
            locationWork.setLatitude(geoLatitude);
            locationWork.setLongitude(geoLong);

            doGeofencing(geoKey, geoLatitude, geoLong, mWaypointDistance);

        }



    }

    public boolean doHighAccuracy ()
    {
        return mPrefs.getBoolean(PREF_HIGHRES_LOCATION,true);
    }

    private void doGeofencing(String geofKey, double geoLat, double geoLong, float geoRad) {

        if (mGeofClient == null) {
            mGeofClient = LocationServices.getGeofencingClient(this);
            mGeoList = new ArrayList<>();
        }

        mGeoList.add(new Geofence.Builder()
                // Set the request ID of the geofence. This is a string to identify this
                // geofence.
                .setRequestId(geofKey)

                .setCircularRegion(
                        geoLat,
                        geoLong,
                        geoRad
                )
                .setExpirationDuration(Geofence.NEVER_EXPIRE)
                .setTransitionTypes(Geofence.GEOFENCE_TRANSITION_ENTER |
                        Geofence.GEOFENCE_TRANSITION_EXIT | Geofence.GEOFENCE_TRANSITION_DWELL)
                .setLoiteringDelay(1 * 60 * 1000) //1 minute for DWELL
                .build());

        GeofencingRequest.Builder builder = new GeofencingRequest.Builder();
        builder.setInitialTrigger(GeofencingRequest.INITIAL_TRIGGER_DWELL);
        builder.addGeofences(mGeoList);
        builder.build();

        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                       return;
        }


        mGeofClient.addGeofences(builder.build(), getGeofencePendingIntent())
                .addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        // Geofences added
                        // ...
                        Log.d(TAG,"geofence added");

                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        // Failed to add geofences
                        Log.d(TAG,"geofence fail");

                    }
                });

    }

    private void stopGeofencing ()
    {
        if (mGeofClient != null)
        mGeofClient.removeGeofences(getGeofencePendingIntent())
                .addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        // Geofences removed
                        // ...
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        // Failed to remove geofences
                        // ...
                    }
                });

    }

    private void startPowerTracking ()
    {
        mPowerReceiver = new PowerConnectionReceiver();
        // register our power status receivers
        IntentFilter powerConnectedFilter = new IntentFilter(Intent.ACTION_POWER_CONNECTED);
        registerReceiver(mPowerReceiver, powerConnectedFilter);

        IntentFilter powerDisconnectedFilter = new IntentFilter(Intent.ACTION_POWER_DISCONNECTED);
        registerReceiver(mPowerReceiver, powerDisconnectedFilter);
    }

    private void stopPowerTracking ()
    {
        if (mPowerReceiver != null) {
            unregisterReceiver(mPowerReceiver);
            mPowerReceiver = null;
        }
    }

    private SensorManager mSensorManager;
    private SoundMonitor mSoundMonitor;

    private void doEnvironSnapshot ()
    {
        if (mSensorManager == null)
            mSensorManager = (SensorManager)getSystemService(SENSOR_SERVICE);


        Sensor sensor = mSensorManager.getDefaultSensor(Sensor.TYPE_AMBIENT_TEMPERATURE); // requires API level 14.
        if (sensor != null)
            mSensorManager.registerListener(this,sensor,SensorManager.SENSOR_DELAY_NORMAL);
        else {
            sensor = mSensorManager.getDefaultSensor(Sensor.TYPE_TEMPERATURE); // requires API level 14.
            if (sensor != null)
                mSensorManager.registerListener(this, sensor, SensorManager.SENSOR_DELAY_NORMAL);
            else
            {
                float battTemp = batteryTemperature(this);
                logEvent(StudyEvent.EVENT_TYPE_TEMPERATURE,battTemp+"");
            }
        }

        sensor = mSensorManager.getDefaultSensor(Sensor.TYPE_LIGHT); // requires API level 14.
        if (sensor != null)
            mSensorManager.registerListener(this,sensor,SensorManager.SENSOR_DELAY_NORMAL);

        sensor = mSensorManager.getDefaultSensor(Sensor.TYPE_RELATIVE_HUMIDITY); // requires API level 14.
        if (sensor != null)
            mSensorManager.registerListener(this,sensor,SensorManager.SENSOR_DELAY_NORMAL);

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.RECORD_AUDIO) == PackageManager.PERMISSION_GRANTED) {

            if (mSoundMonitor == null)
                mSoundMonitor = new SoundMonitor();

            readSoundMonitor();

            mHandler.postDelayed(new Runnable() {
                @Override
                public void run() {


                    mSoundMonitor = null;
                    mSensorManager.unregisterListener(TrackingService.this);
                }
            }, 5000);
        }
    }

    public static float batteryTemperature(Context context)
    {
        Intent intent = context.registerReceiver(null, new IntentFilter(Intent.ACTION_BATTERY_CHANGED));
        float  temp   = ((float) intent.getIntExtra(BatteryManager.EXTRA_TEMPERATURE,0)) / 10;
        return temp;
    }

    /**
    public static boolean logEvent (Context context, String eventType, String eventValue)
    {
        Date now = new Date();
        if (timeInBounds(now))
        {
            StudyEvent studyEvent = new StudyEvent(eventType, now.getTime(), eventValue);
            getRepository(context).insert(studyEvent);
            return true;
        }

        return false;
    }**/

    public boolean logInsight (String eventType, String eventValue)
    {
        return logEvent(this,EVENT_TYPE_INSIGHT,eventValue + "|" + eventType,new Date().getTime());
    }

    public boolean logEvent (String eventType, String eventValue)
    {
        return logEvent(this,eventType,eventValue,new Date().getTime());
    }

    public boolean logEvent (String eventType, String eventValue, long time)
    {
        return logEvent(this,eventType,eventValue, new Date().getTime());
    }

    public static boolean logEvent (Context context, String eventType, String eventValue)
    {
        StudyEvent studyEvent = new StudyEvent(eventType, new Date().getTime(), eventValue);
        getRepository(context).insert(studyEvent);


        return false;
    }

    public static boolean logEvent (Context context, String eventType, String eventValue, long eventTime)
    {
        StudyEvent studyEvent = new StudyEvent(eventType, eventTime, eventValue);
        getRepository(context).insert(studyEvent);


        return false;
    }

    int timeTrackingId = 7777;

    private synchronized void startTimeTracking () {
        if (mTimeTracker == null) {
            mTimeTracker = new TimeTrackingMonitor(this);
            showNotification(timeTrackingId,"time",
                    getString(R.string.time_tracking_notification_title),
                    getString(R.string.time_tracking_notification_detail), StudyPickerActivity.class, R.drawable.ic_notifications_time);

        }

    }

    private void stopTimeTracking () {
        if (mTimeTracker != null)
        {
            mTimeTracker.stop(this);
            mTimeTracker = null;
            cancelNotification(timeTrackingId);

            timeTrackingId++;
        }

    }

    private synchronized void startSteps () {
        if (mSteps == null) {
            mSteps = new StepCountMonitor(this);

        }
    }

    private void stopSteps () {
        if (mSteps != null) {
            mSteps.stop(this);
            mSteps = null;
        }
    }

    private synchronized void startAccel () {
        if (mAccel == null) {
            int sensitivity = 5;
            mAccel = new AccelerometerMonitor(this, sensitivity);

        }
    }

    private void stopAccel () {
        if (mAccel != null) {
            mAccel.stop(this);
            mAccel = null;
        }
    }

    private static StudyRepository sRepository;

    public static synchronized StudyRepository getRepository (Context context)
    {
        if (sRepository == null)
            sRepository = new StudyRepository(context);

        return sRepository;
    }


    private void readSoundMonitor ()
    {
        if (mSoundMonitor != null) {
            double dbLevel = mSoundMonitor.getNoiseLevel();

            if (dbLevel != -1) {
                logEvent(StudyEvent.EVENT_TYPE_SOUND,dbLevel+"");
            }
        }
    }

    @Override
    public void onSensorChanged(SensorEvent event) {

            String eventType = "unknown";

            if (event.sensor.getType() == Sensor.TYPE_AMBIENT_TEMPERATURE) {
                eventType = StudyEvent.EVENT_TYPE_TEMPERATURE;
            } else if (event.sensor.getType() == Sensor.TYPE_TEMPERATURE) {
                eventType = StudyEvent.EVENT_TYPE_TEMPERATURE;
            } else if (event.sensor.getType() == Sensor.TYPE_LIGHT) {
                eventType = StudyEvent.EVENT_TYPE_LIGHT;

            } else if (event.sensor.getType() == Sensor.TYPE_RELATIVE_HUMIDITY) {
                eventType = StudyEvent.EVENT_TYPE_HUMIDITY;
            }

            logEvent(eventType,event.values[0]+"");
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {

    }


    private void generateDailyInsightsAsync ()
    {
        new Thread(new Runnable () {
            public void run ()
            {
                generateDailyInsights();
            }
        }).start();
    }

    private void generateDailyInsights ()
    {
        Calendar cal = Calendar.getInstance();

        cal.add(Calendar.DAY_OF_MONTH,-1);
        cal.set(Calendar.HOUR_OF_DAY, 0);
        cal.set(Calendar.MINUTE, 0);
        Date dateStart = cal.getTime();

        cal.set(Calendar.HOUR_OF_DAY, 23);
        cal.set(Calendar.MINUTE, 59);

        Date dateEnd = cal.getTime();


        StudySummary.getTotalTimeAtOffice(this,dateStart.getTime(),dateEnd.getTime(), new StudySummary.StudySummaryListener(){

            @Override
            public void handleResult(String result) {
            }

            @Override
            public void handleResult(int total) {

                if (total > 0) {

                    if (total < 60)
                        logInsight(StudyEvent.EVENT_TYPE_GEO_LOGGING, getString(R.string.insight_at_work)
                                + " " + total + getString(R.string.min_yesterday));
                    else
                    {
                        int hours = total/60;
                        int minutes = (total-(hours*60));

                        if (hours == 0) {
                            logInsight(StudyEvent.EVENT_TYPE_GEO_LOGGING, getString(R.string.insight_at_work) +
                                    " " + minutes + getString(R.string.min_yesterday));
                        }
                        else if (hours == 1) {
                            logInsight(StudyEvent.EVENT_TYPE_GEO_LOGGING, getString(R.string.insight_at_work) + " "
                                    + hours + getString(R.string.insight_hour) + " " + minutes + getString(R.string.min_yesterday));
                        }
                        else
                        {
                            logInsight(StudyEvent.EVENT_TYPE_GEO_LOGGING, getString(R.string.insight_at_work) + " "
                                    + hours + getString(R.string.insight_hours) + " " + minutes + getString(R.string.min_yesterday));
                        }
                    }

                }

            }
        });

        StudySummary.getTotalScreenOnEventsStatic(this,dateStart.getTime(),dateEnd.getTime(), new StudySummary.StudySummaryListener(){

                      @Override
                      public void handleResult(String result) {
                              logInsight(EVENT_TYPE_SCREEN,getString(R.string.insight_checked_phone) + " " + result + " " + getString(R.string.insight_times_yesterday));
                      }

                              @Override
                      public void handleResult(int total) {

                      }
                  });


        StudySummary.getTotalAppUsageTimeStatic(this,dateStart.getTime(),dateEnd.getTime(), new StudySummary.StudySummaryListener(){

            @Override
            public void handleResult(String result) {
            }

            @Override
            public void handleResult(int total) {

                if (total > 0) {

                    if (total < 60)
                     logInsight(EVENT_TYPE_APP_USAGE, getString(R.string.insight_used_work_apps)  + " " +  total  + " " +  getString(R.string.min_yesterday));
                    else
                    {
                        int hours = total/60;
                        int minutes = (total-(hours*60));

                        logInsight(EVENT_TYPE_APP_USAGE, getString(R.string.insight_used_work_apps) + " " +  hours + getString(R.string.insight_hours) + " " +  minutes + getString(R.string.min_yesterday));

                    }
                }

            }
        });

        StudySummary.getLastCommuteTimeStatic(this,dateStart.getTime(),dateEnd.getTime(), new StudySummary.StudySummaryListener(){

            @Override
            public void handleResult(String result) {
            }

            @Override
            public void handleResult(int total) {

                if (total > 0) {

                    if (total < 60)
                        logInsight(EVENT_TYPE_COMMUTE, getString(R.string.insight_last_commute) + total + getString(R.string.insight_just_mins));
                    else
                    {
                        int hours = total/60;
                        int minutes = (total-(hours*60));

                        logInsight(EVENT_TYPE_COMMUTE, getString(R.string.insight_last_commute) + hours + getString(R.string.insight_hours) + minutes + getString(R.string.insight_just_mins));

                    }

                }

            }
        });




        /**
         * Behavior: Time spent on phone
         * Total screen time. Don’t forget to recharge!
         *
         * Behavior: Checking phone
         * You’ve checked your phone 15 times before arriving to work.
         *
         * You checked your phone 50 times today.
         *
         * You checked your phone 50 times outside of work.
         *
         * Weekly:
         * You checked your phone 120 times outside of work this week. Is work crowding your personal life? You have a right to disconnect.
         *
         *
         * Behavior: Working while commuting
         * Total app time and total commute time. Are you working while commuting? Some workers are paid for this. Are you?
         *
         * Behavior: Long hours / Overtime (if person has a set schedule)
         * You’ve been at work for 12 hrs. When was your last break?
         *
         * You’ve been at work 12 hrs today. Did you know international standards recommend an 11 hr break between shifts?
         */

        //time to share daily insights / nudges!
        showNotification(999,KEY_INSIGHTS,getString(R.string.notify_daily_insight_title),getString(R.string.notify_daily_insight_desc), InsightsActivity.class, R.drawable.ic_notifications_active);


    }

    private void generateWeeklyInsights ()
    {

    }



}
