/*
 * Copyright 2017 Google Inc. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.uniglobalunion.spotlight;

import android.content.Context;
import android.content.res.Resources;

import com.google.android.gms.location.DetectedActivity;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;

/**
 * Constants used in this sample.
 */
public final class Constants {

    private Constants() {}

    private static final String PACKAGE_NAME =
            "org.uniglobalunion.spotlight";

    public static final String KEY_ACTIVITY_UPDATES_REQUESTED = PACKAGE_NAME +
            ".ACTIVITY_UPDATES_REQUESTED";

    public static final String KEY_DETECTED_ACTIVITIES = PACKAGE_NAME + ".DETECTED_ACTIVITIES";
    public static final String KEY_UPDATES_START_TIME = PACKAGE_NAME + ".UPDATES_START";
    public static final String KEY_UPDATES_STOP_TIME = PACKAGE_NAME + ".UPDATES_STOP";
    public static final String KEY_UPDATES_TOTAL_TIME = PACKAGE_NAME + ".UPDATES_TOTAL";

    public static final String PREFS_KEY_APP_ENABLED = PACKAGE_NAME + ".APP_ENABLED";
    /**
     * The desired time between activity detections. Larger values result in fewer activity
     * detections while improving battery life. A value of 0 results in activity detections at the
     * fastest possible rate.
     */
    static final long DETECTION_INTERVAL_IN_MILLISECONDS = 60 * 1000; // once per minute

    static final int CONFIDENCE_THRESHOLD = 65;
    /**
     * List of DetectedActivity types that we monitor in this sample.
     */
    static final int[] MONITORED_ACTIVITIES = {
            DetectedActivity.STILL,
            DetectedActivity.ON_FOOT,
            DetectedActivity.WALKING,
            DetectedActivity.RUNNING,
            DetectedActivity.ON_BICYCLE,
            DetectedActivity.IN_VEHICLE,
            DetectedActivity.TILTING,
            DetectedActivity.UNKNOWN
    };

    public static final String APP_UPDATE_PATH="https://gitlab.com/guardianproject/spotlight/raw/master/Android/app_update.json";

    public static final String PREF_START_TIME = "pref_start_time";
    public static final String PREF_END_TIME = "pref_end_time";
    public static final String PREF_LIMIT_LOG_TIME = "pref_limit_log_time";
    public static final String PREF_WAYPOINT_SIZE = "pref_waypoint_distance";
    public static final String PREF_HIGHRES_LOCATION = "pref_high_res_location";
    public static final String PREF_INSIGHT_TIME = "pref_insight_time";

    public static final String PREF_DAYS_OF_WEEK = "pref_days_of_week";

    public static final String TIME_TRACKING_STATE = "trackTimeOn";
}
