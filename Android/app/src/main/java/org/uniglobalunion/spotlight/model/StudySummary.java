package org.uniglobalunion.spotlight.model;

import android.content.Context;
import android.content.SharedPreferences;
import android.location.Location;
import android.os.AsyncTask;
import android.preference.PreferenceManager;
import android.util.Log;

import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.Observer;

import com.google.android.gms.maps.model.LatLng;

import org.uniglobalunion.spotlight.Prefs;
import org.uniglobalunion.spotlight.R;
import org.uniglobalunion.spotlight.service.TrackingService;
import org.uniglobalunion.spotlight.util.UnitLocale;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.StringTokenizer;
import java.util.concurrent.TimeUnit;
import java.util.prefs.Preferences;

import static org.uniglobalunion.spotlight.Constants.PREFS_KEY_APP_ENABLED;
import static org.uniglobalunion.spotlight.service.TrackingService.getRepository;

public class StudySummary {


    public static void getTotalTimeTracking (Context context, LifecycleOwner owner, long startTime, long endTime, StudySummaryListener listener)
    {

        getRepository(context).getStudyEvents(StudyEvent.EVENT_TYPE_TIME, startTime, endTime).observe(owner, new Observer<List<StudyEvent>>() {
            @Override
            public void onChanged(List<StudyEvent> studyEvents) {

                long totalTimeUsage = 0;
                //   HashMap<String,Long> perAppTime = new HashMap<>();

                for (StudyEvent event : studyEvents) {

                    totalTimeUsage += Long.parseLong(event.getEventValue());


                }


                int hourTotal = (int)TimeUnit.HOURS.convert(totalTimeUsage, TimeUnit.MILLISECONDS);
                long minTotal = TimeUnit.MINUTES.convert(totalTimeUsage, TimeUnit.MILLISECONDS)-TimeUnit.HOURS.toMinutes(TimeUnit.MILLISECONDS.toHours(totalTimeUsage)); //total minutes in a day

                listener.handleResult((int)minTotal);

                String result = hourTotal + " " + context.getString(R.string.insight_hours) + " " + minTotal + " " + context.getString(R.string.label_mins);

                listener.handleResult(result);
            }
        });
    }

    public static void getTotalAppUsageTime (Context context, LifecycleOwner owner, long startTime, long endTime, StudySummaryListener listener)
    {

        String tordAppString = PreferenceManager.getDefaultSharedPreferences(context)
                .getString(PREFS_KEY_APP_ENABLED, "");
        StringTokenizer st = new StringTokenizer(tordAppString,"|");
        ArrayList<String> appPkgList = new ArrayList<>();
        while (st.hasMoreTokens())
            appPkgList.add(st.nextToken());

        getRepository(context).getStudyEvents(StudyEvent.EVENT_TYPE_APP_USAGE, startTime, endTime).observe(owner, new Observer<List<StudyEvent>>() {
            @Override
            public void onChanged(List<StudyEvent> studyEvents) {

                long totalTimeUsage = 0;
             //   HashMap<String,Long> perAppTime = new HashMap<>();

                for (StudyEvent event : studyEvents) {

                    StringTokenizer st = new StringTokenizer(event.getEventValue(),",");
                    String appPkg = st.nextToken();

                    if (!appPkgList.contains(appPkg))
                        continue;

                    long totalTime = -1;
                    long lastTimeUsed = -1;
                    long startTime = -1;
                    long endTime = -1;

                    if (st.hasMoreTokens()) {
                        totalTime = Long.parseLong(st.nextToken());
                        lastTimeUsed = Long.parseLong(st.nextToken());
                        startTime = Long.parseLong(st.nextToken());
                        endTime = Long.parseLong(st.nextToken());
                    }
                    else
                    {
                        totalTime = 30000;
                        lastTimeUsed = event.getTimestamp();
                        startTime = event.getTimestamp()-totalTime;
                        endTime = lastTimeUsed;
                    }

                    if (lastTimeUsed < startTime || lastTimeUsed > endTime )
                        continue;

                    totalTimeUsage += totalTime;


                }


                int minTotal = (int)TimeUnit.MINUTES.convert(totalTimeUsage, TimeUnit.MILLISECONDS); //total minutes in a day

                listener.handleResult(minTotal);

                String result = minTotal + " " + context.getString(R.string.label_mins);

                listener.handleResult(result);
            }
        });
    }



    public static void getTotalAppUsageTimeStatic (Context context, final long startTimeQuery, final long endTimeQuery, final StudySummaryListener listener)
    {

        new AsyncTask<Void, Void, Long>() {

            @Override
            protected void onPostExecute(Long totalTimeUsage) {
                super.onPostExecute(totalTimeUsage);

                int minTotal = (int)TimeUnit.MINUTES.convert(totalTimeUsage, TimeUnit.MILLISECONDS); //total minutes in a day

                listener.handleResult(minTotal);

                String result = minTotal + " " + context.getString(R.string.label_mins);

                listener.handleResult(result);

            }

            @Override
            protected Long doInBackground(Void... voids) {

                String tordAppString = PreferenceManager.getDefaultSharedPreferences(context)
                        .getString(PREFS_KEY_APP_ENABLED, "");
                StringTokenizer st = new StringTokenizer(tordAppString,"|");
                ArrayList<String> appPkgList = new ArrayList<>();
                while (st.hasMoreTokens())
                    appPkgList.add(st.nextToken());

                List<StudyEvent> studyEvents = getRepository(context).getStudyEventsStatic(StudyEvent.EVENT_TYPE_APP_USAGE, startTimeQuery, endTimeQuery);

                long totalTimeUsage = 0;
                //HashMap<String,Long> perAppTime = new HashMap<>();

                for (StudyEvent event : studyEvents) {

                    st = new StringTokenizer(event.getEventValue(),",");
                    String appPkg = st.nextToken();

                    if (!appPkgList.contains(appPkg))
                        continue;

                    long totalTime = -1;
                    long lastTimeUsed = -1;
                    long startTime = -1;
                    long endTime = -1;

                    if (st.hasMoreTokens()) {
                        totalTime = Long.parseLong(st.nextToken());
                        lastTimeUsed = Long.parseLong(st.nextToken());
                        startTime = Long.parseLong(st.nextToken());
                        endTime = Long.parseLong(st.nextToken());
                    }
                    else
                    {
                        totalTime = 30000;
                        lastTimeUsed = event.getTimestamp();
                        startTime = event.getTimestamp()-totalTime;
                        endTime = lastTimeUsed;
                    }

                    if (lastTimeUsed < startTime || lastTimeUsed > endTime )
                        continue;

                    totalTimeUsage += totalTime;

                }



                return totalTimeUsage;
            }
        }.execute();


    }

    public static void getTotalScreenTime (Context context, LifecycleOwner owner, long startTime, long endTime, StudySummaryListener listener)
    {

        getRepository(context).getStudyEvents(StudyEvent.EVENT_TYPE_SCREEN, startTime, endTime).observe(owner, new Observer<List<StudyEvent>>() {
            @Override
            public void onChanged(List<StudyEvent> studyEvents) {

                long totalTimeUsage = 0;

                long lastScreenOn = -1;

                int totalScreenOnCount = 0;

                for (StudyEvent event : studyEvents) {

                    boolean isScreenOn = event.getEventValue().equals("on");

                    if (isScreenOn) {
                        lastScreenOn = event.getTimestamp();
                        totalScreenOnCount++;
                    }
                    else
                    {
                        if (lastScreenOn != -1) {
                            totalTimeUsage += lastScreenOn - event.getTimestamp();
                            lastScreenOn = -1;
                        }
                    }

                }


                int minTotal = (int)TimeUnit.MINUTES.convert(totalTimeUsage, TimeUnit.MILLISECONDS); //total minutes in a day

                listener.handleResult(minTotal);

                String result = minTotal + " " + context.getString(R.string.label_mins);

                listener.handleResult(result);
            }
        });
    }

    public static void getTotalScreenTimeStatic (Context context, long startTime, long endTime, StudySummaryListener listener) {

        new AsyncTask<Void, Void, Long>() {

            @Override
            protected Long doInBackground(Void... voids) {

                List<StudyEvent> studyEvents = getRepository(context).getStudyEventsStatic(StudyEvent.EVENT_TYPE_SCREEN, startTime, endTime);

                long totalTimeUsage = 0;

                long lastScreenOff = -1;

                for (StudyEvent event : studyEvents) {

                    if (event.getEventValue().equals("off")) {
                        lastScreenOff = event.getTimestamp();
                    } else if (lastScreenOff != -1) {
                        {
                            long screenOnTime = lastScreenOff - event.getTimestamp();
                          //  Log.d("ScreenTime","screenOnTime: at " + new Date( event.getTimestamp()).toLocaleString() + ": " + screenOnTime + "ms / " + (screenOnTime/1000/60) + "mins");
                            totalTimeUsage += screenOnTime;
                            lastScreenOff = -1;
                        }

                    }
                }

                return totalTimeUsage;
            }

            @Override
            protected void onPostExecute(Long totalTimeUsage) {
                super.onPostExecute(totalTimeUsage);

                int minTotal = (int) TimeUnit.MINUTES.convert(totalTimeUsage, TimeUnit.MILLISECONDS); //total minutes in a day

                listener.handleResult(minTotal);

                String result = minTotal + " " + context.getString(R.string.label_mins);

                listener.handleResult(result);

            }
        }.execute();

    }

    public static void getTotalScreenOnEvents (Context context, LifecycleOwner owner, long startTime, long endTime, StudySummaryListener listener)
    {

        getRepository(context).getStudyEvents(StudyEvent.EVENT_TYPE_SCREEN, startTime, endTime).observe(owner, new Observer<List<StudyEvent>>() {
            @Override
            public void onChanged(List<StudyEvent> studyEvents) {

                int totalScreenOnCount = 0;

                for (StudyEvent event : studyEvents) {

                    boolean isScreenOn = event.getEventValue().equals("on");

                    if (isScreenOn)
                        totalScreenOnCount++;

                }

                listener.handleResult(totalScreenOnCount);

                String result = totalScreenOnCount+"";

                listener.handleResult(result);
            }
        });
    }

    public static void getTotalScreenOnEventsStatic (Context context, long startTime, long endTime, StudySummaryListener listener)
    {





        new AsyncTask<Void, Void, Integer>() {

            @Override
            protected Integer doInBackground(Void... voids) {

                List<StudyEvent> studyEvents = getRepository(context).getStudyEventsStatic(StudyEvent.EVENT_TYPE_SCREEN, startTime, endTime);


                int totalScreenOnCount = 0;

                for (StudyEvent event : studyEvents) {

                    boolean isScreenOn = event.getEventValue().equals("on");

                    if (isScreenOn)
                        totalScreenOnCount++;

                }

                return totalScreenOnCount;
            }

            @Override
            protected void onPostExecute(Integer totalScreenOnCount) {
                super.onPostExecute(totalScreenOnCount);

                listener.handleResult(totalScreenOnCount);

                String result = totalScreenOnCount+"";

                listener.handleResult(result);

            }
        }.execute();
    }

    public static void getTotalMotion (Context context, LifecycleOwner owner, long startTime, long endTime, StudySummaryListener listener)
    {
        ArrayList<String> mStudyIds = new ArrayList<>();
      //  mStudyIds.add(StudyEvent.EVENT_TYPE_MOTION);
        mStudyIds.add(StudyEvent.EVENT_TYPE_STEPS);

        getRepository(context).getStudyEvents(mStudyIds, startTime, endTime).observe(owner, new Observer<List<StudyEvent>>() {
            @Override
            public void onChanged(List<StudyEvent> studyEvents) {


                String label = context.getString(R.string.label_steps);
                long[] xvals = new long[24];
                float[] yvals = new float[24];


                for (int i = 0; i < xvals.length; i++) {
                    xvals[i] = i;
                    yvals[i] = 0;
                }

                for (StudyEvent event : studyEvents) {
                    Date dateEvent = new Date(event.getTimestamp());
                    int idx = dateEvent.getHours();
                    xvals[idx] = idx;
                    yvals[idx] += Float.valueOf(event.getEventValue());
                }

                long minTotal = TimeUnit.MINUTES.convert(endTime - startTime, TimeUnit.MILLISECONDS); //total minutes in a day

                String txtTotalTime = "0 mins";
                if (studyEvents.size() > 1) {
                    minTotal = studyEvents.get(0).getTimestamp() - studyEvents.get(studyEvents.size() - 1).getTimestamp();
                    minTotal = TimeUnit.MINUTES.convert(minTotal, TimeUnit.MILLISECONDS);
                    txtTotalTime = minTotal + " mins";
                }

                int minMoved = 0;
                long lastMin = 0;

                int totalSteps = 0;

                for (StudyEvent event: studyEvents)
                {
                    int newMin = new Date(event.getTimestamp()).getMinutes();

                    if (lastMin != 0 && lastMin != newMin)
                        minMoved++;

                    lastMin = newMin;
                    totalSteps += (int)Float.parseFloat(event.getEventValue());

                }

                String result = minMoved + " " + context.getString(R.string.label_mins) + " " + totalSteps + " " + context.getString(R.string.label_steps);

                listener.handleResult(result);
                listener.handleResult(minMoved);

            }
        });
    }

    public static void getTotalDistance (final Context context, LifecycleOwner owner, long startTime, long endTime, StudySummaryListener listener)
    {


        getRepository(context).getStudyEvents(StudyEvent.EVENT_TYPE_GEO_LOGGING, startTime, endTime).observe(owner, new Observer<List<StudyEvent>>() {
            @Override
            public void onChanged(List<StudyEvent> studyEvents) {


                float totalDistanceInMeters = 0;
                float geoAccuracyFilter = Prefs.getGeoAccuracyFilter(context);

                if (studyEvents.size() > 0) {

                    Location lastLoc = null;


                    for (StudyEvent event : studyEvents) {
                        Location location = new Location("");

                        String[] locParts = event.getEventValue().split(",");
                        location.setLatitude(Double.parseDouble(locParts[0]));
                        location.setLongitude(Double.parseDouble(locParts[1]));

                        float accuracy = Float.parseFloat(locParts[2]);

                        if (accuracy < geoAccuracyFilter) {

                            if (lastLoc != null) {
                                totalDistanceInMeters += lastLoc.distanceTo(location);
                            }

                            lastLoc = location;

                        }

                    }




                }

                String distanceStr;

                if (UnitLocale.getDefault() == UnitLocale.Metric) {
                    if (totalDistanceInMeters < 1000)
                        distanceStr = ((int) totalDistanceInMeters) + " " + context.getString(R.string.label_meters);
                    else
                        distanceStr = ((int) totalDistanceInMeters / 1000) + " " + context.getString(R.string.label_km);
                } else {
                    double miles = UnitLocale.getDefault().getMiles(totalDistanceInMeters);
                    distanceStr = String.format("%.2f", miles) + " " + context.getString(R.string.label_miles);
                    listener.handleResult((int)miles);
                }

                listener.handleResult(distanceStr);

            }
        });

    }

    public static void getLastCommuteTimeGeofence (Context context, LifecycleOwner owner, long startTime, long endTime, StudySummaryListener listener)
    {

        getRepository(context).getStudyEvents(StudyEvent.EVENT_TYPE_GEO_FENCE, startTime, endTime).observe(owner, new Observer<List<StudyEvent>>() {
            @Override
            public void onChanged(List<StudyEvent> studyEvents) {


                long lastHomeTime = 0;
                long lastCommuteTime = 0;

                if (studyEvents.size() > 0) {

                    for (int i = studyEvents.size()-1;i>-1;i--) {

                        StudyEvent event = studyEvents.get(i);
                        if (event.getEventValue().contains("home"))
                            lastHomeTime = event.getTimestamp();

                        if  (event.getEventValue().contains("work")) {
                            lastCommuteTime = event.getTimestamp()-lastHomeTime;

                            if (lastCommuteTime > 0)
                                break;
                        }
                    }


                }

                int lastCommuteTimeMin = (int)TimeUnit.MINUTES.convert(lastCommuteTime, TimeUnit.MILLISECONDS);
                listener.handleResult(lastCommuteTimeMin);
                listener.handleResult(lastCommuteTimeMin + " " + context.getString(R.string.label_mins));

            }
        });

    }

    public static void getTotalTimeAtOffice (Context context, long startTime, long endTime, StudySummaryListener listener)
    {

        final List<StudyEvent> studyEvents = getRepository(context).getStudyEventsStatic(StudyEvent.EVENT_TYPE_GEO_FENCE, startTime, endTime);

        long totalTimeAtWork = 0;
        long lastTimeAtWork = -1;
        boolean isAtWork = false;

        if (studyEvents.size() > 0) {


            for (int i = studyEvents.size()-1;i>-1;i--) {

                StudyEvent event = studyEvents.get(i);

                if  (event.getEventValue().equals("geof-exit: work")) {

                    if (isAtWork)
                    {
                        totalTimeAtWork+=event.getTimestamp()-lastTimeAtWork;
                    }

                    isAtWork = false;
                    lastTimeAtWork = -1;
                }
                else if (event.getEventValue().equals("geof-entered: work")) {
                    if (!isAtWork) {
                        isAtWork = true;
                        lastTimeAtWork = event.getTimestamp();
                    }
                }
            }

        }

        int totalTimeAtWorkMin = (int)TimeUnit.MINUTES.convert(totalTimeAtWork, TimeUnit.MILLISECONDS);
        listener.handleResult(totalTimeAtWorkMin);
        listener.handleResult(totalTimeAtWorkMin + " " + context.getString(R.string.label_mins));

    }

    public static void getAllCommuteTimes (Context context, LifecycleOwner owner, long startTime, long endTime, StudySummaryListener listener)
    {

        getRepository(context).getStudyEvents(StudyEvent.EVENT_TYPE_COMMUTE, startTime, endTime).observe(owner, new Observer<List<StudyEvent>>() {
            @Override
            public void onChanged(List<StudyEvent> studyEvents) {

                DateFormat df = SimpleDateFormat.getTimeInstance(SimpleDateFormat.SHORT);
                StringBuffer sb = new StringBuffer();

                for (StudyEvent event : studyEvents)
                {

                    //get latest commute time
                    long lastCommuteTime = Long.parseLong(event.getEventValue());

                    int lastCommuteTimeMin = (int)TimeUnit.MINUTES.convert(lastCommuteTime, TimeUnit.MILLISECONDS);

                    String commuteTime = df.format(new Date(event.getTimestamp()));
                    sb.append(lastCommuteTimeMin + " " + context.getString(R.string.label_mins));
                    sb.append(" (").append(commuteTime).append(")");
                    sb.append("\n");
                }


                listener.handleResult(sb.toString());

            }
        });

    }

    public static void getLastCommuteTime (Context context, LifecycleOwner owner, long startTime, long endTime, StudySummaryListener listener)
    {

        getRepository(context).getStudyEvents(StudyEvent.EVENT_TYPE_COMMUTE, startTime, endTime).observe(owner, new Observer<List<StudyEvent>>() {
            @Override
            public void onChanged(List<StudyEvent> studyEvents) {


                long lastCommuteTime = 0;

                if (studyEvents.size() > 0) {

                    //get latest commute time
                    lastCommuteTime = Long.parseLong(studyEvents.get(0).getEventValue());
                    /*

                    for (StudyEvent event : studyEvents) {

                        lastCommuteTime += Long.parseLong(event.getEventValue());

                    }

                    lastCommuteTime = lastCommuteTime / studyEvents.size();

                     */

                }

                int lastCommuteTimeMin = (int)TimeUnit.MINUTES.convert(lastCommuteTime, TimeUnit.MILLISECONDS);
                listener.handleResult(lastCommuteTimeMin);
                listener.handleResult(lastCommuteTimeMin + " " + context.getString(R.string.label_mins));

            }
        });

    }

    public static void getLastCommuteTimeStatic (Context context, long startTime, long endTime, StudySummaryListener listener)
    {

        List<StudyEvent> studyEvents = getRepository(context).getStudyEventsStatic(StudyEvent.EVENT_TYPE_COMMUTE, startTime, endTime);

        long lastCommuteTime = 0;

        if (studyEvents.size() > 0) {

            //get latest commute time
            lastCommuteTime = Long.parseLong(studyEvents.get(0).getEventValue());
                    /*

                    for (StudyEvent event : studyEvents) {

                        lastCommuteTime += Long.parseLong(event.getEventValue());

                    }

                    lastCommuteTime = lastCommuteTime / studyEvents.size();

                     */

        }

        int lastCommuteTimeMin = (int)TimeUnit.MINUTES.convert(lastCommuteTime, TimeUnit.MILLISECONDS);
        listener.handleResult(lastCommuteTimeMin);
        listener.handleResult(lastCommuteTimeMin + " " + context.getString(R.string.label_mins));

    }

    public static void getAverageCommuteTime (Context context, LifecycleOwner owner, long startTime, long endTime, StudySummaryListener listener)
    {

        getRepository(context).getStudyEvents(StudyEvent.EVENT_TYPE_COMMUTE, startTime, endTime).observe(owner, new Observer<List<StudyEvent>>() {
            @Override
            public void onChanged(List<StudyEvent> studyEvents) {


                long lastCommuteTime = 0;

                if (studyEvents.size() > 0) {

                    for (StudyEvent event : studyEvents) {

                        lastCommuteTime += Long.parseLong(event.getEventValue());

                    }

                    lastCommuteTime = lastCommuteTime / studyEvents.size();

                }

                int lastCommuteTimeMin = (int)TimeUnit.MINUTES.convert(lastCommuteTime, TimeUnit.MILLISECONDS);
                listener.handleResult(lastCommuteTimeMin);
                listener.handleResult(lastCommuteTimeMin + " " + context.getString(R.string.label_mins));

            }
        });

    }



    /**
    public static void getLastCommuteTime (Context context, LifecycleOwner owner, long startTime, long endTime, StudySummaryListener listener)
    {

        getRepository(context).getStudyEvents(StudyEvent.EVENT_TYPE_GEO_FENCE, startTime, endTime).observe(owner, new Observer<List<StudyEvent>>() {
            @Override
            public void onChanged(List<StudyEvent> studyEvents) {


                long lastCommuteTime = 0;

                if (studyEvents.size() > 0) {

                    Location lastLoc = null;

                    long atHome = -1;
                    long atWork = -1;

                    String keyHome = "home";//"geo-exit: home";
                    String keyWork = "work";//"geo-entered: work";

                    for (StudyEvent event : studyEvents) {

                        if (event.getEventValue().equals(keyHome))
                            atHome = event.getTimestamp();
                        else if (event.getEventValue().equals(keyWork))
                            atWork = event.getTimestamp();

                        if (atHome != -1 && atWork != -1)
                        {

                            lastCommuteTime = Math.abs(atWork - atHome);

                            //commute complete!
                            atHome = -1;
                            atWork = -1;
                            break;
                        }

                    }


                }

                int lastCommuteTimeMin = (int)TimeUnit.MINUTES.convert(lastCommuteTime, TimeUnit.MILLISECONDS);
                listener.handleResult(lastCommuteTimeMin);
                listener.handleResult(lastCommuteTimeMin + " " + context.getString(R.string.label_mins));

            }
        });

    }
**/

    public static void getAverageCommuteTime (Context context, LifecycleOwner owner, StudySummaryListener listener)
    {
        getRepository(context).getStudyEvents(StudyEvent.EVENT_TYPE_GEO_FENCE, new Date(0).getTime(), new Date().getTime()).observe(owner, new Observer<List<StudyEvent>>() {
            @Override
            public void onChanged(List<StudyEvent> studyEvents) {


                long averageCommuteTime = 0;

                if (studyEvents.size() > 0) {

                    Location lastLoc = null;

                    long atHome = -1;
                    long atWork = -1;

                    String keyHome = "home";
                    String keyWork = "work";

                    for (StudyEvent event : studyEvents) {

                        if (event.getEventValue().equals(keyHome))
                            atHome = event.getTimestamp();
                        else if (event.getEventValue().equals(keyWork))
                            atWork = event.getTimestamp();


                        if (atHome != -1 && atWork != -1)
                        {

                            long lastCommuteTime = Math.abs(atWork - atHome);

                            //commute complete!
                            atHome = -1;
                            atWork = -1;

                            if (averageCommuteTime == 0)
                                averageCommuteTime = lastCommuteTime;
                            else
                            {
                                averageCommuteTime = (averageCommuteTime+lastCommuteTime)/2;
                            }

                        }

                    }

                }


                int averageCommuteTimeMin = (int)TimeUnit.MINUTES.convert(averageCommuteTime, TimeUnit.MILLISECONDS);
                listener.handleResult(averageCommuteTimeMin);
                listener.handleResult(averageCommuteTimeMin + " " + context.getString(R.string.label_mins));


            }
        });

    }

    public interface StudySummaryListener {
        public void handleResult (String result);
        public void handleResult (int total);
    }
}
