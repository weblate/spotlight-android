package org.uniglobalunion.spotlight.model;

public  class StudyListing {

    public String name;
    public String desc;
    public String id;
    public String iconAssetPath;
    public boolean trackGeo = false;
    public boolean trackActivities = false;
    public boolean trackApps = false;
    public boolean trackEnviron = false;
    public boolean trackEvent = false;
    public boolean trackCommute = false;
    public boolean trackTime = false;
    public boolean isEnabled = false;

    public int order = 0;

    public StudyListing(String id, String name, String desc, String iconAssetPath, boolean trackGeo, boolean trackActivities, boolean trackApps, boolean trackEnviron, boolean trackEvent, boolean trackCommute, boolean trackTime) {
        this.id = id;
        this.name = name;
        this.desc = desc;
        this.iconAssetPath = iconAssetPath;
        this.trackGeo = trackGeo;
        this.trackActivities = trackActivities;
        this.trackApps = trackApps;
        this.trackEnviron = trackEnviron;
        this.trackEvent = trackEvent;
        this.trackCommute = trackCommute;
        this.trackTime = trackTime;
    }
}