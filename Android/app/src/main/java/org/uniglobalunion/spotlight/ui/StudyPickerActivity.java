package org.uniglobalunion.spotlight.ui;

import static com.schibstedspain.leku.LocationPickerActivityKt.LATITUDE;
import static com.schibstedspain.leku.LocationPickerActivityKt.LOCATION_ADDRESS;
import static com.schibstedspain.leku.LocationPickerActivityKt.LONGITUDE;
import static com.schibstedspain.leku.LocationPickerActivityKt.ZIPCODE;
import static org.uniglobalunion.spotlight.service.TrackingService.ACTION_APP_SNAPSHOT;
import static org.uniglobalunion.spotlight.ui.FragmentStudyStatus.APPS_REQUEST_CODE;
import static org.uniglobalunion.spotlight.ui.FragmentStudyStatus.MAP_HOME_REQUEST_CODE;
import static org.uniglobalunion.spotlight.ui.FragmentStudyStatus.MAP_WORK_REQUEST_CODE;

import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.AssetManager;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;

import com.github.javiersantos.appupdater.AppUpdater;
import com.github.javiersantos.appupdater.enums.UpdateFrom;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Handler;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import org.uniglobalunion.spotlight.Constants;
import org.uniglobalunion.spotlight.R;
import org.uniglobalunion.spotlight.SpotlightApp;
import org.uniglobalunion.spotlight.model.StudyListing;
import org.uniglobalunion.spotlight.model.StudySummary;
import org.uniglobalunion.spotlight.security.LockScreenActivity;
import org.uniglobalunion.spotlight.security.PreferencesSettings;
import org.uniglobalunion.spotlight.sensors.PermissionManager;
import org.uniglobalunion.spotlight.service.TrackingService;

import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.Field;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Timer;

public class StudyPickerActivity extends AppCompatActivity {

    SpotlightApp mApp;
    RecyclerView rv;
    View tvTapToEdit;
    Button btnStartWork;
    boolean trackTimeOn = false;

    boolean enabledStudiesOnly = true;

    Date todayStart,todayEnd;
    Date weekStart, weekEnd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);



        if (PreferencesSettings.getPrefBool(this,"locked"))
        {
            finish();
            lockApp();
        }
        else {
            setContentView(R.layout.activity_study_picker);

            mApp = (SpotlightApp) getApplication();

            rv = findViewById(R.id.rv);
            LinearLayoutManager llm = new LinearLayoutManager(this);
            llm.setOrientation(LinearLayoutManager.VERTICAL);
            rv.setLayoutManager(llm);

            tvTapToEdit = findViewById(R.id.tap_to_edit);
            tvTapToEdit.setOnClickListener(v -> {
                enabledStudiesOnly = !enabledStudiesOnly;
                updateStudies();
            });

            trackTimeOn = PreferencesSettings.getPrefBool(this,Constants.TIME_TRACKING_STATE);

            btnStartWork = findViewById(R.id.btn_start_work);
            updateWorkTrackerButton();

            btnStartWork.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    toggleWorkTracker();

                }
            });

            /**
            AppUpdater appUpdater = new AppUpdater(this)
                    .setUpdateFrom(UpdateFrom.JSON)
                    .setUpdateJSON(Constants.APP_UPDATE_PATH);
            appUpdater.start();
             **/

            PreferencesSettings.saveToPref(this,"serviceRunning",true);

            checkPermissions();

            ((SpotlightApp)getApplication()).startService();
        }

    }

    private int REQUEST_PERMISSION=1001;
    private void checkPermissions ()
    {
        SharedPreferences sPrefs = PreferenceManager.getDefaultSharedPreferences(this);
        Map<String,?> prefMap = PreferenceManager.getDefaultSharedPreferences(this).getAll();

        for (String key : prefMap.keySet())
        {
            if (key.startsWith(Constants.KEY_ACTIVITY_UPDATES_REQUESTED))
            {
                if (sPrefs.getBoolean(key,false)) {
                    String studyId = key.substring(key.lastIndexOf("_")+1);
                    StudyListing studyListing = mApp.getStudy(studyId);
                    if (studyListing != null) {
                        studyListing.isEnabled = true;
                        if (studyListing.trackApps)
                            PermissionManager.enableUsageTracking(this);
                        else if (studyListing.trackGeo)
                            PermissionManager.checkLocationPermissions(this,++REQUEST_PERMISSION);
                        else if (studyListing.trackActivities)
                            PermissionManager.checkStepPermission(this,++REQUEST_PERMISSION);
                    }
                }
            }
        }

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == RESULT_OK)
        {
            ((SpotlightApp)getApplication()).startEnabledStudies();

        }
    }



    @Override
    protected void onResume() {
        super.onResume();
        enabledStudiesOnly = true;

        updateTimeframe();


        updateStudies();
    }

    private void updateTimeframe ()
    {

        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.HOUR_OF_DAY, 0);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND,1);

        todayStart = cal.getTime();

        cal.set(Calendar.HOUR_OF_DAY, 23);
        cal.set(Calendar.MINUTE, 59);
        cal.set(Calendar.SECOND,0);

        todayEnd = cal.getTime();

        cal = Calendar.getInstance();

        cal.set(Calendar.DAY_OF_WEEK, Calendar.SATURDAY);
        cal.set(Calendar.HOUR_OF_DAY, 23);
        cal.set(Calendar.MINUTE, 59);
        cal.set(Calendar.SECOND,0);

        weekEnd = cal.getTime();

        cal.add(Calendar.DAY_OF_YEAR, -7);
        weekStart = cal.getTime();

    }

    private void updateStudies ()
    {
        RVAdapter adapter = new RVAdapter();
        ArrayList<StudyListing> studies = new ArrayList<>(mApp.getStudies(enabledStudiesOnly));

        Collections.sort(studies, new Comparator<StudyListing>() {
            public int compare(StudyListing o1, StudyListing o2) {
                return o1.order - o2.order;
            }
        });

        adapter.setStudies(studies);
        rv.setAdapter(adapter);

        if (studies.size() > 0)
        {
            rv.setVisibility(View.VISIBLE);
            tvTapToEdit.setVisibility(View.GONE);
        }
        else
        {

            rv.setVisibility(View.GONE);
            tvTapToEdit.setVisibility(View.VISIBLE);
        }

        if (enabledStudiesOnly)
        {
            getSupportActionBar().setDisplayHomeAsUpEnabled(false);
            setTitle(R.string.app_name);

        }
        else
        {

            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            setTitle(R.string.menu_edit);
        }

    }

    public void showStudy (String studyId, String title, boolean geo, boolean activity, boolean apps, boolean environ, boolean event, boolean commute, boolean time)
    {
        Intent intent = new Intent(this,StudyExplorerActivity.class);
        intent.putExtra("title",title);
        intent.putExtra("id",studyId);
        intent.putExtra("apps",apps);//track app usage
        intent.putExtra("geo",geo);//track geo location
        intent.putExtra("activity",activity);//track activity (motion)
        intent.putExtra("environ",environ); //track environ
        intent.putExtra("event",event); //track environ
        intent.putExtra("commute",commute); //track environ
        intent.putExtra("time",time); //track environ

        startActivity(intent);
    }



    public class RVAdapter extends RecyclerView.Adapter<RVAdapter.StudyViewHolder>{

        private ArrayList<StudyListing> mStudies;

        public class StudyViewHolder extends RecyclerView.ViewHolder {
            CardView cv;
            TextView studyName;
            TextView studyDesc;
            TextView studyStatus;
            ImageView studyIcon;
            Button studyButton;
            StudyListing studyListing;
            Button studyEnableButton;

            StudyViewHolder(View itemView) {
                super(itemView);
                cv = itemView.findViewById(R.id.cv);
                studyName = itemView.findViewById(R.id.study_name);
                studyDesc = itemView.findViewById(R.id.study_desc);
                studyStatus = itemView.findViewById(R.id.study_status_summary);
                studyIcon = itemView.findViewById(R.id.study_icon);
                studyButton = itemView.findViewById(R.id.study_button);
                studyEnableButton = itemView.findViewById(R.id.study_enable_button);
            }

        }

        final Handler handler = new Handler();

        public RVAdapter () {
            super ();

            Runnable runnable = new Runnable() {

                @Override
                public void run() {
                    try{
                        //do your code here
                        notifyDataSetChanged();
                    }
                    catch (Exception e) {
                        e.printStackTrace();
                    }
                    finally{
                        //also call the same runnable to call it at regular interval
                        handler.postDelayed(this, 60000);
                    }
                }
            };
            handler.postDelayed(runnable, 60000);

        }


        public void setStudies (ArrayList<StudyListing> studies)
        {
            mStudies = studies;
        }

        @Override
        public int getItemCount() {
            return mStudies.size();
        }

        @Override
        public StudyViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
            View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.study_card_row, viewGroup, false);
            final StudyViewHolder pvh = new StudyViewHolder(v);



            pvh.cv.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    showStudy(pvh.studyListing.id,pvh.studyListing.name,pvh.studyListing.trackGeo,pvh.studyListing.trackActivities,pvh.studyListing.trackApps,pvh.studyListing.trackEnviron,pvh.studyListing.trackEvent,pvh.studyListing.trackCommute, pvh.studyListing.trackTime);
                }
            });


            return pvh;
        }

        @Override
        public void onBindViewHolder(final StudyViewHolder studyViewHolder, int i) {

            studyViewHolder.studyListing = mStudies.get(i);
            studyViewHolder.studyName.setText( mStudies.get(i).name);
            studyViewHolder.studyDesc.setText(mStudies.get(i).desc);
            studyViewHolder.studyStatus.setText("");


            if (enabledStudiesOnly)
            {

                studyViewHolder.studyDesc.setText("");
                studyViewHolder.studyStatus.setText("...");

                if (studyViewHolder.studyListing != null && studyViewHolder.studyListing.id != null) {
                    if (studyViewHolder.studyListing.id.equals("study5")) {
                        StudySummary.getLastCommuteTime(StudyPickerActivity.this, StudyPickerActivity.this, todayStart.getTime(), todayEnd.getTime(), new StudySummary.StudySummaryListener() {
                            @Override
                            public void handleResult(String result) {

                                String commuteTime = result;
                                studyViewHolder.studyStatus.setText(commuteTime);

                            }

                            @Override
                            public void handleResult(int total) {
                                //total minutes
                                //gCommute.setValue(total);
                            }
                        });
                    } else if (studyViewHolder.studyListing.id.equals("study1")) {

                        StudySummary.getTotalMotion(StudyPickerActivity.this, StudyPickerActivity.this, todayStart.getTime(), todayEnd.getTime(), new StudySummary.StudySummaryListener() {
                            @Override
                            public void handleResult(String result) {

                                String commuteTime = result;
                                studyViewHolder.studyStatus.setText(commuteTime);

                            }

                            @Override
                            public void handleResult(int total) {
                                //total minutes
                                //gCommute.setValue(total);
                            }
                        });


                    }
                    else if (studyViewHolder.studyListing.id.equals("study6")) {

                        StudySummary.getTotalTimeTracking(StudyPickerActivity.this, StudyPickerActivity.this, weekStart.getTime(), weekEnd.getTime(), new StudySummary.StudySummaryListener() {
                            @Override
                            public void handleResult(String result) {

                                String timeTrackingTotal = result;
                                studyViewHolder.studyStatus.setText(getString(R.string.summary_this_week) + " " + timeTrackingTotal);

                            }

                            @Override
                            public void handleResult(int total) {
                                //total minutes
                                //gCommute.setValue(total);
                            }
                        });


                    }
                    else if (studyViewHolder.studyListing.id.equals("study4")) {

                        StudySummary.getTotalDistance(StudyPickerActivity.this, StudyPickerActivity.this, todayStart.getTime(), todayEnd.getTime(), new StudySummary.StudySummaryListener() {
                            @Override
                            public void handleResult(String result) {

                                String commuteTime = result;
                                studyViewHolder.studyStatus.setText(commuteTime);

                            }

                            @Override
                            public void handleResult(int total) {
                                //total minutes
                                //gCommute.setValue(total);
                            }
                        });


                    }
                    else if (studyViewHolder.studyListing.id.equals("study3")) {

                        StudySummary.getTotalAppUsageTime(StudyPickerActivity.this, StudyPickerActivity.this, weekStart.getTime(), weekEnd.getTime(), new StudySummary.StudySummaryListener() {
                            @Override
                            public void handleResult(String result) {

                                String appTime = result;
                                studyViewHolder.studyStatus.setText(getString(R.string.summary_this_week) + " " + appTime);

                            }

                            @Override
                            public void handleResult(int total) {
                                //total minutes
                                //gCommute.setValue(total);
                            }
                        });



                    }
                }
                else
                {
                    studyViewHolder.studyDesc.setText( mStudies.get(i).desc);
                    studyViewHolder.studyStatus.setText( "");

                }

            }

            if (mStudies.get(i).iconAssetPath.startsWith("R.drawable"))
            {
                int resId = getResources().getIdentifier(mStudies.get(i).iconAssetPath.substring(mStudies.get(i).iconAssetPath.lastIndexOf('.')+1), "drawable", getPackageName());

                if (resId > 0)
                    studyViewHolder.studyIcon.setImageDrawable(getDrawable(resId));
            }
            else
                studyViewHolder.studyIcon.setImageBitmap(getBitmapFromAsset(StudyPickerActivity.this, mStudies.get(i).iconAssetPath));


            studyViewHolder.studyListing =  mStudies.get(i);

            if (!enabledStudiesOnly)
            {
                studyViewHolder.studyEnableButton.setVisibility(View.VISIBLE);
                if (studyViewHolder.studyListing.isEnabled)
                    studyViewHolder.studyEnableButton.setText(getString(R.string.remove));
                else
                    studyViewHolder.studyEnableButton.setText(getString(R.string.add));

                studyViewHolder.studyEnableButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        studyViewHolder.studyListing.isEnabled = !studyViewHolder.studyListing.isEnabled;
                        enableStudy(studyViewHolder.studyListing.id,studyViewHolder.studyListing.isEnabled);
                        updateStudies();

                    }
                });
            }
            else
            {

                studyViewHolder.studyEnableButton.setVisibility(View.GONE);
                studyViewHolder.studyEnableButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        //do nothing
                    }
                });
            }
        }

        @Override
        public void onAttachedToRecyclerView(RecyclerView recyclerView) {
            super.onAttachedToRecyclerView(recyclerView);

        }

    }

    private void enableStudy(String studyId,boolean enabled)
    {

        SharedPreferences mPrefs = PreferenceManager.getDefaultSharedPreferences(this);

        String key = Constants.KEY_ACTIVITY_UPDATES_REQUESTED + "_" + studyId;

        if (mPrefs.getBoolean(key,false)!=enabled) {

            if (!enabled) {
                mPrefs.edit().remove(key).commit();
            } else {
                mPrefs.edit()
                        .putBoolean(key, enabled).commit();
            }

            ((SpotlightApp)getApplication()).getStudy(studyId).isEnabled=enabled;
            updateStudies();
        }

    }

    public static Bitmap getBitmapFromAsset(Context context, String filePath) {

        Bitmap bitmap = null;

        AssetManager assetManager = context.getAssets();

        InputStream istr;

        try {
            istr = assetManager.open(filePath);
            bitmap = BitmapFactory.decodeStream(istr);
        } catch (IOException e) {
            // handle exception
        }

        return bitmap;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.study_picker_menu, menu);
        return true;
    }



    @Override
    public boolean onOptionsItemSelected(MenuItem item) {


        if (item.getItemId() == R.id.menu_settings)
       {
           startActivity(new Intent(this, SettingsActivity.class));
       }
       else  if (item.getItemId() == R.id.menu_data)
       {
           startActivity(new Intent(this, StudyResultsActivity.class));
       }
       else if (item.getItemId() == R.id.menu_exit)
       {
           stopService();
       }
       else if (item.getItemId() == R.id.menu_lock)
       {
           lockApp ();
       }
       else if (item.getItemId() == R.id.menu_notify)
       {
           showInsights();
       }
       else if (item.getItemId() == R.id.menu_edit)
       {
           enabledStudiesOnly = !enabledStudiesOnly;
           updateStudies();
       }
       else if (item.getItemId() == R.id.menu_about)
       {
            openBrowser("https://spotlightproject.gitlab.io");
       }
       else if (item.getItemId() == R.id.menu_privacy)
       {
           openBrowser("https://spotlightproject.gitlab.io/privacy.html");

       }
        else if (item.getItemId() == android.R.id.home)
        {
            enabledStudiesOnly = true;
            updateStudies();
        }

        return super.onOptionsItemSelected(item);
    }

    private void lockApp ()
    {
        finish();
        PreferencesSettings.saveToPref(this,"locked",true);

        Intent intent = new Intent(this, LockScreenActivity.class);
        startActivity(intent);
    }

    private void showInsights ()
    {
        Intent intent = new Intent(this, InsightsActivity.class);
        startActivity(intent);
    }

    private void showDashboard ()
    {
        Intent intent = new Intent(this, OverviewActivity.class);
        startActivity(intent);
    }

    private void stopService ()
    {

        PreferencesSettings.saveToPref(this,"serviceRunning",false);

        Intent intent = new Intent(this, TrackingService.class);
        stopService(intent);
        finish();
    }


    private void openBrowser (String link)
    {
        try {
            Intent myIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(link));
            startActivity(myIntent);
        } catch (ActivityNotFoundException e) {
            Toast.makeText(this, "No application can handle this request."
                    + " Please install a webbrowser",  Toast.LENGTH_LONG).show();
            e.printStackTrace();
        }
    }


    private void toggleWorkTracker ()
    {
        trackTimeOn = !PreferencesSettings.getPrefBool(this,Constants.TIME_TRACKING_STATE);

        updateWorkTrackerButton ();

        PreferencesSettings.saveToPref(this,Constants.TIME_TRACKING_STATE,trackTimeOn);

        String studyId = "study6";

        if (trackTimeOn) {

            enableStudy(studyId, true);

            ((SpotlightApp) getApplication()).startStudy(studyId);
        }
        else
            ((SpotlightApp)getApplication()).stopStudy(studyId);


    }

    private void updateWorkTrackerButton ()
    {
        if (trackTimeOn)
        {
            btnStartWork.setText(R.string.stop_work_tracking);
            btnStartWork.setBackgroundColor(getResources().getColor(R.color.holo_red_dark));
        }
        else
        {
            btnStartWork.setText(R.string.start_work_tracking);
            btnStartWork.setBackgroundColor(getResources().getColor(R.color.time_tracking_off));

        }
    }
}
