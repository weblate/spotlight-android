package org.uniglobalunion.spotlight.ui;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;

import org.uniglobalunion.spotlight.BuildConfig;
import org.uniglobalunion.spotlight.R;
import org.uniglobalunion.spotlight.security.LockScreenActivity;
import org.uniglobalunion.spotlight.security.PreferencesSettings;
import org.uniglobalunion.spotlight.service.TrackingService;

public class LoadingActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_loading);


        if (PreferencesSettings.getPrefBool(this,"locked"))
        {
            finish();
            lockApp();
        }
        else {
            SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);

            if (!prefs.getBoolean("onboarding", false)) {
                startActivity(new Intent(this, OnboardingActivity.class));
            } else {
                try {

                    startActivity(new Intent(this, Class.forName(BuildConfig.MAIN_ACTIVITY)));
                    startService( new Intent(this, TrackingService.class));

                    if (getIntent().hasExtra("isNotify") && getIntent().getBooleanExtra("isNotify",false))
                    {
                        Intent intentNotify = new Intent(this,NotifyInfoActivity.class);
                        intentNotify.putExtra("title",getIntent().getStringExtra("title"));
                        intentNotify.putExtra("message",getIntent().getStringExtra("message"));

                        startActivity(intentNotify);

                    }

                } catch (ClassNotFoundException e) {
                    e.printStackTrace();
                }
            }

            finish();
        }
    }

    private void lockApp ()
    {
        finish();
        Intent intent = new Intent(this, LockScreenActivity.class);
        startActivity(intent);
    }
}
