package org.uniglobalunion.spotlight.ui.charts;

import android.content.Context;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.fragment.app.Fragment;

import com.github.mikephil.charting.animation.Easing;
import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.components.AxisBase;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.formatter.StackedValueFormatter;
import com.github.mikephil.charting.formatter.ValueFormatter;
import com.github.mikephil.charting.interfaces.datasets.IBarDataSet;
import com.github.mikephil.charting.utils.ColorTemplate;

import org.uniglobalunion.spotlight.R;

import java.util.ArrayList;
import java.util.Collection;
import java.util.StringTokenizer;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link BarResultFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link BarResultFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class BarResultFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM_TITLE = "title";
    private static final String ARG_PARAM_LABELS = "labels";
    private static final String ARG_PARAM_VALUES_X = "xvalues";
    private static final String ARG_PARAM_VALUES_Y = "yvalues";
    private static final String ARG_PARAM_VALUES_Y_STACKED = "yvaluestack";
    private static final String ARG_PARAM_VALUES_Y_LABELS = "yvalueslabels";

    private static final String ARG_PARAM_COLORS = "colors";
    private static final String ARG_PARAM_Y_MOD = "ymod";
    private static final String ARG_PARAM_Y_LABEL = "ylab";
    private static final String ARG_PARAM_Y_MAX = "ymax";



    private OnFragmentInteractionListener mListener;

    private BarChart mChart;
    private String mChartTitle = "Results";
    private String mLabel;
    private long[] mXvals;
    private float[][] mYvals;
    private String[] mYLabels;
    private ArrayList<Integer> mColors;

    private float yMod = 1f;
    private String yLab = "";
    private float yMax = -1f;

    public BarResultFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param title Parameter 1.
     * @param label Parameter 2.
     * @param xvalues Parameter 3
     * @param yvalues Parameter 3
     *
     * @return A new instance of fragment PieResultFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static BarResultFragment newInstance(String title, String label, long[] xvalues, float[][] yvalues, String[] ylabels, float yMod, String yLabel, float yMax, ArrayList<Integer> colors) {
        BarResultFragment fragment = new BarResultFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM_TITLE, title);
        args.putString(ARG_PARAM_LABELS, label);
        args.putLongArray(ARG_PARAM_VALUES_X, xvalues);
        args.putSerializable(ARG_PARAM_VALUES_Y_STACKED, yvalues);
        args.putStringArray(ARG_PARAM_VALUES_Y_LABELS,ylabels);
        args.putIntegerArrayList(ARG_PARAM_COLORS, colors);
        args.putFloat(ARG_PARAM_Y_MOD,yMod);
        args.putString(ARG_PARAM_Y_LABEL,yLabel);
        args.putFloat(ARG_PARAM_Y_MAX,yMax);

        fragment.setArguments(args);
        return fragment;
    }

    // TODO: Rename and change types and number of parameters
    public static BarResultFragment newInstance(String title, String label, long[] xvalues, float[] yvalues,  float yMod, String yLabel, float yMax, ArrayList<Integer> colors) {
        BarResultFragment fragment = new BarResultFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM_TITLE, title);
        args.putString(ARG_PARAM_LABELS, label);
        args.putLongArray(ARG_PARAM_VALUES_X, xvalues);
        args.putFloatArray(ARG_PARAM_VALUES_Y, yvalues);
        args.putIntegerArrayList(ARG_PARAM_COLORS, colors);
        args.putFloat(ARG_PARAM_Y_MOD,yMod);
        args.putString(ARG_PARAM_Y_LABEL,yLabel);
        args.putFloat(ARG_PARAM_Y_MAX,yMax);

        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mChartTitle = getArguments().getString(ARG_PARAM_TITLE);
            mLabel = getArguments().getString(ARG_PARAM_LABELS);
            mXvals = getArguments().getLongArray(ARG_PARAM_VALUES_X);


            yMod = getArguments().getFloat(ARG_PARAM_Y_MOD);
            yLab = getArguments().getString(ARG_PARAM_Y_LABEL);
            yMax = getArguments().getFloat(ARG_PARAM_Y_MAX);

            if (getArguments().containsKey(ARG_PARAM_VALUES_Y_STACKED)) {
                mYvals = (float[][])getArguments().getSerializable(ARG_PARAM_VALUES_Y_STACKED);
                mYLabels = getArguments().getStringArray(ARG_PARAM_VALUES_Y_LABELS);

                /**
                mYvals = new float[yVals.length][];
                mYvalLabels = new String[yVals.length][];

                for (int i = 0; i < yVals.length; i++) {
                    StringTokenizer st = new StringTokenizer(yVals[i],",");

                    mYvals[i] = new float[st.countTokens()];
                    mYvalLabels[i] = new String[mYvals[i].length];

                    for (int n = 0; n < mYvals[i].length; n++) {
                        String[] valueToken = st.nextToken().split(":");
                        if (valueToken.length == 1)
                        {
                            mYvalLabels[i][n] = "";
                            mYvals[i][n] = Float.parseFloat(valueToken[0]);
                        }
                        else {
                            mYvalLabels[i][n] = valueToken[0];
                            mYvals[i][n] = Float.parseFloat(valueToken[1]);
                        }
                    }
                }**/

            }
            else if (getArguments().containsKey(ARG_PARAM_VALUES_Y))
            {
                float[] yVals = getArguments().getFloatArray(ARG_PARAM_VALUES_Y);
                mYvals = new float[yVals.length][];

                for (int i = 0; i < yVals.length; i++)
                {
                    mYvals[i] = new float[1];
                    mYvals[i][0] = yVals[i];
                }
            }

            mColors = getArguments().getIntegerArrayList(ARG_PARAM_COLORS);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_bar_chart_result, container, false);

        mChart = view.findViewById(R.id.chartBar);


        initChart(yMod, yLab);
        setData(mLabel, mXvals, mYvals, mYLabels, mColors);

        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
       //     throw new RuntimeException(context.toString()
         //           + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }

    private void initChart (final float yModifier, final String yLabel)
    {


        mChart.getLegend().setEnabled(false);
        mChart.getDescription().setEnabled(false);
        mChart.setExtraOffsets(5, 10, 5, 5);

        mChart.setDragDecelerationFrictionCoef(0.95f);

     //   mChart.setMaxVisibleValueCount(4);
      //  mChart.setCenterTextTypeface(tfLight);
       // mChart.setCenterText(generateCenterSpannableText());

        mChart.setHighlightPerTapEnabled(false);

        mChart.setDrawGridBackground(false);
        mChart.setDrawBorders(false);

        mChart.animateY(1400, Easing.EaseInOutQuad);

        mChart.getAxisLeft().setEnabled(true);
        mChart.getAxisRight().setEnabled(false);

        XAxis xAxis = mChart.getXAxis();

        xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);
        xAxis.setTextSize(10f);
        xAxis.setTextColor(Color.DKGRAY);
        xAxis.setDrawAxisLine(true);
        xAxis.setDrawGridLines(false);
// set a custom value formatter
        xAxis.setValueFormatter(new ValueFormatter() {

            @Override
            public String getAxisLabel(float value, AxisBase axis) {

                int hour = (int)value;
                if (hour == 0)
                    return getString(R.string.label_12am);
                else if (hour == 12)
                    return getString(R.string.label_12pm);
                else if (hour < 12)
                    return hour + getString(R.string.label_am);
                else
                    return (hour-12) + getString(R.string.label_pm);
            }
        });


        if (yMax != -1f)
            mChart.getAxisLeft().setAxisMaximum(yMax);

        mChart.getAxisLeft().setValueFormatter(new ValueFormatter() {

            @Override
            public String getAxisLabel(float value, AxisBase axis) {

                float yVal = Math.round(Math.max(0,value/yModifier));
                if (yVal < 0)
                    yVal = 1;

                return ((int)yVal) + yLabel;
            }
        });

        //mChart.getAxisLeft().setGranularity(60000f);
        //mChart.getAxisLeft().setGranularityEnabled(true); // Required to enable granularity


        Legend l = mChart.getLegend();
        l.setVerticalAlignment(Legend.LegendVerticalAlignment.TOP);
        l.setHorizontalAlignment(Legend.LegendHorizontalAlignment.RIGHT);
        l.setOrientation(Legend.LegendOrientation.VERTICAL);
        l.setDrawInside(true);
        l.setXEntrySpace(7f);
        l.setYEntrySpace(0f);
        l.setYOffset(0f);


    }

    private void setData(String label, long[] xVal, float[][] yVals, String[] yLabels, ArrayList<Integer> colors) {
        ArrayList<BarEntry> entries = new ArrayList<>();

        if (yVals[0].length == 0)
            return;

        // NOTE: The order of the entries when being added to the entries array determines their position around the center of
        // the chart.
        for (int i = 0; i < xVal.length; i++) {

            BarEntry entry = null;

            if (yVals[i].length > 1)
                entry = new BarEntry(xVal[i], yVals[i]);
            else
                entry = new BarEntry(xVal[i], yVals[i][0]);

            entries.add(entry);
        }

        BarDataSet dataSet = new BarDataSet(entries, label); // add entries to dataset

        dataSet.setDrawIcons(false);
        dataSet.setDrawValues(false);
        dataSet.setColors(colors);

        if (yLabels != null && yLabels.length > 0)
            dataSet.setStackLabels(yLabels);

        BarData data = new BarData(dataSet);

        /**
        data.setValueFormatter(new StackedValueFormatter(false, "", 0)
        {

            @Override
            public String getBarStackedLabel(float value, BarEntry entry) {

                if (value == 0)
                    return "";
                else
                    return "";//super.getBarStackedLabel(value, entry);
            }
        });**/


        mChart.setData(data);


        // undo all highlights
        //mChart.highlightValues(null);
        mChart.setDrawValueAboveBar(false);
        mChart.setHighlightFullBarEnabled(false);
        mChart.setFitBars(true);

        mChart.invalidate();
    }
}
