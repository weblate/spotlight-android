package org.uniglobalunion.spotlight

import android.app.Activity
import android.content.Context
import android.content.DialogInterface
import androidx.appcompat.app.AlertDialog
import com.google.android.gms.common.util.IOUtils
import com.maxkeppeler.sheets.info.InfoSheet
import com.maxkeppeler.sheets.input.InputSheet
import com.maxkeppeler.sheets.input.type.InputCheckBox
import com.maxkeppeler.sheets.input.type.InputEditText
import com.maxkeppeler.sheets.input.type.InputRadioButtons
import org.cleaninsights.sdk.*
import java.io.IOException
import java.util.ArrayList

open class CleanInsightsManager {

    val CI_CAMPAIGN = "measurement-accuracy"

    private var mMeasure: CleanInsights? = null
    private var mHasConsent = true

    fun initMeasurement(context : Context) {
        if (mMeasure == null) {
            // Instantiate with configuration and directory to write store to, best in an `Application` subclass.
            try {
                mMeasure = CleanInsights(
                    context.assets.open("cleaninsights.json").reader().readText(),
                    context.filesDir
                )

                mHasConsent = mMeasure!!.isCampaignCurrentlyGranted(CI_CAMPAIGN)

            } catch (e: IOException) {
                e.printStackTrace()
            }
        }
    }

    private fun getConsent(context: Activity, campaignId: String?, viewId: String) {

        var success = mMeasure!!.requestConsent(campaignId!!, object : JavaConsentRequestUi {
            override fun show(
                s: String,
                campaign: Campaign,
                consentRequestUiCompletionHandler: ConsentRequestUiCompletionHandler
            ) {

                InfoSheet().show(context) {
                    title(getString(R.string.ci_title))
                    content(context.getString(R.string.clean_insight_consent_prompt))
                    onNegative(getString(R.string.ci_negative)) {
                        // Handle event
                        consentRequestUiCompletionHandler.completed(false)
                    }
                    onPositive(getString(R.string.ci_confirm)) {
                        // Handle event
                        mHasConsent = true
                        consentRequestUiCompletionHandler.completed(true)
                        mMeasure!!.grant(campaignId)
                        showSurvey(context, viewId)

                    }
                }

            }

            override fun show(
                feature: Feature,
                consentRequestUiCompletionHandler: ConsentRequestUiCompletionHandler
            ) {

            }


        })

        return success
    }

    fun measureView(view: String, campaignId: String?) {
        if (mMeasure != null && mHasConsent) {
            val alPath = ArrayList<String>()
            alPath.add(view)
            mMeasure!!.measureVisit(alPath, campaignId!!)
            mMeasure!!.persist()
        }
    }

    fun measureEvent(key: String?, value: String?, campaignId: String?) {
        if (mMeasure != null && mHasConsent) {
            mMeasure!!.measureEvent(key!!, value!!, campaignId!!)
            mMeasure!!.persist()
        }
    }

    /**
    fun showInfo (context : Activity) {


    }**/

    fun showSurvey (context : Activity, viewId: String) {

        if (!mHasConsent) {
            val result = getConsent(context, CI_CAMPAIGN, viewId)

        }
        else {
            InputSheet().show(context) {
                title(context.getString(R.string.measurement_survey_title))
                with(InputCheckBox("general_accuracy") { // Read value later by index or custom key from bundle
                    label(getString(R.string.measure_title))
                    text(getString(R.string.measure_description))
                    // ... more options
                })
                with(InputEditText ("feedback") {
                    label(getString(R.string.measure_comment_title))
                    hint(getString(R.string.measure_comments_desc))
                    //     validationListener {  } // Add custom validation logic
                    //   changeListener { value -> } // Input value changed
                    // resultListener { value -> () } // Input value changed when form finished
                })

                // ... more input options
                onNegative(getString(R.string.cancel)) {

                    measureView("survey:" + viewId, CI_CAMPAIGN)

                }
                onPositive(getString(R.string.action_okay)) { result ->
                    val feedback = result.getString("feedback") // Read value of inputs by index
                    val check = result.getBoolean("general_accuracy") // Read value by passed key

                    measureView("survey:" + viewId, CI_CAMPAIGN)

                    if (feedback != null)
                        measureEvent("feedback", feedback, CI_CAMPAIGN)

                    measureEvent("general_accuracy", check.toString(), CI_CAMPAIGN)

                }
            }
        }
    }


}
