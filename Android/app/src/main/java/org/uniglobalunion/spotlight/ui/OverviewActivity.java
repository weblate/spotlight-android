/*
 * Copyright 2017 Google Inc. All Rights Reserved.
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.uniglobalunion.spotlight.ui;

import android.Manifest;
import android.app.AppOpsManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.provider.Settings;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.schibstedspain.leku.LocationPickerActivity;

import org.uniglobalunion.spotlight.Constants;
import org.uniglobalunion.spotlight.R;
import org.uniglobalunion.spotlight.model.StudyListing;
import org.uniglobalunion.spotlight.model.StudySummary;
import org.uniglobalunion.spotlight.security.LockScreenActivity;
import org.uniglobalunion.spotlight.security.PreferencesSettings;
import org.uniglobalunion.spotlight.service.TrackingService;

import java.util.Calendar;
import java.util.Date;

import pl.pawelkleczkowski.customgauge.CustomGauge;

import static android.app.AppOpsManager.MODE_ALLOWED;
import static android.app.AppOpsManager.OPSTR_GET_USAGE_STATS;
import static android.os.Process.myUid;
import static com.schibstedspain.leku.LocationPickerActivityKt.LATITUDE;
import static com.schibstedspain.leku.LocationPickerActivityKt.LOCATION_ADDRESS;
import static com.schibstedspain.leku.LocationPickerActivityKt.LONGITUDE;
import static com.schibstedspain.leku.LocationPickerActivityKt.ZIPCODE;

public class OverviewActivity extends AppCompatActivity {

    protected static final String TAG = "MainActivity";

    private Context mContext;

    private StudyListing studyListing = null;


    Date todayStart, todayEnd;

    CustomGauge gDistance, gApps, gCommute;

    @SuppressWarnings("unchecked")
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.overview_activity);

        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.HOUR_OF_DAY, 0);
        cal.set(Calendar.MINUTE, 1);

        todayStart = cal.getTime();

        cal.set(Calendar.HOUR_OF_DAY, 23);
        cal.set(Calendar.MINUTE, 59);

        todayEnd = cal.getTime();

        mContext = this;
        Toolbar toolbar = findViewById(R.id.toolbar);


        setSupportActionBar(toolbar);
        setTitle("");
     //   getSupportActionBar().hide();

        gDistance = findViewById(R.id.progressBarDistance);
        gApps = findViewById(R.id.progressBarAppUsage);
        gCommute = findViewById(R.id.progressBarCommute);

        Intent i = new Intent(this, TrackingService.class);
        startService(i);

    }


    @Override
    protected void onResume() {
        super.onResume();

        showDistance();
        showAppUsage();
        showCommute();
    }


    public void onDistanceClicked (View view)
    {
        /**
         * {
         *       "id": "study4",
         *       "name": "Distance",
         *       "description": "Track your location and measure distanced traveled each day",
         *       "icon": "R.drawable.ic_distance_sensors",
         *       "geo": true,
         *       "apps": false,
         *       "activities": false,
         *       "environment": false,
         *       "event": false
         *     },
         */
        showStudy("study4","Distance",true,false,false,false,false, false);

    }

    public void onCommuteClicked (View view)
    {
/**
 * {
 *       "id": "study4",
 *       "name": "Distance",
 *       "description": "Track your location and measure distanced traveled each day",
 *       "icon": "R.drawable.ic_distance_sensors",
 *       "geo": true,
 *       "apps": false,
 *       "activities": false,
 *       "environment": false,
 *       "event": false
 *     },
 */
        showStudy("study4","Distance",true,false,false,false,false, true);
    }


    public void onAppUsageClicked (View view)
    {
        /**
         * {
         *       "id": "study3",
         *       "name": "App Usage",
         *       "description": "Measure how much time you spend using work apps",
         *       "icon": "R.drawable.ic_screen_time_sensors",
         *       "geo": false,
         *       "apps": true,
         *       "activities": false,
         *       "environment": false,
         *       "event": false
         *     }
         */

        showStudy("study3","App Usage",false,false,true,false,false, false);
    }

    public void showStudy (String studyId, String title, boolean geo, boolean activity, boolean apps, boolean environ, boolean event, boolean commute)
    {
        Intent intent = new Intent(this,StudyExplorerActivity.class);
        intent.putExtra("title",title);
        intent.putExtra("id",studyId);
        intent.putExtra("apps",apps);//track app usage
        intent.putExtra("geo",geo);//track geo location
        intent.putExtra("activity",activity);//track activity (motion)
        intent.putExtra("environ",environ); //track environ
        intent.putExtra("event",event); //track environ
        intent.putExtra("commute",commute); //track environ


        startActivity(intent);
    }


    private void showMinutesMoved ()
    {
        StudySummary.getTotalMotion(this, this, todayStart.getTime(), todayEnd.getTime(), new StudySummary.StudySummaryListener() {
            @Override
            public void handleResult(String result) {

             //   ((TextView) findViewById(R.id.result_distance)).setText(result);

            }

            @Override
            public void handleResult(int total) {
                //total minutes
            }
        });
    }

    private void showAppUsage ()
    {

        if(!isStudyEnabled("study3"))
        {
            ((TextView) findViewById(R.id.result_app_usage)).setTextSize(18);
            ((TextView) findViewById(R.id.result_app_usage)).setText(getString(R.string.tap_to_config));

        }
        else {
            StudySummary.getTotalAppUsageTime(this, this, todayStart.getTime(), todayEnd.getTime(), new StudySummary.StudySummaryListener() {
                @Override
                public void handleResult(String result) {
                    ((TextView) findViewById(R.id.result_app_usage)).setTextSize(24);
                    ((TextView) findViewById(R.id.result_app_usage)).setText(result);

                }

                @Override
                public void handleResult(int total) {
                    //total minutes

                    gApps.setValue(total);
                }
            });
        }
    }

    private void showCommute ()
    {
        if(!isStudyEnabled("study4"))
        {
            ((TextView) findViewById(R.id.result_commute)).setTextSize(18);
            ((TextView) findViewById(R.id.result_commute)).setText(getString(R.string.tap_to_config));

        }
        else {
            StudySummary.getLastCommuteTime(this, this, todayStart.getTime(), todayEnd.getTime(), new StudySummary.StudySummaryListener() {
                @Override
                public void handleResult(String result) {
                    ((TextView) findViewById(R.id.result_commute)).setTextSize(24);
                    ((TextView) findViewById(R.id.result_commute)).setText(result);

                }

                @Override
                public void handleResult(int total) {
                    //total minutes
                    gCommute.setValue(total);
                }
            });
        }
    }

    private void showDistance ()
    {

        if(!isStudyEnabled("study4"))
        {
            ((TextView) findViewById(R.id.result_distance)).setTextSize(18);
            ((TextView) findViewById(R.id.result_distance)).setText(getString(R.string.tap_to_config));

        }
        else
        {
            StudySummary.getTotalDistance(this, this, todayStart.getTime(), todayEnd.getTime(), new StudySummary.StudySummaryListener() {
                @Override
                public void handleResult(String result) {

                    ((TextView) findViewById(R.id.result_distance)).setTextSize(24);
                    ((TextView) findViewById(R.id.result_distance)).setText(result);

                }

                @Override
                public void handleResult(int total) {
                    //total distance in meters
                    //pbDistance.setProgress(total);
                    gDistance.setValue(total);
                }
            });
        }


    }



    private void lockApp ()
    {
        finish();
        PreferencesSettings.saveToPref(this,"locked",true);

        Intent intent = new Intent(this, LockScreenActivity.class);
        startActivity(intent);
    }

    private void stopService ()
    {

        Intent intent = new Intent(this, TrackingService.class);
        stopService(intent);
        finish();
    }


    private void showData ()
    {

        Intent intent = new Intent(this, StudyResultsActivity.class);

        startActivity(intent);

    }

    private void enableUsageTracking () {

        if (!checkForAppsPermission(this)) {

            DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    switch (which){
                        case DialogInterface.BUTTON_POSITIVE:
                            //Yes button clicked
                            startActivity(new Intent(Settings.ACTION_USAGE_ACCESS_SETTINGS));
                            break;

                        case DialogInterface.BUTTON_NEGATIVE:
                            //No button clicked
                            break;
                    }
                }
            };

            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setMessage(getString(R.string.prompt_app_usage_permission)).setPositiveButton(R.string.yes, dialogClickListener)
                    .setNegativeButton(R.string.no, dialogClickListener).show();

        }

    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

    }

    /**
     * Registers for activity recognition updates using
     * Registers success and failure callbacks.
     */
    public void startStudy(String studyId) {
        Intent intent = new Intent(this, TrackingService.class);
        intent.setAction(TrackingService.ACTION_START_TRACKING);
        intent.putExtra("studyid",studyId);
        ContextCompat.startForegroundService(this,intent);
    }

    /**
     * Registers for activity recognition updates using
     * Registers success and failure callbacks.
     */
    public void sendAction(String action, String studyId) {
        Intent intent = new Intent(this, TrackingService.class);
        intent.setAction(action);
        intent.putExtra("studyid",studyId);
        ContextCompat.startForegroundService(this,intent);
    }


    /**
     * Removes activity recognition updates using
     * failure callbacks.
     */
    public void stopStudy (String studyId) {

        Intent intent = new Intent(this,TrackingService.class);
        intent.setAction(TrackingService.ACTION_STOP_TRACKING);
        intent.putExtra("studyid",studyId);
        startService(intent);

    }

    public void resetStudy ()
    {

    }



    /**
     * Retrieves the boolean from SharedPreferences that tracks whether we are requesting activity
     * updates.
     */
    private boolean isStudyEnabled(String studyId) {
        return PreferenceManager.getDefaultSharedPreferences(this)
                .getBoolean(Constants.KEY_ACTIVITY_UPDATES_REQUESTED + "_" + studyId, false);
    }

    private void setStudyEnabled(String studyId, boolean enabled) {
        PreferenceManager.getDefaultSharedPreferences(this).edit()
                .putBoolean(Constants.KEY_ACTIVITY_UPDATES_REQUESTED + "_" + studyId, enabled).commit();

        studyListing.isEnabled = enabled;

        if (studyListing.isEnabled && studyListing.trackApps)
        {
            enableUsageTracking();
        }
    }


    private boolean checkForAppsPermission(Context context) {
        AppOpsManager appOps = (AppOpsManager) context.getSystemService(Context.APP_OPS_SERVICE);
        int mode = appOps.checkOpNoThrow(OPSTR_GET_USAGE_STATS,myUid(), context.getPackageName());
        return mode == MODE_ALLOWED;
    }

    public void viewDataClicked (View view)
    {
        showData();
    }

    public void setHomeClicked (View view)
    {
      //  Intent intent = new Intent(this, TrackingService.class);
      //  intent.setAction(TrackingService.ACTION_SET_GEO_HOME);
       // startService(intent);
        initLocationPicker (MAP_HOME_REQUEST_CODE);
    }


    public void setWorkClicked (View view)
    {
        //Intent intent = new Intent(this, TrackingService.class);
        //intent.setAction(TrackingService.ACTION_SET_GEO_WORK);
        //startService(intent);
        initLocationPicker (MAP_WORK_REQUEST_CODE);
    }

    public void chooseAppsClicked (View view)
    {
        startActivityForResult(new Intent(this,AppManagerActivity.class),APPS_REQUEST_CODE);
    }

    private int MAP_HOME_REQUEST_CODE = 1001;
    private int MAP_WORK_REQUEST_CODE = MAP_HOME_REQUEST_CODE+1;
    private int APPS_REQUEST_CODE = MAP_WORK_REQUEST_CODE+1;


    private void initLocationPicker (int requestCode)
    {
        Intent locationPickerIntent = new LocationPickerActivity.Builder()
                .withDefaultLocaleSearchZone()
                .shouldReturnOkOnBackPressed()
                .withSatelliteViewHidden()
                .withGooglePlacesEnabled()
                .withGoogleTimeZoneEnabled()
                .withVoiceSearchHidden()
                .withUnnamedRoadHidden()
                .build(this);


        startActivityForResult(locationPickerIntent, requestCode);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == RESULT_OK)
        {
            if (requestCode == MAP_HOME_REQUEST_CODE || requestCode == MAP_WORK_REQUEST_CODE) {
                double latitude = data.getDoubleExtra(LATITUDE, 0.0);
                double longitude = data.getDoubleExtra(LONGITUDE, 0.0);
                String address = data.getStringExtra(LOCATION_ADDRESS);
                String postalcode = data.getStringExtra(ZIPCODE);

                String geoKey = "home";
                if (requestCode == MAP_WORK_REQUEST_CODE)
                    geoKey = "work";

                SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);

                prefs
                        .edit()
                        .putFloat("geo-" + geoKey + "-lat", (float) latitude)
                        .putFloat("geo-" + geoKey + "-long", (float) longitude)
                        .apply();

            }
            else if (requestCode == APPS_REQUEST_CODE)
            {

            }

        }
    }

    private int REQUEST_RECORD_AUDIO = 1234;

    private boolean checkEnvironPermissions ()
    {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.RECORD_AUDIO) != PackageManager.PERMISSION_GRANTED) {

            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.RECORD_AUDIO},
                    REQUEST_RECORD_AUDIO);

            return false;
        }
        else
            return true;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.study_picker_menu, menu);
        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {


        if (item.getItemId() == R.id.menu_about) {
            finish();
            startActivity(new Intent(this, OnboardingActivity.class));
        }
        else  if (item.getItemId() == R.id.menu_settings)
        {
            startActivity(new Intent(this, SettingsActivity.class));
        }
        else  if (item.getItemId() == R.id.menu_data)
        {
            startActivity(new Intent(this, StudyResultsActivity.class));
        }
        else if (item.getItemId() == R.id.menu_exit)
        {
            stopService();
        }
        else if (item.getItemId() == R.id.menu_lock)
        {
            lockApp ();
        }
        else if (item.getItemId() == R.id.menu_privacy)
        {
            showPage(getString(R.string.privacy_link));
        }


        return super.onOptionsItemSelected(item);
    }

    private void showPage (String url)
    {
        Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
        startActivity(browserIntent);
    }

}
