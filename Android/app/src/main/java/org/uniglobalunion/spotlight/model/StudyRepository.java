package org.uniglobalunion.spotlight.model;

import android.content.Context;
import android.os.AsyncTask;

import androidx.lifecycle.LiveData;

import java.util.List;

public class StudyRepository {

    private StudyDao mStudyDao;
    private LiveData<List<Study>> mAllStudies;

    public StudyRepository(Context context) {
        StudyRoomDatabase db = StudyRoomDatabase.getDatabase(context);
        mStudyDao = db.studyEventDao();
        mAllStudies = mStudyDao.getStudies();
    }

    public LiveData<List<StudyEvent>> getStudyEvents() {
        return mStudyDao.getStudyEvents();
    }

    public LiveData<List<StudyEvent>> getStudyEvents(String studyId) {
        return mStudyDao.getStudyEvents(studyId);
    }

    public List<StudyEvent> getStudyEventsStatic(String studyId) {
        return mStudyDao.getStudyEventsStatic(studyId);
    }

    public LiveData<List<StudyEvent>> getStudyEvents(List<String> studyId) {
        return mStudyDao.getStudyEvents(studyId);
    }

    public LiveData<List<StudyEvent>> getStudyEvents(List<String> studyId, long startTime, long endTime) {
        return mStudyDao.getStudyEvents(studyId, startTime, endTime);
    }

    public LiveData<List<StudyEvent>> getStudyEvents(long startTime, long endTime) {
        return mStudyDao.getStudyEvents(startTime, endTime);
    }

    public void deleteStudyEvents (String studyId) {
        new deleteStudyEventAsyncTask(mStudyDao,-1,-1).execute(studyId);
    }

    private static class deleteStudyEventAsyncTask extends AsyncTask<String, Void, Void> {

        private StudyDao mAsyncTaskDao;
        private long mStartTime, mEndTime;

        deleteStudyEventAsyncTask(StudyDao dao, long startTime, long endTime) {
            mAsyncTaskDao = dao;
            mStartTime = startTime;
            mEndTime = endTime;
        }

        @Override
        protected Void doInBackground(final String... params) {

            if (params.length > 0) {
                for (int i = 0; i < params.length; i++)
                    if (mStartTime != -1)
                        mAsyncTaskDao.deleteStudyEvents(params[i],mStartTime,mEndTime);
                    else
                        mAsyncTaskDao.deleteStudyEvents(params[i]);
            }
            else
                if (mStartTime != -1)
                    mAsyncTaskDao.deleteAllStudyEvents(mStartTime, mEndTime);
                else
                    mAsyncTaskDao.deleteAllStudyEvents();



            return null;
        }
    }


    public void deleteStudyEvents (List<String> studyIds, long startTime, long endTime) {

        for (String studyId : studyIds)
            new deleteStudyEventAsyncTask(mStudyDao,startTime,endTime).execute(studyId);

    }

    public void deleteAllStudyEvents () {
        new deleteStudyEventAsyncTask(mStudyDao,-1,-1).execute();
    }

    public void deleteAllStudyEvents (long startTime, long endTime) {
        new deleteStudyEventAsyncTask(mStudyDao,startTime, endTime).execute();
    }

    public LiveData<List<StudyEvent>> getStudyEvents(String studyId, long startTime, long endTime) {
        return mStudyDao.getStudyEvents(studyId, startTime, endTime);
    }

    public List<StudyEvent> getStudyEventsStatic(String studyId, long startTime, long endTime) {
        return mStudyDao.getStudyEventsStatic(studyId, startTime, endTime);
    }

    public List<StudyEvent> getStudyEventsStatic(List<String> studyId, long startTime, long endTime) {
        return mStudyDao.getStudyEventsStatic(studyId, startTime, endTime);
    }

    public LiveData<List<Study>> getStudies() {
        return mAllStudies;
    }

    public void insert (StudyEvent studyEvent) {
        new insertStudyEventAsyncTask(mStudyDao).execute(studyEvent);
    }

    private static class insertStudyEventAsyncTask extends AsyncTask<StudyEvent, Void, Void> {

        private StudyDao mAsyncTaskDao;

        insertStudyEventAsyncTask(StudyDao dao) {
            mAsyncTaskDao = dao;
        }

        @Override
        protected Void doInBackground(final StudyEvent... params) {
            mAsyncTaskDao.insert(params[0]);
            return null;
        }
    }

    public void insert (Study study) {
        new insertStudyAsyncTask(mStudyDao).execute(study);
    }

    private static class insertStudyAsyncTask extends AsyncTask<Study, Void, Void> {

        private StudyDao mAsyncTaskDao;

        insertStudyAsyncTask(StudyDao dao) {
            mAsyncTaskDao = dao;
        }

        @Override
        protected Void doInBackground(final Study... params) {
            mAsyncTaskDao.insert(params[0]);
            return null;
        }
    }
}
