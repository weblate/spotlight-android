package org.uniglobalunion.spotlight.model;

import android.app.Application;
import android.text.TextUtils;

import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import java.util.List;

public class StudyViewModel extends AndroidViewModel {

    private StudyRepository mRepository;

    private LiveData<List<StudyEvent>> mAllStudyEvents;
    private LiveData<List<Study>> mAllStudies;

    public StudyViewModel (Application application) {
        super(application);
        mRepository = new StudyRepository(application);
       // mAllStudyEvents = mRepository.getStudyEvents();
    }

    public LiveData<List<StudyEvent>> getStudyEvents() { return mAllStudyEvents; }

    public LiveData<List<StudyEvent>> getStudyEvents(String studyId) {
        if (!TextUtils.isEmpty(studyId))
            return mRepository.getStudyEvents(studyId);
        else
            return null;
    }

    private LiveData<List<StudyEvent>> getAllStudyEvents ()
    {
        if (mAllStudies == null)
            mAllStudyEvents = mRepository.getStudyEvents();

        return mAllStudyEvents;
    }

    public LiveData<List<StudyEvent>> getStudyEvents(List<String> studyIds) {
        if (studyIds != null && studyIds.size() > 0)
            return mRepository.getStudyEvents(studyIds);
        else
            return getAllStudyEvents ();
    }

    public LiveData<List<StudyEvent>> getStudyEvents(List<String> studyIds, long startTime, long endTime) {
        if (studyIds != null && studyIds.size() > 0)
            return mRepository.getStudyEvents(studyIds,startTime,endTime);
        else
            return mRepository.getStudyEvents(startTime,endTime);
    }

    public void deleteStudyEvents (String studyId) {
        mRepository.deleteStudyEvents(studyId);
    }

    public void deleteStudyEvents (List<String> studyIds, long startTime, long endTime) {
        if (studyIds != null && studyIds.size() > 0)
            mRepository.deleteStudyEvents(studyIds,startTime,endTime);
        else
            mRepository.deleteAllStudyEvents(startTime, endTime);
    }



    public void insert(StudyEvent studyEvent) { mRepository.insert(studyEvent); }

    public void insert(Study study) { mRepository.insert(study); }

}
