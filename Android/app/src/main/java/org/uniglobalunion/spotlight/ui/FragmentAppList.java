package org.uniglobalunion.spotlight.ui;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.preference.PreferenceManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ProgressBar;
import android.widget.TextView;

import org.uniglobalunion.spotlight.AppInfo;
import org.uniglobalunion.spotlight.R;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.StringTokenizer;

import static org.uniglobalunion.spotlight.Constants.PREFS_KEY_APP_ENABLED;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link FragmentAppList.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link FragmentAppList#newInstance} factory method to
 * create an instance of this fragment.
 */
public class FragmentAppList extends Fragment {


    private OnFragmentInteractionListener mListener;

    private GridView listApps;
    private ListAdapter adapterApps;
    private ProgressBar progressBar;
    PackageManager pMgr = null;

    private HashMap<String,String> appLabels = new HashMap<>();

    public FragmentAppList() {

    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @return A new instance of fragment FragmentAppList.
     */
    // TODO: Rename and change types and number of parameters
    public static FragmentAppList newInstance() {
        FragmentAppList fragment = new FragmentAppList();
        Bundle args = new Bundle();
//        args.putString(ARG_PARAM1, param1);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
        }
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_fragment_app_list, container, false);

        listApps = view.findViewById(R.id.applistview);
      //  reloadApps();
        return view;
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {

        }

        if (getActivity()!=null)
        reloadApps(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }

    public void setAppLabel (String pkgId, String label)
    {
        appLabels.put(pkgId, label);
    }

    public void reloadApps (final Context context) {

        if (context != null && (!isDetached()))
        {
            mPrefs = PreferenceManager.getDefaultSharedPreferences(context);
            pMgr = context.getPackageManager();

            new AsyncTask<Void, Void, Void>() {
                protected void onPreExecute() {

                }

                protected Void doInBackground(Void... unused) {
                    loadApps(mPrefs, context);
                    return null;
                }

                protected void onPostExecute(Void unused) {
                    listApps.setAdapter(adapterApps);
                }
            }.execute();

        }
    }

    SharedPreferences mPrefs = null;
    ArrayList<AppInfo> mApps = null;

    public ArrayList<AppInfo> getCurrentApps ()
    {
        return mApps;
    }

    private void loadApps (SharedPreferences prefs, Context context)
    {

        mApps = getApps(context, prefs);

        Collections.sort(mApps,new Comparator<AppInfo>() {
            public int compare(AppInfo o1, AppInfo o2) {
                /* Some apps start with lowercase letters and without the sorting being case
                   insensitive they'd appear at the end of the grid of apps, a position where users
                   would likely not expect to find them.
                 */
                if (o1.isTorified() == o2.isTorified())
                    return o1.getName().compareToIgnoreCase(o2.getName());
                if (o1.isTorified()) return -1;
                return 1;
            }
        });

        final LayoutInflater inflater = getLayoutInflater();

        adapterApps = new ArrayAdapter<AppInfo>(getActivity(), R.layout.layout_apps_item, R.id.itemtext,mApps) {

            public View getView(int position, View convertView, ViewGroup parent) {

                ListEntry entry = null;

                if (convertView == null)
                    convertView = inflater.inflate(R.layout.layout_apps_item, parent, false);
                else
                    entry = (ListEntry) convertView.getTag();

                if (entry == null) {
                    // Inflate a new view
                    entry = new ListEntry();
                    entry.icon = (ImageView) convertView.findViewById(R.id.itemicon);
                    entry.box = (CheckBox) convertView.findViewById(R.id.itemcheck);
                    entry.text = (TextView) convertView.findViewById(R.id.itemtext);
                    convertView.setTag(entry);
                    entry.box.setVisibility(View.GONE);
                    entry.text.setTextColor(R.color.app_primary_dark);
                }

                final AppInfo app = mApps.get(position);

                if (entry.icon != null) {

                    try {
                        entry.icon.setImageDrawable(pMgr.getApplicationIcon(app.getPackageName()));
                        entry.icon.setTag(entry.box);
                    }
                    catch (Exception e)
                    {
                        e.printStackTrace();
                    }
                }

                if (entry.text != null) {

                    if (appLabels.containsKey(app.getPackageName()))
                        entry.text.setText(appLabels.get(app.getPackageName()));
                    else
                        entry.text.setText(app.getName());

                    entry.text.setTag(entry.box);
                }



                return convertView;
            }
        };


    }

    private static class ListEntry {
        private CheckBox box;
        private TextView text;
        private ImageView icon;
    }



    private ArrayList<AppInfo> getApps (Context context, SharedPreferences prefs)
    {

        String tordAppString = prefs.getString(PREFS_KEY_APP_ENABLED, "");

        ArrayList<AppInfo> apps = new ArrayList<AppInfo>();

        ApplicationInfo aInfo = null;

        AppInfo app = null;

        StringTokenizer st = new StringTokenizer(tordAppString,"|");
        while (st.hasMoreTokens())
        {
            try {
                aInfo = pMgr.getApplicationInfo(st.nextToken(),0);

                app = new AppInfo();
                app.setEnabled(aInfo.enabled);
                app.setUid(aInfo.uid);
                app.setProcname(aInfo.processName);
                app.setPackageName(aInfo.packageName);
                app.setUsername(pMgr.getNameForUid(app.getUid()));
                app.setUsesInternet(true);

            } catch (Exception e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            /**
             if ((aInfo.flags & ApplicationInfo.FLAG_SYSTEM) == 1)
             {
             //System app
             app.setUsesInternet(true);
             }**/


            try
            {
                app.setName(pMgr.getApplicationLabel(aInfo).toString());
            }
            catch (Exception e)
            {
                // no name
                continue; //we only show apps with names
            }

            apps.add(app);






        }

        Collections.sort(apps);

        return apps;
    }
}
