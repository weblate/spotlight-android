package org.uniglobalunion.spotlight.model;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;

import java.util.List;

@Dao
public interface StudyDao {


    @Insert
    void insert(StudyEvent studyEvent);

    @Query("SELECT * from study_event_table ORDER BY id DESC")
    LiveData<List<StudyEvent>> getStudyEvents();

    @Query("SELECT * from study_event_table  WHERE study_id == :studyId ORDER BY id DESC")
    LiveData<List<StudyEvent>> getStudyEvents(String studyId);

    @Query("SELECT * from study_event_table  WHERE study_id == :studyId ORDER BY id DESC")
    List<StudyEvent> getStudyEventsStatic(String studyId);

    @Query("SELECT * from study_event_table  WHERE study_id IN (:studyIds) ORDER BY id DESC")
    LiveData<List<StudyEvent>> getStudyEvents(List<String> studyIds);

    @Query("SELECT * from study_event_table  WHERE study_id == :studyId AND study_timestamp > :studyTimeStart AND study_timestamp <= :studyTimeEnd ORDER BY id DESC")
    LiveData<List<StudyEvent>> getStudyEvents(String studyId, long studyTimeStart, long studyTimeEnd);

    @Query("SELECT * from study_event_table  WHERE study_id == :studyId AND study_timestamp > :studyTimeStart AND study_timestamp <= :studyTimeEnd ORDER BY id DESC")
    List<StudyEvent> getStudyEventsStatic(String studyId, long studyTimeStart, long studyTimeEnd);

    @Query("SELECT * from study_event_table  WHERE study_id IN (:studyIds) AND study_timestamp > :studyTimeStart AND study_timestamp <= :studyTimeEnd ORDER BY id DESC")
    LiveData<List<StudyEvent>> getStudyEvents(List<String> studyIds, long studyTimeStart, long studyTimeEnd);

    @Query("SELECT * from study_event_table  WHERE study_timestamp > :studyTimeStart AND study_timestamp <= :studyTimeEnd ORDER BY id DESC")
    LiveData<List<StudyEvent>> getStudyEvents(long studyTimeStart, long studyTimeEnd);

    @Query("SELECT * from study_event_table  WHERE study_id IN (:studyIds) AND study_timestamp > :studyTimeStart AND study_timestamp <= :studyTimeEnd ORDER BY id DESC")
    List<StudyEvent> getStudyEventsStatic(List<String> studyIds, long studyTimeStart, long studyTimeEnd);

    @Query("SELECT COUNT(id) from study_event_table")
    int getStudyEventCount();

    @Insert
    void insert(Study study);

    @Query("DELETE FROM study_event_table WHERE study_id == :studyId ")
    void deleteStudyEvents(String studyId);

    @Query("DELETE FROM study_event_table WHERE study_id IN (:studyId) ")
    void deleteStudyEvents(List<String> studyId);

    @Query("DELETE FROM study_event_table")
    void deleteAllStudyEvents();

    @Query("DELETE FROM study_event_table WHERE study_timestamp > :studyTimeStart AND study_timestamp <= :studyTimeEnd")
    void deleteAllStudyEvents(long studyTimeStart, long studyTimeEnd);

    @Query("DELETE FROM study_event_table WHERE study_id == :studyId AND study_timestamp > :studyTimeStart AND study_timestamp <= :studyTimeEnd ")
    void deleteStudyEvents(String studyId, long studyTimeStart, long studyTimeEnd);

    @Query("DELETE FROM study_event_table WHERE study_id IN (:studyId) AND study_timestamp > :studyTimeStart AND study_timestamp <= :studyTimeEnd ")
    void deleteStudyEvents(List<String> studyId, long studyTimeStart, long studyTimeEnd);

    @Query("DELETE FROM study_event_table WHERE study_timestamp > :studyTimeStart AND study_timestamp <= :studyTimeEnd ")
    void deleteStudyEvents(long studyTimeStart, long studyTimeEnd);



    @Query("DELETE FROM study_table")
    void deleteAllStudies();

    @Query("SELECT * from study_table ORDER BY id ASC")
    LiveData<List<Study>> getStudies();

    @Query("SELECT * FROM study_table WHERE id == :studyId")
    List<Study> getStudy(String studyId);

}