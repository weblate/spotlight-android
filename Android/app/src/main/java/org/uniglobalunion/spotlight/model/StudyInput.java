package org.uniglobalunion.spotlight.model;

public class StudyInput {

    public static final String INPUT_TYPE_ACTIVITY = "1";
    public static final String INPUT_TYPE_GEOFENCE = "2";
    public static final String INPUT_TYPE_GEOLOCATION = "3";
    public static final String INPUT_TYPE_APP_USAGE = "4";
    public static final String INPUT_TYPE_SCREEN = "4";
    public static final String INPUT_TYPE_POWER = "5";

}
