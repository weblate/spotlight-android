/*
 * Copyright 2017 Google Inc. All Rights Reserved.
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.uniglobalunion.spotlight.ui;

import android.Manifest;
import android.app.AppOpsManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.location.Location;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.provider.Settings;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.widget.SwitchCompat;
import androidx.cardview.widget.CardView;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.Observer;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.Circle;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.android.gms.maps.model.TileOverlay;
import com.google.android.gms.maps.model.TileOverlayOptions;
import com.google.android.material.snackbar.Snackbar;
import com.google.maps.android.SphericalUtil;
import com.google.maps.android.heatmaps.HeatmapTileProvider;
import com.schibstedspain.leku.LocationPickerActivity;

import org.uniglobalunion.spotlight.Constants;
import org.uniglobalunion.spotlight.Prefs;
import org.uniglobalunion.spotlight.R;
import org.uniglobalunion.spotlight.SpotlightApp;
import org.uniglobalunion.spotlight.model.StudyEvent;
import org.uniglobalunion.spotlight.model.StudyListing;
import org.uniglobalunion.spotlight.model.StudyRepository;
import org.uniglobalunion.spotlight.model.StudySummary;
import org.uniglobalunion.spotlight.sensors.PermissionManager;
import org.uniglobalunion.spotlight.service.TrackingService;
import org.uniglobalunion.spotlight.ui.charts.BarResultFragment;
import org.uniglobalunion.spotlight.util.SharingUtils;
import org.uniglobalunion.spotlight.util.UnitLocale;

import java.io.File;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.StringTokenizer;
import java.util.concurrent.TimeUnit;

import static android.app.AppOpsManager.MODE_ALLOWED;
import static android.app.AppOpsManager.OPSTR_GET_USAGE_STATS;
import static android.os.Process.myUid;
import static org.uniglobalunion.spotlight.Constants.PREFS_KEY_APP_ENABLED;
import static org.uniglobalunion.spotlight.service.GeofenceTransitionsIntentService.GEOKEY_HOME;
import static org.uniglobalunion.spotlight.service.GeofenceTransitionsIntentService.GEOKEY_WORK;
import static org.uniglobalunion.spotlight.service.TrackingService.ACTION_ENVIRON_SNAPSHOT;
import static org.uniglobalunion.spotlight.service.TrackingService.getRepository;

public class FragmentStudyStatus extends Fragment {

    protected static final String TAG = "MainActivity";

    private Context mContext;


    private String studyTitle = "Study";
    private String studyId = "study1";

    private StudyListing studyListing = null;

    private FragmentAppList mFragApps;

    boolean showActivities = false;
    boolean showEnviron = false;
    boolean showApps = false;
    boolean showGeo = false;
    boolean showCommute = false;
    boolean showEvent = false;
    boolean showTime = false;

    Spinner spinnerTime;
    SwitchCompat switchStatus;
    TextView tvTitle;
    SwitchCompat switchHeatMap;

    Date todayStart, todayEnd;
    boolean dataHasChanged = false;

    Handler mHandler = new Handler();

    GoogleMap mGoogleMap;
    TileOverlay mTileHeatMap;
    ArrayList<LatLng> llList = null;

    boolean mapSetup = false;
    boolean mapZoomed = false;

    Calendar mCal;

    boolean isAttached = false;
    static ArrayList<Integer> colors = null;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        isAttached = true;
        /**
         if (context instanceof OnFragmentInteractionListener) {
         mListener = (OnFragmentInteractionListener) context;
         } else {
         throw new RuntimeException(context.toString()
         + " must implement OnFragmentInteractionListener");
         }**/
    }

    @Override
    public void onDetach() {
        super.onDetach();


        isAttached = false;

    }

    public FragmentStudyStatus() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment FragmentStudyDaily.
     */
    // TODO: Rename and change types and number of parameters
    public static FragmentStudyStatus newInstance(String param1, String param2) {
        FragmentStudyStatus fragment = new FragmentStudyStatus();
        Bundle args = new Bundle();

        fragment.setArguments(args);
        return fragment;
    }

    View view;

    public View getPrimaryView ()
    {
        return view;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_study_status, container, false);

        switchStatus = view.findViewById(R.id.request_activity_updates_switch);

        tvTitle = view.findViewById(R.id.txtStatus);

        switchHeatMap = view.findViewById(R.id.toggle_heat_map);

        view.findViewById(R.id.button_share_map).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                mGoogleMap.snapshot(new GoogleMap.SnapshotReadyCallback() {
                    @Override
                    public void onSnapshotReady(Bitmap bitmap) {
                        String totalDistance = ((TextView) view.findViewById(R.id.location_info)).getText().toString();
                        SharingUtils.shareBitmapWithText(mContext,bitmap,totalDistance, true);
                    }
                });
            }
        });

        initUI();
        return view;
    }

    @SuppressWarnings("unchecked")
    @Override
    public void onCreate(Bundle data) {
        super.onCreate(data);



    }


    private void initUI () {
        studyTitle = getActivity().getIntent().getStringExtra("title");

        studyId = getActivity().getIntent().getStringExtra("id");
        studyListing = ((SpotlightApp)getActivity().getApplication()).getStudy(studyId);


        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.HOUR_OF_DAY, 0);
        cal.set(Calendar.MINUTE, 1);
        cal.set(Calendar.SECOND,0);

        todayStart = cal.getTime();

        cal.set(Calendar.HOUR_OF_DAY, 23);
        cal.set(Calendar.MINUTE, 59);
        cal.set(Calendar.SECOND,0);


        todayEnd = cal.getTime();

        mContext = getActivity();

        showActivities = getActivity().getIntent().getBooleanExtra("activity", false);
        showEnviron = getActivity().getIntent().getBooleanExtra("environ", false);
        showApps = getActivity().getIntent().getBooleanExtra("apps", false);
        showGeo = getActivity().getIntent().getBooleanExtra("geo", false);
        showEvent = getActivity().getIntent().getBooleanExtra("event", false);
        showCommute = getActivity().getIntent().getBooleanExtra("commute", false);
        showTime = getActivity().getIntent().getBooleanExtra("time", false);


        /**
        mStudyViewModel = ViewModelProviders.of(this).get(StudyViewModel.class);
        mStudyViewModel.getStudyEvents().observe(this, studyEvents -> {
            // Update the cached copy of the words in the adapter.
            //adapter.setWords(words);
            //updateDetectedActivitiesList(studyEvents);
        });**/

        ImageView ivStart = view.findViewById(R.id.get_started_image);
        TextView tvStart = view.findViewById(R.id.get_started_title);

        if (showActivities) {
            ivStart.setImageResource(R.drawable.humaaan2);
            tvStart.setText(getString(R.string.get_started_movement));
        }
        else if (showGeo) {
            ivStart.setImageResource(R.drawable.car);
            tvStart.setText(getString(R.string.get_started_geo));
        }
        else if (showEnviron) {
            ivStart.setImageResource(R.drawable.humaaan1);
            tvStart.setText(getString(R.string.get_started_atmosphere));
        }
        else if (showApps) {
            ivStart.setImageResource(R.drawable.humaaan3);
            tvStart.setText(getString(R.string.get_started_apps));
        }
        else if (showEvent) {
            ivStart.setImageResource(R.drawable.splash);
            tvStart.setText(getString(R.string.get_started_events));
        }

        if (isStudyEnabled())
        {
            view.findViewById(R.id.view_get_started).setVisibility(View.GONE);
            view.findViewById(R.id.view_data).setVisibility(View.VISIBLE);

            if (showApps)
                PermissionManager.enableUsageTracking (getActivity());

        }
        else
        {
            view.findViewById(R.id.view_get_started).setVisibility(View.VISIBLE);
            view.findViewById(R.id.view_data).setVisibility(View.GONE);
        }

        initCalendar(cal,0);
    }

    public Calendar getCalendar ()
    {
        return mCal;
    }

    public void changeDay(int dayChange)
    {

        mapSetup = false;
        mapZoomed = false;
        mCal.add(Calendar.DAY_OF_MONTH,dayChange);
        initCalendar(mCal,0);
    }

    public void getStartedClicked (View button)
    {
        setStudyEnabled(true);
        startStudy();
        view.findViewById(R.id.view_get_started).setVisibility(View.GONE);
        view.findViewById(R.id.view_data).setVisibility(View.VISIBLE);
    }

    public void initCalendar (Calendar cal, int position)
    {
        dataHasChanged = false;
        mCal = cal;

        tvTitle.setText(SimpleDateFormat.getDateInstance(SimpleDateFormat.LONG).format(cal.getTime()));

        cal.set(Calendar.HOUR_OF_DAY, 0);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND, 0);


        if (position == 0) {

            todayStart = cal.getTime();

            cal.set(Calendar.HOUR_OF_DAY, 23);
            cal.set(Calendar.MINUTE, 59);
            cal.set(Calendar.SECOND, 0);

            todayEnd = cal.getTime();


        } else if (position == 1) {

            cal.add(Calendar.DAY_OF_MONTH,-7);
            todayStart = cal.getTime();

            cal.add(Calendar.DAY_OF_MONTH,7);
            cal.set(Calendar.HOUR_OF_DAY, 23);
            cal.set(Calendar.MINUTE, 59);
            cal.set(Calendar.SECOND, 0);

            todayEnd = cal.getTime();


        } else if (position == 2) {


            cal.add(Calendar.DAY_OF_MONTH,-30);
            todayStart = cal.getTime();

            cal.add(Calendar.DAY_OF_MONTH,30);
            cal.set(Calendar.HOUR_OF_DAY, 23);
            cal.set(Calendar.MINUTE, 59);
            cal.set(Calendar.SECOND, 0);


            todayEnd = cal.getTime();

        }

        initData();
    }

    private void initData () {

        mapSetup = false;
        mapZoomed = false;

        ((LinearLayout)view.findViewById(R.id.result_chart_fragment)).removeAllViews();
        ((LinearLayout)view.findViewById(R.id.result_chart_time_fragment)).removeAllViews();

        CardView cardView = view.findViewById(R.id.result_chart);
        cardView.setVisibility(showActivities ? View.VISIBLE : View.GONE);

        if (showActivities) {


                ArrayList<String> mStudyIds = new ArrayList<>();
//                mStudyIds.add(StudyEvent.EVENT_TYPE_MOTION);
                mStudyIds.add(StudyEvent.EVENT_TYPE_STEPS);

            getRepository(getActivity()).getStudyEvents(mStudyIds, todayStart.getTime(), todayEnd.getTime()).observe(this, new Observer<List<StudyEvent>>() {
                    @Override
                    public void onChanged(List<StudyEvent> studyEvents) {

                        if (dataHasChanged)
                            return;

                        dataHasChanged = true;

                        String label = getString(R.string.label_steps);
                        long[] xvals = new long[24];
                        float[] yvals = new float[24];


                        for (int i = 0; i < xvals.length; i++) {
                            xvals[i] = i;
                            yvals[i] = 0;
                        }

                        for (StudyEvent event : studyEvents) {
                            Date dateEvent = new Date(event.getTimestamp());
                            int idx = dateEvent.getHours();
                            xvals[idx] = idx;
                            yvals[idx] += Float.valueOf(event.getEventValue());
                        }

                        //long minTotal = TimeUnit.MINUTES.convert(todayEnd.getTime() - todayStart.getTime(), TimeUnit.MILLISECONDS); //total minutes in a day

                        /**
                        String txtTotalTime = "0 mins";
                        if (studyEvents.size() > 1) {
                            minTotal = studyEvents.get(0).getTimestamp() - studyEvents.get(studyEvents.size() - 1).getTimestamp();
                            minTotal = TimeUnit.MINUTES.convert(minTotal, TimeUnit.MILLISECONDS);
                            txtTotalTime = minTotal + " mins";
                        }**/

                        int minMoved = 0;
                        long lastMin = 0;

                        int totalSteps = 0;

                        for (StudyEvent event: studyEvents)
                        {
                            int newMin = new Date(event.getTimestamp()).getMinutes();

                            if (lastMin != 0 && lastMin != newMin)
                                minMoved++;

                            lastMin = newMin;
                            totalSteps += (int)Float.parseFloat(event.getEventValue());

                        }

                        float maxStepsPerHour = 2000f;

                        ArrayList<Integer> colors = new ArrayList<>();
                        colors.add(getResources().getColor(R.color.holo_green_light));

                        BarResultFragment fragLine = BarResultFragment.newInstance(getString(R.string.title_study_results), label, xvals, yvals, 1f,"", maxStepsPerHour,colors);

                        // Begin the transaction
                        FragmentTransaction ft = getActivity().getSupportFragmentManager().beginTransaction();
                        // Replace the contents of the container with the new fragment
                        ft.replace(R.id.result_chart_fragment, fragLine);
                        // or ft.add(R.id.your_placeholder, new FooFragment());
                        // Complete the changes added above
                        ft.commit();
                        //findViewById(R.id.detected_activities).setVisibility(showActivities ? View.VISIBLE : View.GONE);

                        String chartInfoStr = minMoved + " " + getString(R.string.minutes_label) + "\n" + totalSteps + " " + getString(R.string.label_steps);

                        ((TextView) view.findViewById(R.id.chart_info)).setText(chartInfoStr);

                    }
                });

        }

        CardView cardViewTime = view.findViewById(R.id.result_chart_time);
        cardViewTime.setVisibility(showTime ? View.VISIBLE : View.GONE);

        if (showTime) {


            ArrayList<String> mStudyIds = new ArrayList<>();
//                mStudyIds.add(StudyEvent.EVENT_TYPE_MOTION);
            mStudyIds.add(StudyEvent.EVENT_TYPE_TIME);

            getRepository(getActivity()).getStudyEvents(mStudyIds, todayStart.getTime(), todayEnd.getTime()).observe(this, new Observer<List<StudyEvent>>() {
                @Override
                public void onChanged(List<StudyEvent> studyEvents) {

                    if (dataHasChanged)
                        return;

                    dataHasChanged = true;

                    String label = getString(R.string.title_time_tracked);//getString(R.string.label_steps);
                    long[] xvals = new long[24];
                    float[] yvals = new float[24];

                    for (int i = 0; i < xvals.length; i++) {
                        xvals[i] = i;
                        yvals[i] = 0;
                    }

                    for (StudyEvent event : studyEvents) {
                        Date dateEvent = new Date(event.getTimestamp());
                        int idx = dateEvent.getHours();
                        xvals[idx] = idx;
                        yvals[idx] += Long.parseLong(event.getEventValue())/60000f;
                    }

                    //long minTotal = TimeUnit.MINUTES.convert(todayEnd.getTime() - todayStart.getTime(), TimeUnit.MILLISECONDS); //total minutes in a day

                    /**
                     String txtTotalTime = "0 mins";
                     if (studyEvents.size() > 1) {
                     minTotal = studyEvents.get(0).getTimestamp() - studyEvents.get(studyEvents.size() - 1).getTimestamp();
                     minTotal = TimeUnit.MINUTES.convert(minTotal, TimeUnit.MILLISECONDS);
                     txtTotalTime = minTotal + " mins";
                     }**/

                    long totalMsWorked = 0;

                    for (StudyEvent event: studyEvents)
                    {
                        totalMsWorked += Long.parseLong(event.getEventValue());

                    }


                    int hourTotal = (int)TimeUnit.HOURS.convert(totalMsWorked, TimeUnit.MILLISECONDS);
                    long minTotal = TimeUnit.MINUTES.convert(totalMsWorked, TimeUnit.MILLISECONDS)-TimeUnit.HOURS.toMinutes(TimeUnit.MILLISECONDS.toHours(totalMsWorked)); //total minutes in a day


                    float maxStepsPerHour = 60f;

                    ArrayList<Integer> colors = new ArrayList<>();
                    colors.add(getResources().getColor(R.color.holo_green_light));

                    BarResultFragment fragLine = BarResultFragment.newInstance(getString(R.string.title_study_results), label, xvals, yvals, 1f,"", maxStepsPerHour,colors);

                    // Begin the transaction
                    FragmentTransaction ft = getActivity().getSupportFragmentManager().beginTransaction();
                    // Replace the contents of the container with the new fragment
                    ft.replace(R.id.result_chart_time_fragment, fragLine);
                    // or ft.add(R.id.your_placeholder, new FooFragment());
                    // Complete the changes added above
                    ft.commit();
                    //findViewById(R.id.detected_activities).setVisibility(showActivities ? View.VISIBLE : View.GONE);

                    String chartInfoStr = hourTotal + " " + getString(R.string.insight_hours) + " " + minTotal + " " + getString(R.string.insight_just_mins);

                    ((TextView)view.findViewById(R.id.chart_info_time_title)).setText(getString(R.string.title_time_tracked));
                    ((TextView) view.findViewById(R.id.chart_info_time)).setText(chartInfoStr);

                }
            });

        }

        view.findViewById(R.id.detected_environment).setVisibility(showEnviron ? View.VISIBLE : View.GONE);

        if (showEnviron) {
            view.findViewById(R.id.detected_environment_action_button).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (PermissionManager.checkEnvironPermissions(getActivity(),REQUEST_PERMISSION))
                        sendAction(ACTION_ENVIRON_SNAPSHOT);
                }
            });


            getRepository(getActivity()).getStudyEvents(StudyEvent.EVENT_TYPE_TEMPERATURE, todayStart.getTime(), todayEnd.getTime()).observe(this, new Observer<List<StudyEvent>>() {
                @Override
                public void onChanged(List<StudyEvent> studyEvents) {
                    TextView tv = ((TextView)view.findViewById(R.id.detected_environment_temp_value));

                    float value = 0;
                    for (StudyEvent event : studyEvents)
                        value += Float.valueOf(event.getEventValue());

                    if (value > 0) {
                        value = value / studyEvents.size();

                        String dispValue = (int)value+"";
                        tv.setText(dispValue + " °C");
                    } else {
                        tv.setText(getString(R.string.sensor_not_available));
                    }
                }
            });

            getRepository(getActivity()).getStudyEvents(StudyEvent.EVENT_TYPE_LIGHT, todayStart.getTime(), todayEnd.getTime()).observe(this, new Observer<List<StudyEvent>>() {
                @Override
                public void onChanged(List<StudyEvent> studyEvents) {
                    TextView tv = ((TextView) view.findViewById(R.id.detected_environment_temp_light_value));

                    float value = 0;
                    for (StudyEvent event : studyEvents)
                        value += Float.valueOf(event.getEventValue());

                    value = value / studyEvents.size();

                    if (value > 0) {
                        value = value / studyEvents.size();

                        String dispValue = (int)value+"";
                        tv.setText(dispValue + " lux");
                    } else {
                        tv.setText(getString(R.string.sensor_not_available));
                    }

                    String txtTotalTime = "0 mins";
                    if (studyEvents.size() > 1) {
                        long diffInMillies = studyEvents.get(0).getTimestamp() - studyEvents.get(studyEvents.size() - 1).getTimestamp();
                        txtTotalTime = TimeUnit.MINUTES.convert(diffInMillies, TimeUnit.MILLISECONDS) + " mins";
                    }

                }
            });
        }
        else if (showEvent)
        {
            /**
            getRepository(getActivity()).getStudyEvents(StudyEvent.EVENT_TYPE_REPORT, todayStart.getTime(), todayEnd.getTime()).observe(this, new Observer<List<StudyEvent>>() {
                @Override
                public void onChanged(List<StudyEvent> studyEvents) {

                    TextView tv = view.findViewById(R.id.detected_events_summary);

                    StringBuffer sb = new StringBuffer();

                    for (StudyEvent event : studyEvents)
                    {
                        StringTokenizer st = new StringTokenizer(event.getEventValue(),",");

                        while (st.hasMoreTokens())
                            sb.append(st.nextToken()).append("\n");

                        sb.append("________________\n");
                    }

                    tv.setText(sb.toString());
                }
            });**/

        }

        getRepository(getActivity()).getStudyEvents("environ_sound_max_amplitude", todayStart.getTime(), todayEnd.getTime()).observe(this, new Observer<List<StudyEvent>>() {
            @Override
            public void onChanged(List<StudyEvent> studyEvents) {
                TextView tv = ((TextView)view.findViewById(R.id.detected_environment_temp_sound_value));

                float value = 0;
                for (StudyEvent event : studyEvents)
                    value += Float.valueOf(event.getEventValue());

                value = value / studyEvents.size();

                if (value > 0) {
                    value = value / studyEvents.size();
                    String dispValue = (int)value+"";

                    tv.setText(dispValue + " db");
                }
                else
                {
                    tv.setText (getString(R.string.sensor_not_available));
                }

                /**
                String label = "sound";
                long[] xvals = new long[studyEvents.size()];
                float[] yvals = new float[studyEvents.size()];

                int i = 0;
                for (StudyEvent event: studyEvents)
                {
                    xvals[i] = event.getTimestamp();
                    yvals[i] = Float.valueOf(event.getEventValue());
                    i++;
                }

                LineResultFragment fragLine = LineResultFragment.newInstance("Sound", label, xvals, yvals);

                // Begin the transaction
                FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
                // Replace the contents of the container with the new fragment
                ft.replace(R.id.result_chart_sound, fragLine);
                // or ft.add(R.id.your_placeholder, new FooFragment());
                // Complete the changes added above
                ft.commit();
                **/
                //findViewById(R.id.detected_activities).setVisibility(showActivities ? View.VISIBLE : View.GONE);

            }
        });



        view.findViewById(R.id.detected_activities_apps).setVisibility(showApps ? View.VISIBLE : View.GONE);

        if (showApps)
        {
            mFragApps = FragmentAppList.newInstance();

            boolean showApps = true;

            if (showApps)
            {
                final LiveData<List<StudyEvent>> data = getRepository(getActivity()).getStudyEvents(StudyEvent.EVENT_TYPE_APP_USAGE, todayStart.getTime(), todayEnd.getTime());

                dataHasChanged = false;

                data.observe(this, new Observer<List<StudyEvent>>() {
                    @Override
                    public void onChanged(List<StudyEvent> studyEvents) {

                        if (dataHasChanged)
                            return;

                        dataHasChanged = true;

                        PackageManager pm = getContext().getPackageManager();

                        String label = getString(R.string.app_usage);

                        String tordAppString = PreferenceManager.getDefaultSharedPreferences(getContext())
                                .getString(PREFS_KEY_APP_ENABLED, "");
                        StringTokenizer st = new StringTokenizer(tordAppString,"|");
                        ArrayList<String> appPkgList = new ArrayList<>();
                        while (st.hasMoreTokens())
                            appPkgList.add(st.nextToken());


                        colors = new ArrayList<>();

                        long[] xvals = new long[24];
                        HashMap<String,Long> perAppTotalTime = new HashMap<>();
                        HashMap<Integer,HashMap<String,Float>> perAppPerHourTotals = new HashMap<>();

                        for (int i = 0; i < xvals.length; i++) {
                            xvals[i] = i;
                            HashMap<String,Float> perAppHour = new HashMap<>();
                            for (String pkgId : appPkgList)
                            {
                                perAppHour.put(pkgId,0f);
                            }
                            perAppPerHourTotals.put(i,perAppHour);
                        }

                        long totalTimeUsage = 0;

                        for (StudyEvent event : studyEvents) {

                            st = new StringTokenizer(event.getEventValue(), ",");
                            String appPkg = st.nextToken();

                            if (!appPkgList.contains(appPkg))
                                continue;

                            long totalTime = -1;
                            long lastTimeUsed = -1;
                            long startTime = -1;
                            long endTime = -1;

                            if (st.hasMoreTokens()) {
                                totalTime = Long.parseLong(st.nextToken());
                                lastTimeUsed = Long.parseLong(st.nextToken());
                                startTime = Long.parseLong(st.nextToken());
                                endTime = Long.parseLong(st.nextToken());
                            }
                            else
                            {
                                totalTime = 30000;
                                lastTimeUsed = event.getTimestamp();
                                startTime = event.getTimestamp()-totalTime;
                                endTime = lastTimeUsed;
                            }

                            Date dateEvent = new Date(lastTimeUsed);

                            if (dateEvent.before(todayStart))
                                continue;

                            int idx = dateEvent.getHours();
                            xvals[idx] = idx;

                            HashMap<String, Float> perAppPerHour = perAppPerHourTotals.get(idx);
                            if (perAppPerHour == null) {
                                perAppPerHour = new HashMap<>();
                                perAppPerHourTotals.put(idx,perAppPerHour);
                            }

                            if (!perAppTotalTime.containsKey(appPkg)) {
                                perAppTotalTime.put(appPkg, totalTime);
                                totalTimeUsage += totalTime;

                                //  yvals[idx]++;
                                if (!perAppPerHour.containsKey(appPkg))
                                    perAppPerHour.put(appPkg,(float)totalTime);
                                else {
                                    perAppPerHour.put(appPkg, perAppPerHour.get(appPkg)+totalTime);
                                }

                            } else {

                                perAppTotalTime.put(appPkg, perAppTotalTime.get(appPkg) + totalTime);
                                totalTimeUsage += totalTime;
                                //yvals[idx]++;
                                if (!perAppPerHour.containsKey(appPkg))
                                    perAppPerHour.put(appPkg,(float)totalTime);
                                else {
                                    perAppPerHour.put(appPkg, perAppPerHour.get(appPkg)+totalTime);
                                }

                            }


                        }




                        float[][] yVals = new float[24][];
                        String[] yLabels = new String[appPkgList.size()];

                        int i = 0;
                        for (String pkgId : appPkgList)
                        {
                            String appName = pkgId;
                            try {
                                ApplicationInfo app = pm.getApplicationInfo(pkgId, 0);
                                appName = pm.getApplicationLabel(app).toString();
                            } catch (PackageManager.NameNotFoundException e) {
                                e.printStackTrace();
                            }

                            if (perAppTotalTime.containsKey(pkgId)) {
                                long appTotalTimeMins = TimeUnit.MINUTES.convert(perAppTotalTime.get(pkgId), TimeUnit.MILLISECONDS);
                                //appName += " (" + appTotalTimeMins + getString(R.string.label_mins) + ")";

                                mFragApps.setAppLabel(pkgId,appName + "\n" + appTotalTimeMins + getString(R.string.label_mins));

                            }


                            yLabels[i++] = appName;
                            colors.add(AppColorGenerator.getColorForApp(getActivity(),pkgId));
                        }


                        for (i = 0; i < perAppPerHourTotals.size(); i++)
                        {
                            HashMap<String,Float> perAppHour = perAppPerHourTotals.get(i);
                            yVals[i] = new float[perAppHour.size()];

                            for (int n = 0; n < appPkgList.size();n++)
                            {
                                String appKey = appPkgList.get(n);
                                float perAppHourValue = perAppHour.get(appKey);
                                yVals[i][n] = perAppHourValue;
                            //    Log.d("AppUsage",appKey + ": " + i + "x" + n + " = " + perAppHourValue);
                            }
                        }

                        float maxMinPerHour = 60f*60000f;

                        BarResultFragment fragChart =
                                BarResultFragment.newInstance(getString(R.string.title_study_results), label, xvals, yVals, yLabels, 60000f, getString(R.string.label_mins), maxMinPerHour,colors);


                        // Begin the transaction
                        FragmentTransaction ft = getActivity().getSupportFragmentManager().beginTransaction();

                        ft.replace(R.id.select_apps_fragment, mFragApps);

                        // Replace the contents of the container with the new fragment
                        ft.replace(R.id.result_chart_apps_fragment, fragChart);
                        // or ft.add(R.id.your_placeholder, new FooFragment());
                        // Complete the changes added above
                        ft.commit();



                        showDeviceUsageSummary ();



                    }
                });
            }
            else {

                //show screen time
                float maxMinPerHour = 60000f;


                getRepository(getActivity()).getStudyEvents(StudyEvent.EVENT_TYPE_SCREEN, todayStart.getTime(), todayEnd.getTime()).observe(this, new Observer<List<StudyEvent>>() {
                    @Override
                    public void onChanged(List<StudyEvent> studyEvents) {


                        String label = "screen";
                        long[] xvals = new long[24];
                        float[] yvals = new float[24];


                        for (int i = 0; i < xvals.length; i++) {
                            xvals[i] = i;
                            yvals[i] = 0;
                        }

                        for (StudyEvent event : studyEvents) {
                            Date dateEvent = new Date(event.getTimestamp());
                            int idx = dateEvent.getHours();
                            xvals[idx] = idx;
                            yvals[idx] += 1;
                        }

                        long minTotal = TimeUnit.MINUTES.convert(todayEnd.getTime() - todayStart.getTime(), TimeUnit.MILLISECONDS); //total minutes in a day

                        /**
                        String txtTotalTime = "0 " + getString(R.string.label_mins);
                        if (studyEvents.size() > 1) {
                            minTotal = studyEvents.get(0).getTimestamp() - studyEvents.get(studyEvents.size() - 1).getTimestamp();
                            minTotal = TimeUnit.MINUTES.convert(minTotal, TimeUnit.MILLISECONDS);
                            txtTotalTime = minTotal + ' ' + getString(R.string.label_mins);;
                        }
                        int minMoved = 0;
                        long lastTs = 0;

                        for (StudyEvent event : studyEvents) {
                            long diff = lastTs - event.getTimestamp();

                            if (diff >= 60000)
                                minMoved++;

                            lastTs = event.getTimestamp();

                        }**/


                        ArrayList<Integer> colors = new ArrayList<>();
                        colors.add(getResources().getColor(R.color.holo_green_light));

                        BarResultFragment fragLine = BarResultFragment.newInstance(getString(R.string.title_study_results), label, xvals, yvals, 30000f, getString(R.string.label_mins), maxMinPerHour, colors);

                        // Begin the transaction
                        FragmentTransaction ft = getActivity().getSupportFragmentManager().beginTransaction();

                        ft.replace(R.id.select_apps_fragment, mFragApps);

                        // Replace the contents of the container with the new fragment
                        ft.replace(R.id.result_chart_apps_fragment, fragLine);
                        // or ft.add(R.id.your_placeholder, new FooFragment());
                        // Complete the changes added above
                        ft.commit();
                        //findViewById(R.id.detected_activities).setVisibility(showActivities ? View.VISIBLE : View.GONE);

                        //   String chartInfoStr = minMoved + " " + getString(R.string.minutes_label);

                        // ((TextView) view.findViewById(R.id.chart_info)).setText(chartInfoStr);

                    }
                });
            }

        }

        view.findViewById(R.id.detected_location).setVisibility(showGeo||showCommute ? View.VISIBLE : View.GONE);

        view.findViewById(R.id.detected_activities_walking).setVisibility((getActivity().getIntent().getBooleanExtra("track_location",true)?View.VISIBLE : View.GONE));

        boolean trackActivity = getActivity().getIntent().getBooleanExtra("track_activity",true);
        if (trackActivity) {
            view.findViewById(R.id.detected_activities_walking).setVisibility((getActivity().getIntent().getBooleanExtra("track_walking", true) ? View.VISIBLE : View.GONE));
            view.findViewById(R.id.detected_activities_still).setVisibility((getActivity().getIntent().getBooleanExtra("track_still", true) ? View.VISIBLE : View.GONE));
        }
        else
        {
            view.findViewById(R.id.detected_activities_walking).setVisibility(View.GONE);
            view.findViewById(R.id.detected_activities_still).setVisibility(View.GONE);

        }

        view.findViewById(R.id.detected_events).setVisibility(showEvent ? View.VISIBLE : View.GONE);

        if (showEvent)
        {
            view.findViewById(R.id.detected_events_add_work_journal).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    Intent intent = new Intent(getActivity(),SurveyActivity.class);
                    intent.putExtra("survey","work");
                    startActivity(intent);
                }
            });

            view.findViewById(R.id.detected_events_action_button).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    TextView tvEvent = view.findViewById(R.id.detected_events_what);
                    Spinner spinnerEvent = view.findViewById(R.id.spinner_events);

                    TrackingService.logEvent(getActivity(),StudyEvent.EVENT_TYPE_REPORT,spinnerEvent.getSelectedItem() + ": " + tvEvent.getText());

                    tvEvent.setText("");
                    spinnerEvent.setSelection(0);

                    Snackbar.make(view.findViewById(R.id.status_list),getString(R.string.status_event_logged),Snackbar.LENGTH_SHORT).show();
                }
            });
        }

        if (showGeo)
            setupMap();

        if (showCommute) {
            setupMap();
            TextView tvCommuteInfo = view.findViewById(R.id.commute_info);
            tvCommuteInfo.setVisibility(View.GONE);

        }

    }

    private void showDeviceUsageSummary ()
    {


        StudySummary.getTotalAppUsageTimeStatic(getActivity(), todayStart.getTime(), todayEnd.getTime(), new StudySummary.StudySummaryListener() {
            @Override
            public void handleResult(final String appUsageResult) {

                StudySummary.getTotalScreenTimeStatic(getActivity(), todayStart.getTime(), todayEnd.getTime(), new StudySummary.StudySummaryListener() {
                    @Override
                    public void handleResult(final String screenOnResult) {


                        StudySummary.getTotalScreenOnEventsStatic(getActivity(), todayStart.getTime(), todayEnd.getTime(), new StudySummary.StudySummaryListener() {
                            @Override
                            public void handleResult(String screenCheckResult) {

                                TextView tvResultApps = ((TextView) view.findViewById(R.id.result_chart_apps_info_one));
                                tvResultApps.setText("");

                                StringBuffer sb = new StringBuffer();
                                sb.append("<h2>").append(appUsageResult).append("</h2>");
                                sb.append(getString(R.string.label_total_app_usage));
                                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                                    tvResultApps.setText(Html.fromHtml(sb.toString(), Html.FROM_HTML_MODE_COMPACT));
                                } else {
                                    tvResultApps.setText(Html.fromHtml(sb.toString()));
                                }

                                tvResultApps = ((TextView) view.findViewById(R.id.result_chart_apps_info_two));
                                tvResultApps.setText("");
                                sb = new StringBuffer();
                                sb.append("<h2>").append(screenOnResult).append("</h2>");
                                sb.append(getString(R.string.label_total_screen_on_time));
                                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                                    tvResultApps.setText(Html.fromHtml(sb.toString(), Html.FROM_HTML_MODE_COMPACT));
                                } else {
                                    tvResultApps.setText(Html.fromHtml(sb.toString()));
                                }

                                tvResultApps = ((TextView) view.findViewById(R.id.result_chart_apps_info_three));
                                tvResultApps.setText("");
                                sb = new StringBuffer();
                                sb.append("<h2>").append(screenCheckResult).append("</h2>");
                                sb.append(getString(R.string.label_total_screen_on_checks));
                                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                                    tvResultApps.setText(Html.fromHtml(sb.toString(), Html.FROM_HTML_MODE_COMPACT));
                                } else {
                                    tvResultApps.setText(Html.fromHtml(sb.toString()));
                                }





                            }

                            @Override
                            public void handleResult(int total) {
                                //total minutes
                                //gCommute.setValue(total);
                            }
                        });

                    }

                    @Override
                    public void handleResult(int total) {
                        //total minutes
                        //gCommute.setValue(total);
                    }
                });


            }

            @Override
            public void handleResult(int total) {
                //total minutes
                //gCommute.setValue(total);
            }
        });

    }


    MapFragment mapFragment;

    private void setupMap ()
    {
        //showApps means commute tracking

     //   final SupportMapFragment mapFragment = (SupportMapFragment) getActivity().getSupportFragmentManager()
       //         .findFragmentById(R.id.map);

        view.findViewById(R.id.mapbox).setVisibility(View.VISIBLE);

        view.findViewById(R.id.geocontrols).setVisibility(showCommute ? View.VISIBLE:View.GONE);

        if (mapFragment == null) {
            mapFragment = (MapFragment) getActivity().getFragmentManager().findFragmentById(R.id.map);


            if (mapFragment != null) {

                mapFragment.getView().getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
                    @Override
                    public void onGlobalLayout() {

                        updateMapData();

                    }
                });
            }
        } else {
            updateMapData();
        }

    }

    private void updateMapData () {

        if (mapSetup)
            return;

        if (mGoogleMap == null) {
            mapFragment.getMapAsync(new OnMapReadyCallback() {
                @Override
                public void onMapReady(final GoogleMap googleMap) {

                    mGoogleMap = googleMap;
                    updateMapDataInternal ();
                }
            });
        }
        else
        {
            updateMapDataInternal ();
        }
    }

    private void updateMapDataInternal ()
    {
        llList = new ArrayList<>();
        final float geoAccuracyFilter = Prefs.getGeoAccuracyFilter(getActivity());

        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getActivity());

        if (showCommute)
        {

        }
        else if (showGeo) {
            mGoogleMap.setOnCircleClickListener(new GoogleMap.OnCircleClickListener() {
                @Override
                public void onCircleClick(Circle circle) {

                    long eventTime = (long) circle.getTag();

                    Date dateTime = new Date(eventTime);

                    DateFormat df = SimpleDateFormat.getTimeInstance();
                    String time = df.format(dateTime);

                    mGoogleMap.addMarker(new MarkerOptions().position(circle.getCenter())
                            .title(time)).showInfoWindow();


                }
            });

            mGoogleMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
                @Override
                public boolean onMarkerClick(Marker marker) {
                    marker.setVisible(false);
                    return false;
                }
            });



            switchHeatMap.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                    mTileHeatMap.setVisible(isChecked);


                }
            });


        }

        if (!mapSetup) {

            mapSetup = true;

            if (showCommute)
            {


                StudySummary.getAllCommuteTimes(getActivity(), getActivity(), todayStart.getTime(), todayEnd.getTime(), new StudySummary.StudySummaryListener() {
                    @Override
                    public void handleResult(String result) {

                        String commuteTime = result;

                        ((TextView) view.findViewById(R.id.location_info_title)).setText(getString(R.string.title_commute_times));
                        ((TextView) view.findViewById(R.id.location_info)).setText(commuteTime);

                    }

                    @Override
                    public void handleResult(int total) {
                        //total minutes
                        //gCommute.setValue(total);
                    }
                });

                final Location locationHome = new Location("");
                String geoKey = GEOKEY_HOME;
                if (prefs.getFloat("geo-" + geoKey + "-lat", -1) != -1) {
                    float geofenceRadMeters = 100f; //30meters
                    double geoLatitude = (double) prefs.getFloat("geo-" + geoKey + "-lat", -1);
                    double geoLong = (double) prefs.getFloat("geo-" + geoKey + "-long", -1);

                    locationHome.setLatitude(geoLatitude);
                    locationHome.setLongitude(geoLong);

                }

                final Location locationWork = new Location("");

                geoKey = GEOKEY_WORK;
                if (prefs.getFloat("geo-" + geoKey + "-lat", -1) != -1) {
                    float geofenceRadMeters = 100f; //30meters
                    double geoLatitude = (double) prefs.getFloat("geo-" + geoKey + "-lat", -1);
                    double geoLong = (double) prefs.getFloat("geo-" + geoKey + "-long", -1);

                    locationWork.setLatitude(geoLatitude);
                    locationWork.setLongitude(geoLong);
                }

                getRepository(getActivity()).getStudyEvents(StudyEvent.EVENT_TYPE_GEO_FENCE, todayStart.getTime(), todayEnd.getTime()).observe(getActivity(), new Observer<List<StudyEvent>>() {
                    @Override
                    public void onChanged(List<StudyEvent> studyEvents) {

                        mGoogleMap.clear();

                        if (studyEvents.size() > 0) {

                            mapSetup = true;
                            float totalDistanceInMeters = 0;
                            Location lastLoc = null;
                            boolean addedPoint = false;

                            final LatLngBounds.Builder builder = LatLngBounds.builder();
                            //    PolylineOptions pLine = new PolylineOptions();
                            //    pLine.color(getResources().getColor(R.color.map_circle_color));

                            for (StudyEvent event : studyEvents) {
                                Location location = new Location("");

                                String markerTitle = "";

                                //String[] locParts = event.getEventValue().split(",");
                                if (event.getEventValue().contains("home")) {
                                    location = locationHome;
                                    markerTitle = "home";
                                }
                                else if (event.getEventValue().contains("work")) {
                                    location = locationWork;
                                    markerTitle = "work";
                                }


                                float accuracy = location.getAccuracy();


                                String geoKey = new Date(event.getTimestamp()).toString();

                                // Add a marker in Sydney, Australia,
                                // and move the map's camera to the same location.
                                LatLng newLoc = new LatLng(location.getLatitude(), location.getLongitude());
                                Marker marker = mGoogleMap.addMarker(new MarkerOptions().position(newLoc).visible(true));
                                marker.setTag(event.getTimestamp());

                                Date dateTime = new Date(event.getTimestamp());
                                DateFormat df = SimpleDateFormat.getTimeInstance(SimpleDateFormat.SHORT);
                                String time = df.format(dateTime);

                                marker.setTitle(markerTitle);
                                marker.showInfoWindow();

                                builder.include(newLoc);
                                addedPoint = true;
                                //    pLine.add(newLoc);

                                if (lastLoc != null) {
                                    totalDistanceInMeters += lastLoc.distanceTo(location);
                                }

                                lastLoc = location;

                                Circle circle = mGoogleMap.addCircle(new CircleOptions()
                                        .center(newLoc)
                                        .radius(accuracy/2)
                                        .strokeColor(getResources().getColor(R.color.holo_red_dark))
                                        .fillColor(getResources().getColor(android.R.color.transparent)));

                                circle.setTag(event.getTimestamp());



                            }

                            //mGoogleMap.addPolyline(pLine);


                            if (addedPoint) {
                                mHandler.postDelayed(new Runnable() {
                                    public void run() {
                                        if (isAttached && view.findViewById(R.id.view_data).getVisibility() != View.GONE
                                        ) {
                                            if (studyEvents != null && studyEvents.size() > 0 && (!mapZoomed)) {

                                                LatLngBounds lBounds = builder.build();

                                                int width = getResources().getDisplayMetrics().widthPixels;
                                                int height = getResources().getDisplayMetrics().heightPixels;
                                                int padding = (int) (width * .32); // offset from edges of the map 12% of screen

                                                CameraUpdate cu = CameraUpdateFactory.newLatLngBounds(lBounds, width, height, padding);

                                                mGoogleMap.animateCamera(cu, new GoogleMap.CancelableCallback() {
                                                    @Override
                                                    public void onFinish() {

                                                        // float newZoom = mGoogleMap.getCameraPosition().zoom-2f;
                                                        // mGoogleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(lBounds.getCenter(), newZoom));
                                                    }

                                                    @Override
                                                    public void onCancel() {

                                                    }
                                                });

                                                mapZoomed = true;


                                            }

                                        }

                                    }
                                }, 1500);
                            }


                        }
                    }
                });
            }
            else if (showGeo) {

                Toast.makeText(getActivity(),getString(R.string.status_updating_map),Toast.LENGTH_LONG).show();

                getRepository(getActivity()).getStudyEvents(StudyEvent.EVENT_TYPE_GEO_LOGGING, todayStart.getTime(), todayEnd.getTime()).observe(getActivity(), new Observer<List<StudyEvent>>() {
                    @Override
                    public void onChanged(List<StudyEvent> studyEvents) {

                        if (dataHasChanged)
                            return;

                        dataHasChanged = true;

                        mGoogleMap.clear();

                        if (studyEvents.size() > 0) {

                            mapSetup = true;
                            float totalDistanceInMeters = 0;
                            Location lastLoc = null;
                            boolean addedPoint = false;

                            final LatLngBounds.Builder builder = LatLngBounds.builder();
                            PolylineOptions pLine = new PolylineOptions();
                            pLine.color(getResources().getColor(R.color.map_circle_color));

                            LatLng lastLatLong = null;

                            for (StudyEvent event : studyEvents) {
                                Location location = new Location("");

                                String[] locParts = event.getEventValue().split(",");
                                location.setLatitude(Double.parseDouble(locParts[0]));
                                location.setLongitude(Double.parseDouble(locParts[1]));

                                float accuracy = Float.parseFloat(locParts[2]);

                                if (accuracy < geoAccuracyFilter) {

                                    String geoKey = new Date(event.getTimestamp()).toString();

                                    // and move the map's camera to the same location.
                                    LatLng newLoc = new LatLng(location.getLatitude(), location.getLongitude());
                                    //  mGoogleMap.addMarker(new MarkerOptions().position(newLoc).visible(false));
                                    builder.include(newLoc);
                                    addedPoint = true;
                                    pLine.add(newLoc);


                                    if (lastLoc != null) {
                                        float lastDistance = lastLoc.distanceTo(location);
                                        totalDistanceInMeters += lastDistance;

                                        /**
                                         if (lastLatLong != null && lastDistance > 40f) {
                                         Double HeadingRotation = SphericalUtil.computeHeading(lastLatLong, newLoc);
                                         Marker m = mGoogleMap.addMarker(new MarkerOptions().position(newLoc)
                                         .icon(getEndCapIcon(getActivity(),R.color.holo_blue_bright))
                                         .rotation(HeadingRotation.floatValue()).visible(true));
                                         m.setTag(event.getTimestamp());
                                         }**/

                                    }



                                    lastLoc = location;
                                    lastLatLong = newLoc;

                                    llList.add(lastLatLong);

                                    Circle circle = mGoogleMap.addCircle(new CircleOptions()
                                            .center(newLoc)
                                            .radius(accuracy/2)
                                            .strokeColor(getResources().getColor(android.R.color.transparent))
                                            .fillColor(getResources().getColor(R.color.very_transparent_blue)));

                                    circle.setTag(event.getTimestamp());

                                    circle.setClickable(true);


                                }


                            }

                            mGoogleMap.addPolyline(pLine);


                            if (addedPoint) {

                                // Create a heat map tile provider, passing it the latlngs.
                                HeatmapTileProvider prov = new HeatmapTileProvider.Builder()
                                        .data(llList)
                                        .opacity(0.3f)
                                        .build();

                                // Add a tile overlay to the map, using the heat map tile provider.
                                TileOverlayOptions tOpts = new TileOverlayOptions().tileProvider(prov);
                                tOpts.visible(false);
                               mTileHeatMap = mGoogleMap.addTileOverlay(tOpts);


                                mHandler.postDelayed(new Runnable() {
                                    public void run() {
                                        if (isAttached && view.findViewById(R.id.view_data).getVisibility() != View.GONE
                                        ) {
                                            if (studyEvents != null && studyEvents.size() > 0 && (!mapZoomed)) {

                                                LatLngBounds lBounds = builder.build();

                                                int width = getResources().getDisplayMetrics().widthPixels;
                                                int height = getResources().getDisplayMetrics().heightPixels;
                                                int padding = (int) (width * .32); // offset from edges of the map 12% of screen

                                                CameraUpdate cu = CameraUpdateFactory.newLatLngBounds(lBounds, width, height, padding);

                                                mGoogleMap.animateCamera(cu, new GoogleMap.CancelableCallback() {
                                                    @Override
                                                    public void onFinish() {

                                                        //   float newZoom = mGoogleMap.getCameraPosition().zoom-2f;
                                                        //  mGoogleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(lBounds.getCenter(), newZoom));
                                                    }

                                                    @Override
                                                    public void onCancel() {

                                                    }
                                                });

                                                mapZoomed = true;


                                            }

                                        }

                                    }
                                }, 1000);
                            }


                            String distanceStr = "";

                            if (UnitLocale.getDefault() == UnitLocale.Metric) {

                                if (totalDistanceInMeters < 1000)
                                    distanceStr = ((int) totalDistanceInMeters) + " " + getString(R.string.label_meters);
                                else
                                    distanceStr = ((int) totalDistanceInMeters / 1000) + getString(R.string.label_km);
                            } else {
                                double miles = UnitLocale.getDefault().getMiles(totalDistanceInMeters);
                                distanceStr = String.format("%.2f", miles) + " " + getString(R.string.label_miles);
                            }

                            ((TextView) view.findViewById(R.id.location_info)).setText(distanceStr);


                        }
                    }
                });
            }

        }
    }

    /**
     * Return a BitmapDescriptor of an arrow endcap icon for the passed color.
     *
     * @param context - a valid context object
     * @param color - the color to make the arrow icon
     * @return BitmapDescriptor - the new endcap icon
     */
    public BitmapDescriptor getEndCapIcon(Context context, int color) {

        // mipmap icon - white arrow, pointing up, with point at center of image
        // you will want to create:  mdpi=24x24, hdpi=36x36, xhdpi=48x48, xxhdpi=72x72, xxxhdpi=96x96
        Drawable drawable = ContextCompat.getDrawable(context, R.drawable.arrow);

        // set the bounds to the whole image (may not be necessary ...)
        drawable.setBounds(0, 0, drawable.getIntrinsicWidth(), drawable.getIntrinsicHeight());

        // overlay (multiply) your color over the white icon
        drawable.setColorFilter(color, PorterDuff.Mode.MULTIPLY);

        // create a bitmap from the drawable
        android.graphics.Bitmap bitmap = android.graphics.Bitmap.createBitmap(drawable.getIntrinsicWidth(),
                drawable.getIntrinsicHeight(), Bitmap.Config.ARGB_8888);

        // render the bitmap on a blank canvas
        Canvas canvas = new Canvas(bitmap);
        drawable.draw(canvas);

        // create a BitmapDescriptor from the new bitmap
        return BitmapDescriptorFactory.fromBitmap(bitmap);
    }



    private void showData ()
    {

        Intent intent = new Intent(getActivity(), StudyResultsActivity.class);
        intent.putExtra("studyId",studyId);

        startActivity(intent);

    }




    @Override
    public void onResume() {
        super.onResume();

        mapSetup = false;

        TextView tvWaypoints = view.findViewById(R.id.waypoints_info);

        StringBuffer sb = new StringBuffer();
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getActivity());
        if (prefs.contains("geo-home-address"))
        {
            sb.append(getString(R.string.home)).append(": ");
            sb.append(prefs.getString("geo-home-address",""));
            sb.append("\n\n");
        }

        if (prefs.contains("geo-work-address"))
        {
            sb.append(getString(R.string.work)).append(": ");
            sb.append(prefs.getString("geo-work-address",""));
        }

        tvWaypoints.setText(sb.toString());

        initCalendar(mCal,0);

    }



    /**
     * Registers for activity recognition updates using
     * Registers success and failure callbacks.
     */
    public void startStudy() {
        Intent intent = new Intent(getActivity(), TrackingService.class);
        intent.setAction(TrackingService.ACTION_START_TRACKING);
        intent.putExtra("studyid",studyId);
        ContextCompat.startForegroundService(getActivity(),intent);
    }

    /**
     * Registers for activity recognition updates using
     * Registers success and failure callbacks.
     */
    public void sendAction(String action) {
        Intent intent = new Intent(getActivity(), TrackingService.class);
        intent.setAction(action);
        intent.putExtra("studyid",studyId);
        ContextCompat.startForegroundService(getActivity(),intent);
    }


    /**
     * Removes activity recognition updates using
     * failure callbacks.
     */
    public void stopStudy () {

        Intent intent = new Intent(getActivity(),TrackingService.class);
        intent.setAction(TrackingService.ACTION_STOP_TRACKING);
        intent.putExtra("studyid",studyId);
        getActivity().startService(intent);

        setStudyEnabled(false);
        view.findViewById(R.id.view_get_started).setVisibility(View.VISIBLE);

    }

    public void resetStudy ()
    {

    }

    /**
     * Retrieves the boolean from SharedPreferences that tracks whether we are requesting activity
     * updates.
     */
    private boolean isStudyEnabled() {
        return PreferenceManager.getDefaultSharedPreferences(getActivity())
                .getBoolean(Constants.KEY_ACTIVITY_UPDATES_REQUESTED + "_" + studyId, false);
    }

    private void setStudyEnabled(boolean enabled) {
        PreferenceManager.getDefaultSharedPreferences(getActivity()).edit()
                .putBoolean(Constants.KEY_ACTIVITY_UPDATES_REQUESTED + "_" + studyId, enabled).commit();

        studyListing.isEnabled = enabled;

        if (studyListing.isEnabled)
        {
            if (studyListing.trackApps)
                PermissionManager.enableUsageTracking(getActivity());
            else if (studyListing.trackGeo)
                PermissionManager.checkLocationPermissions(getActivity(),REQUEST_PERMISSION);
            else if (studyListing.trackActivities)
                PermissionManager.checkStepPermission(getActivity(),REQUEST_PERMISSION);
        }

    }


    private boolean checkForPermission(Context context) {
        AppOpsManager appOps = (AppOpsManager) context.getSystemService(Context.APP_OPS_SERVICE);
        int mode = appOps.checkOpNoThrow(OPSTR_GET_USAGE_STATS,myUid(), context.getPackageName());
        return mode == MODE_ALLOWED;
    }

    public void viewDataClicked (View view)
    {
        showData();
    }

    public void setHomeClicked (View view)
    {
      //  Intent intent = new Intent(this, TrackingService.class);
      //  intent.setAction(TrackingService.ACTION_SET_GEO_HOME);
       // startService(intent);
        initLocationPicker (MAP_HOME_REQUEST_CODE);
    }


    public void setWorkClicked (View view)
    {
        //Intent intent = new Intent(this, TrackingService.class);
        //intent.setAction(TrackingService.ACTION_SET_GEO_WORK);
        //startService(intent);
        initLocationPicker (MAP_WORK_REQUEST_CODE);
    }

    public void chooseAppsClicked (View view)
    {
        startActivityForResult(new Intent(getActivity(),AppManagerActivity.class),APPS_REQUEST_CODE);
    }

    public static final int MAP_HOME_REQUEST_CODE = 1001;
    public static final int MAP_WORK_REQUEST_CODE = MAP_HOME_REQUEST_CODE+1;
    public static final int APPS_REQUEST_CODE = MAP_WORK_REQUEST_CODE+1;

    private void initLocationPicker (int requestCode)
    {
        Intent locationPickerIntent = new LocationPickerActivity.Builder()
                .withDefaultLocaleSearchZone()
                .shouldReturnOkOnBackPressed()
                .withSatelliteViewHidden()
                .withGooglePlacesEnabled()
                .withGoogleTimeZoneEnabled()
                .withVoiceSearchHidden()
                .withUnnamedRoadHidden()
                .build(getActivity());

        getActivity().startActivityForResult(locationPickerIntent, requestCode);
    }


    private int REQUEST_PERMISSION = 1234;




    public FragmentAppList getAppListFragment ()
    {
        return mFragApps;
    }

    public void sendAction(String studyId, String action) {
        Intent intent = new Intent(getActivity(), TrackingService.class);
        intent.setAction(action);
        intent.putExtra("studyid",studyId);
        ContextCompat.startForegroundService(getActivity(),intent);
    }

    public static Bitmap loadBitmapFromView(View v) {
        Bitmap b = Bitmap.createBitmap( v.getLayoutParams().width, v.getLayoutParams().height, Bitmap.Config.ARGB_8888);
        Canvas c = new Canvas(b);
        v.layout(v.getLeft(), v.getTop(), v.getRight(), v.getBottom());
        v.draw(c);
        return b;
    }
}