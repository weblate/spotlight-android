package org.uniglobalunion.spotlight.security;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by aleksandr on 2018/02/09.
 */

public class PreferencesSettings {

    private static final String PREF_FILE = "settings_pref";

    public static void saveToPref(Context context,String key, String value) {
        final SharedPreferences sharedPref = context.getSharedPreferences(PREF_FILE, Context.MODE_PRIVATE);
        final SharedPreferences.Editor editor = sharedPref.edit();
        editor.putString(key,value);
        editor.apply();
    }

    public static String getPref(Context context, String key) {
        final SharedPreferences sharedPref = context.getSharedPreferences(PREF_FILE, Context.MODE_PRIVATE);
        final String defaultValue = "";
        return sharedPref.getString(key, defaultValue);
    }

    public static void saveToPref(Context context,String key, boolean value) {
        final SharedPreferences sharedPref = context.getSharedPreferences(PREF_FILE, Context.MODE_PRIVATE);
        final SharedPreferences.Editor editor = sharedPref.edit();
        editor.putBoolean(key,value);
        editor.apply();
    }

    public static boolean getPrefBool(Context context, String key) {
        final SharedPreferences sharedPref = context.getSharedPreferences(PREF_FILE, Context.MODE_PRIVATE);
        final boolean defaultValue = false;
        return sharedPref.getBoolean(key, defaultValue);
    }

}
