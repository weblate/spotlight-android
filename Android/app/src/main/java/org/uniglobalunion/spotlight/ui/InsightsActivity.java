package org.uniglobalunion.spotlight.ui;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.cardview.widget.CardView;
import androidx.core.content.FileProvider;
import androidx.lifecycle.Observer;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;


import org.ocpsoft.prettytime.PrettyTime;
import org.uniglobalunion.spotlight.R;
import org.uniglobalunion.spotlight.SpotlightApp;
import org.uniglobalunion.spotlight.model.Study;
import org.uniglobalunion.spotlight.model.StudyEvent;
import org.uniglobalunion.spotlight.model.StudyListing;
import org.uniglobalunion.spotlight.model.StudyRepository;
import org.uniglobalunion.spotlight.model.StudyViewModel;
import org.uniglobalunion.spotlight.service.TrackingService;
import org.uniglobalunion.spotlight.util.SharingUtils;

import java.io.File;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.StringTokenizer;
import java.util.TimeZone;

import static org.uniglobalunion.spotlight.ui.StudyPickerActivity.getBitmapFromAsset;


public class InsightsActivity extends AppCompatActivity {

    private RecyclerView rv;

    private StudyListing mStudyListing;
    private ArrayList<String> mStudyIdFilter;

    Date dateStart, dateEnd;
    PrettyTime pTime;
    private SpotlightApp mApp;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_insights);
        Toolbar toolbar = findViewById(R.id.toolbar);

        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        setTitle(R.string.insight_activity_title);

        mApp = (SpotlightApp)getApplication();
        mStudyIdFilter = new ArrayList<>();
        mStudyIdFilter.add(StudyEvent.EVENT_TYPE_INSIGHT);

        initDates();

        rv = (RecyclerView)findViewById(R.id.rv);
        LinearLayoutManager llm = new LinearLayoutManager(this);
        rv.setLayoutManager(llm);

        initializeData();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        //MenuInflater inflater = getMenuInflater();
        //inflater.inflate(R.menu.study_result_menu, menu);

        return true;
    }



    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if (item.getItemId() == android.R.id.home)
        {
            finish();
            return true;
        }


        return super.onOptionsItemSelected(item);
    }


    public class RVAdapter extends RecyclerView.Adapter<RVAdapter.InsightViewHolder>{

        private List<StudyEvent> mResults;

        public RVAdapter (List<StudyEvent> results)
        {
            mResults = results;
        }

        public class InsightViewHolder extends RecyclerView.ViewHolder {
            CardView cv;
            TextView name;
            TextView desc;
            ImageView icon;

            InsightViewHolder(View itemView) {
                super(itemView);
                cv = itemView.findViewById(R.id.cv);
                name = itemView.findViewById(R.id.insight_name);
                desc = itemView.findViewById(R.id.insight_desc);
                icon = itemView.findViewById(R.id.insight_icon);
            }

        }

        @Override
        public int getItemCount() {
            return mResults.size();
        }

        @Override
        public InsightViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
            View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.study_card_insight, viewGroup, false);
            final InsightViewHolder pvh = new InsightViewHolder(v);
            pvh.cv.setDrawingCacheEnabled(true);
            pvh.cv.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    Bitmap vBitmap = SharingUtils.loadBitmapFromView(pvh.cv);
                    SharingUtils.shareBitmapWithText(viewGroup.getContext(),vBitmap,"", false);


                }
            });

            return pvh;
        }

        @Override
        public void onBindViewHolder(InsightViewHolder studyResultViewHolder, int i) {

            String[] tokens = mResults.get(i).getEventValue().split("\\|");
            String insightDesc = tokens[0];
            String studyId = "";

            if (tokens.length > 1)
                studyId = tokens[1];

            /**
            if (TextUtils.isEmpty(insightTitle)) {
                studyResultViewHolder.icon.setVisibility(View.GONE);
                studyResultViewHolder.name.setVisibility(View.GONE);
            }
            else {
                studyResultViewHolder.icon.setVisibility(View.VISIBLE);
                studyResultViewHolder.name.setVisibility(View.VISIBLE);
            }**/

            String insightTime = pTime.format(new Date(mResults.get(i).getTimestamp()));


            StudyListing study = mApp.getStudy(studyId);
            if (study != null) {
                String iconAssetPath = mApp.getStudy(studyId).iconAssetPath;

                if (iconAssetPath.startsWith("R.drawable")) {
                    int resId = getResources().getIdentifier(iconAssetPath.substring(iconAssetPath.lastIndexOf('.') + 1), "drawable", getPackageName());

                    if (resId > 0)
                        studyResultViewHolder.icon.setImageDrawable(getDrawable(resId));

                } else
                    studyResultViewHolder.icon.setImageBitmap(getBitmapFromAsset(InsightsActivity.this, iconAssetPath));


            }
            else
            {

                if (studyId.equals(StudyEvent.EVENT_TYPE_APP_USAGE))
                    studyResultViewHolder.icon.setImageResource(R.drawable.ic_screen_time_sensors);
                else if (studyId.equals(StudyEvent.EVENT_TYPE_COMMUTE))
                    studyResultViewHolder.icon.setImageResource(R.drawable.ic_walking);
                else if (studyId.equals(StudyEvent.EVENT_TYPE_SCREEN))
                    studyResultViewHolder.icon.setImageResource(R.drawable.ic_touch_app_black_24dp);
                else if (studyId.equals(StudyEvent.EVENT_TYPE_GEO_LOGGING))
                    studyResultViewHolder.icon.setImageResource(R.drawable.ic_distance_sensors);
                else
                    studyResultViewHolder.icon.setImageResource(R.mipmap.ic_launcher);
            }

            studyResultViewHolder.name.setText(insightDesc);
            studyResultViewHolder.desc.setText(insightTime);



        }

        @Override
        public void onAttachedToRecyclerView(RecyclerView recyclerView) {
            super.onAttachedToRecyclerView(recyclerView);
        }

    }

    // This method creates an ArrayList that has three Person objects
// Checkout the project associated with this tutorial on Github if
// you want to use the same images.
    private void initializeData(){

        StudyViewModel svm = new StudyViewModel(getApplication());
        svm.getStudyEvents(mStudyIdFilter, dateStart.getTime(), dateEnd.getTime()).observe(this, new Observer<List<StudyEvent>>() {
            @Override
            public void onChanged(List<StudyEvent> studyEvents) {

                RVAdapter adapter = new RVAdapter(studyEvents);
                rv.setAdapter(adapter);

            }
        });



    }

    private static StudyRepository sRepository;

    private static synchronized StudyRepository getRepository (Context context)
    {
        if (sRepository == null)
            sRepository = new StudyRepository(context);

        return sRepository;
    }

    private long baseTime = -1;

    private void initDates ()
    {

        pTime = new PrettyTime();

        Calendar cal = Calendar.getInstance();

        baseTime = getIntent().getLongExtra("studyDate",-1L);

        if (baseTime != -1L)
            cal.setTimeInMillis(baseTime);
        else
            baseTime = cal.getTimeInMillis();

        cal.set(Calendar.HOUR_OF_DAY, 0);
        cal.set(Calendar.MINUTE, 0);
        cal.add(Calendar.DAY_OF_MONTH,-3);
        dateStart = cal.getTime();

        cal.add(Calendar.DAY_OF_MONTH,+3);
        cal.set(Calendar.HOUR_OF_DAY, 23);
        cal.set(Calendar.MINUTE, 59);

        dateEnd = cal.getTime();

        /**
         * //this month
        cal.add(Calendar.DAY_OF_MONTH,-30);
        dateStart = cal.getTime();

        cal.add(Calendar.DAY_OF_MONTH,30);
        cal.set(Calendar.HOUR_OF_DAY, 23);
        cal.set(Calendar.MINUTE, 59);

        dateEnd = cal.getTime();**/


        initializeData();

    }


}
