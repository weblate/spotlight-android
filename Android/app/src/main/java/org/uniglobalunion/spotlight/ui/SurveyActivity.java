package org.uniglobalunion.spotlight.ui;

import android.app.AppOpsManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;
import com.quickbirdstudios.surveykit.AnswerFormat;
import com.quickbirdstudios.surveykit.FinishReason;
import com.quickbirdstudios.surveykit.NavigableOrderedTask;
import com.quickbirdstudios.surveykit.NavigationRule;
import com.quickbirdstudios.surveykit.OrderedTask;
import com.quickbirdstudios.surveykit.StepIdentifier;
import com.quickbirdstudios.surveykit.SurveyTheme;
import com.quickbirdstudios.surveykit.Task;
import com.quickbirdstudios.surveykit.TaskIdentifier;
import com.quickbirdstudios.surveykit.TextChoice;
import com.quickbirdstudios.surveykit.result.QuestionResult;
import com.quickbirdstudios.surveykit.result.StepResult;
import com.quickbirdstudios.surveykit.result.TaskResult;
import com.quickbirdstudios.surveykit.result.question_results.BooleanQuestionResult;
import com.quickbirdstudios.surveykit.result.question_results.MultipleChoiceQuestionResult;
import com.quickbirdstudios.surveykit.result.question_results.ScaleQuestionResult;
import com.quickbirdstudios.surveykit.result.question_results.TextQuestionResult;
import com.quickbirdstudios.surveykit.steps.CompletionStep;
import com.quickbirdstudios.surveykit.steps.InstructionStep;
import com.quickbirdstudios.surveykit.steps.QuestionStep;
import com.quickbirdstudios.surveykit.steps.Step;
import com.quickbirdstudios.surveykit.survey.SurveyView;
import com.schibstedspain.leku.LocationPickerActivity;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.content.ContextCompat;

import android.preference.PreferenceManager;
import android.provider.Settings;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import org.jetbrains.annotations.NotNull;
import org.uniglobalunion.spotlight.Constants;
import org.uniglobalunion.spotlight.R;
import org.uniglobalunion.spotlight.SpotlightApp;
import org.uniglobalunion.spotlight.model.StudyEvent;
import org.uniglobalunion.spotlight.model.StudyListing;
import org.uniglobalunion.spotlight.service.TrackingService;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import kotlin.jvm.functions.Function1;
import kotlin.jvm.functions.Function2;

import static android.app.AppOpsManager.MODE_ALLOWED;
import static android.app.AppOpsManager.OPSTR_GET_USAGE_STATS;
import static android.os.Process.myUid;
import static com.schibstedspain.leku.LocationPickerActivityKt.LATITUDE;
import static com.schibstedspain.leku.LocationPickerActivityKt.LOCATION_ADDRESS;
import static com.schibstedspain.leku.LocationPickerActivityKt.LONGITUDE;
import static com.schibstedspain.leku.LocationPickerActivityKt.ZIPCODE;
import static org.uniglobalunion.spotlight.Constants.PREF_DAYS_OF_WEEK;
import static org.uniglobalunion.spotlight.service.TrackingService.ACTION_APP_SNAPSHOT;
import static org.uniglobalunion.spotlight.service.TrackingService.logEvent;
import static org.uniglobalunion.spotlight.ui.FragmentStudyStatus.APPS_REQUEST_CODE;
import static org.uniglobalunion.spotlight.ui.FragmentStudyStatus.MAP_HOME_REQUEST_CODE;
import static org.uniglobalunion.spotlight.ui.FragmentStudyStatus.MAP_WORK_REQUEST_CODE;

public class SurveyActivity extends AppCompatActivity {

    SurveyView mSurveyView;

    SharedPreferences mPrefs;

    String survey = "onboarding";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_survey);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().hide();
        mSurveyView = findViewById(R.id.survey_view);

        mPrefs = PreferenceManager.getDefaultSharedPreferences(this);

        if (getIntent()!=null&&getIntent().hasExtra("survey")&&getIntent().getStringExtra("survey").equals("work"))
            initWorkJournalSurvey();
        else
            initOnboardingSurvey ();
    }


    private void initWorkJournalSurvey ()
    {
        survey = "work";

        ArrayList<Step> steps = new ArrayList<>();
        steps.add(new InstructionStep("",getString(R.string.work_journal_step1),getString(R.string.action_okay),false,new StepIdentifier("step1")));

        QuestionStep qs = new QuestionStep(getString(R.string.work_journal_overworked),getString(R.string.overworked_description),getString(R.string.action_next),
                new AnswerFormat.ScaleAnswerFormat(5,1,1,1f,
                        AnswerFormat.ScaleAnswerFormat.Orientation.Horizontal,"",""),
                false, new StepIdentifier("step_feel_overworked"));

        steps.add(qs);

        steps.add(new QuestionStep(getString(R.string.work_journal_underpaid),"",getString(R.string.action_next),
                new AnswerFormat.BooleanAnswerFormat(getString(R.string.yes),getString(R.string.no),
                        AnswerFormat.BooleanAnswerFormat.Result.PositiveAnswer), false, new StepIdentifier("step_worked_time_not_paid")));

        steps.add(new QuestionStep(getString(R.string.work_journal_like_to_say_more),"",getString(R.string.action_next),
                new AnswerFormat.BooleanAnswerFormat(getString(R.string.yes),getString(R.string.no),
                        AnswerFormat.BooleanAnswerFormat.Result.PositiveAnswer), false, new StepIdentifier("step_tell_more_ask")));


        steps.add(new QuestionStep(getString(R.string.work_journal_say_more),"",getString(R.string.action_next),new AnswerFormat.TextAnswerFormat(100, getString(R.string.work_journal_tell_us_more), new Function1<String, Boolean>() {
            @Override
            public Boolean invoke(String s) {
                return true;
            }
        }),true,new StepIdentifier("step_tell_more")));

        steps.add(new CompletionStep(getString(R.string.work_journal_complete),"",getString(R.string.action_okay),null, 1, false,new StepIdentifier("stepfinal")));

        NavigableOrderedTask task = new NavigableOrderedTask(steps,new TaskIdentifier("task1"));
        task.setNavigationRule(steps.get(3).getId(),new NavigationRule.ConditionalDirectionStepNavigationRule(new Function1<String, StepIdentifier>() {
            @Override
            public StepIdentifier invoke(String s) {

                if (s.startsWith(getString(R.string.response_yes))) {
                    return steps.get(4).getId();
                }
                else
                    return steps.get(5).getId();
            }
        }));

        mSurveyView.setOnSurveyFinish(new Function2()
        {

            @Override
            public Object invoke(Object o, Object o2) {

                TaskResult result = (TaskResult)o;
                FinishReason reason = (FinishReason)o2;

                if (reason == FinishReason.Completed)
                {

                    int scaleValue = 0;
                    boolean notPaid = false;
                    String tellMore = "";

                    for (StepResult stepResult : result.getResults())
                    {
                        String stepId = stepResult.getId().getId();

                        if (stepId.equals("step_worked_time_not_paid"))
                        {
                            notPaid = ((BooleanQuestionResult)stepResult.getResults().get(0)).getAnswer()== AnswerFormat.BooleanAnswerFormat.Result.PositiveAnswer;

                        }
                        else if (stepId.equals("step_feel_overworked"))
                        {
                            scaleValue = ((ScaleQuestionResult)stepResult.getResults().get(0)).getAnswer().intValue();
                        }
                        else if (stepId.equals("step_tell_more"))
                        {
                            tellMore = ((TextQuestionResult)stepResult.getResults().get(0)).getAnswer();
                        }

                    }


                    logEvent(SurveyActivity.this, StudyEvent.EVENT_TYPE_REPORT,"Overworked:"+scaleValue+",notPaid:"+notPaid+",tellMore:"+tellMore);

                }
                else if (reason == FinishReason.Discarded)
                {

                }
                else if (reason == FinishReason.Failed)
                {

                }


                finish();

                return null;
            }


        });

        SurveyTheme theme = new SurveyTheme(ContextCompat.getColor(this,R.color.app_accent),
                ContextCompat.getColor(this,R.color.new_primary),
                ContextCompat.getColor(this,R.color.app_primary_darker));

        mSurveyView.start(task, theme);
    }

    private void initOnboardingSurvey ()
    {

        ArrayList<Step> steps = new ArrayList<>();
  //      steps.add(new InstructionStep("Hello","We have a few questions about your work","Okay",false,new StepIdentifier("step1")));

        steps.add(new QuestionStep(getString(R.string.survey_question_regular_work_schedule),"",getString(R.string.action_next),
                new AnswerFormat.BooleanAnswerFormat(getString(R.string.survey_question_regular_work_schedule_yes),getString(R.string.survey_question_regular_work_schedule_no),
                        AnswerFormat.BooleanAnswerFormat.Result.PositiveAnswer), false, new StepIdentifier("step_fixed_time")));

        steps.add(new QuestionStep(getString(R.string.survey_question_regular_work_schedule_when_begin),"",getString(R.string.action_next),
                new AnswerFormat.TimeAnswerFormat(new AnswerFormat.TimeAnswerFormat.Time(8,0)), false, new StepIdentifier("step_time_start")));

        steps.add(new QuestionStep(getString(R.string.survey_question_regular_work_schedule_when_end),"",getString(R.string.action_next),
                new AnswerFormat.TimeAnswerFormat(new AnswerFormat.TimeAnswerFormat.Time(18,0)), false, new StepIdentifier("step_time_stop")));

        ArrayList<TextChoice> listChoices = new ArrayList<TextChoice>();
        listChoices.add(new TextChoice(getString(R.string.day_sunday),"0"));
        listChoices.add(new TextChoice(getString(R.string.day_monday),"1"));
        listChoices.add(new TextChoice(getString(R.string.day_tuesday),"2"));
        listChoices.add(new TextChoice(getString(R.string.day_wed),"3"));
        listChoices.add(new TextChoice(getString(R.string.day_thursday),"4"));
        listChoices.add(new TextChoice(getString(R.string.day_frday),"5"));
        listChoices.add(new TextChoice(getString(R.string.day_saturday),"6"));

        steps.add(new QuestionStep(getString(R.string.survey_question_regular_work_schedule_days_of_week),"",getString(R.string.action_next),
                new AnswerFormat.MultipleChoiceAnswerFormat(listChoices,new ArrayList<TextChoice>()), false, new StepIdentifier("step_time_days")));

        steps.add(new QuestionStep(getString(R.string.survey_question_regular_work_place),"",getString(R.string.action_next),
                new AnswerFormat.BooleanAnswerFormat(getString(R.string.survey_question_regular_work_place_yes),getString(R.string.survey_question_regular_work_place_no),
                        AnswerFormat.BooleanAnswerFormat.Result.PositiveAnswer), false, new StepIdentifier("step_fixed_location")));
        steps.add(new InstructionStep(getString(R.string.survey_question_regular_work_location),"",getString(R.string.survey_question_regular_work_location_action),false,new StepIdentifier("step_set_work_location")));
        steps.add(new InstructionStep(getString(R.string.survey_question_regular_home_location),"",getString(R.string.survey_question_regular_home_location_action),false,new StepIdentifier("step_set_home_location")));

        steps.add(new QuestionStep(getString(R.string.survey_question_work_apps),getString(R.string.survey_question_work_apps_desc),getString(R.string.action_next),
                new AnswerFormat.BooleanAnswerFormat(getString(R.string.survey_question_work_apps_yes),getString(R.string.survey_question_work_apps_no),
                        AnswerFormat.BooleanAnswerFormat.Result.PositiveAnswer), false, new StepIdentifier("step_use_apps")));

        steps.add(new InstructionStep(getString(R.string.survey_question_work_apps_permission_prompt),"",getString(R.string.survey_question_work_apps_allow_access),true,new StepIdentifier("step_use_apps_permission")));

        steps.add(new InstructionStep(getString(R.string.survey_question_work_apps_choose_apps),"",getString(R.string.survey_question_work_apps_choose_apps_prompt),true,new StepIdentifier("step_use_apps_choose")));


        steps.add(new CompletionStep(getString(R.string.survey_configure_success),"",getString(R.string.action_next),null,1,false,new StepIdentifier("stepfinal")));

        NavigableOrderedTask task = new NavigableOrderedTask(steps,new TaskIdentifier("task1"));

        task.setNavigationRule(steps.get(0).getId(),new NavigationRule.ConditionalDirectionStepNavigationRule(new Function1<String, StepIdentifier>() {
            @Override
            public StepIdentifier invoke(String s) {

                if (s.startsWith(getString(R.string.response_yes)))
                    return steps.get(1).getId();
                else
                    return steps.get(4).getId();
            }
        }));

        task.setNavigationRule(steps.get(4).getId(),new NavigationRule.ConditionalDirectionStepNavigationRule(new Function1<String, StepIdentifier>() {
            @Override
            public StepIdentifier invoke(String s) {

                if (s.startsWith(getString(R.string.response_yes))) {
                    return steps.get(5).getId();
                }
                else
                    return steps.get(7).getId();
            }
        }));

        task.setNavigationRule(steps.get(7).getId(),new NavigationRule.ConditionalDirectionStepNavigationRule(new Function1<String, StepIdentifier>() {
            @Override
            public StepIdentifier invoke(String s) {

                if (s.startsWith(getString(R.string.response_yes))) {
                    return steps.get(8).getId();
                }
                else
                    return steps.get(10).getId();
            }
        }));


        task.setNavigationRule(steps.get(5).getId(),new NavigationRule.ConditionalDirectionStepNavigationRule(new Function1<String, StepIdentifier>() {
            @Override
            public StepIdentifier invoke(String s) {

                initLocationPicker(MAP_WORK_REQUEST_CODE);
                return steps.get(6).getId();
            }
        }));

        task.setNavigationRule(steps.get(6).getId(),new NavigationRule.ConditionalDirectionStepNavigationRule(new Function1<String, StepIdentifier>() {
            @Override
            public StepIdentifier invoke(String s) {

                initLocationPicker(MAP_HOME_REQUEST_CODE);
                return steps.get(7).getId();
            }
        }));

        task.setNavigationRule(steps.get(8).getId(),new NavigationRule.ConditionalDirectionStepNavigationRule(new Function1<String, StepIdentifier>() {
            @Override
            public StepIdentifier invoke(String s) {

                enableUsageTrackingDialog();
                return steps.get(9).getId();
            }
        }));

        task.setNavigationRule(steps.get(9).getId(),new NavigationRule.ConditionalDirectionStepNavigationRule(new Function1<String, StepIdentifier>() {
            @Override
            public StepIdentifier invoke(String s) {

                chooseApps();
                return steps.get(10).getId();
            }
        }));


        mSurveyView.setOnSurveyFinish(new Function2()
        {

            @Override
            public Object invoke(Object o, Object o2) {

                TaskResult result = (TaskResult)o;
                FinishReason reason = (FinishReason)o2;

                if (reason == FinishReason.Completed)
                {

                    for (StepResult stepResult : result.getResults())
                    {
                        String stepId = stepResult.getId().getId();

                        if (stepId.equals("step_fixed_time"))
                        {

                            boolean isFixedHours = ((BooleanQuestionResult)stepResult.getResults().get(0)).getAnswer()== AnswerFormat.BooleanAnswerFormat.Result.PositiveAnswer;

                            if (isFixedHours) {

                                //get start and end time and days of the week

                                enableStudy("study1", true); //daily movement
                                enableStudy("study2", true); //work log
                                enableStudy("study3", true); //app usage
                            }
                            else
                            {
                                enableStudy("study1", true); //daily movement
                                enableStudy("study2", true); //work log
                                enableStudy("study3", false); //app usage
                                enableStudy("study5", false); //commute time
                            }
                        }
                        else if (stepId.equals("step_fixed_location"))
                        {

                            boolean isFixedLocation = ((BooleanQuestionResult)stepResult.getResults().get(0)).getAnswer()== AnswerFormat.BooleanAnswerFormat.Result.PositiveAnswer;

                            if (isFixedLocation)
                            {
                                enableStudy("study4",true); //location
                                enableStudy("study5",true); //commute time
                                enableStudy("study0",false); //environmental
                                enableStudy("study2",true); //work log

                            }
                            else
                            {
                                enableStudy("study4",true); //location
                                enableStudy("study5",false); //commute time
                                enableStudy("study1",true); //daily movement
                                enableStudy("study0",false); //environmental
                                enableStudy("study2",true); //work log

                            }


                        }
                        else if (stepId.equals("step_use_apps"))
                        {
                            boolean usesApps = ((BooleanQuestionResult)stepResult.getResults().get(0)).getAnswer()== AnswerFormat.BooleanAnswerFormat.Result.PositiveAnswer;
                            enableStudy("study3", usesApps); //app usage

                        }
                        else if (stepId.equals("step_time_days"))
                        {
                            MultipleChoiceQuestionResult mqresult = (MultipleChoiceQuestionResult)stepResult.getResults().get(0);

                            StringBuffer sb = new StringBuffer();
                            for (TextChoice tchoice: mqresult.getAnswer())
                            {
                                sb.append(tchoice.getValue()).append(",");
                            }

                            mPrefs.edit().putString(PREF_DAYS_OF_WEEK,sb.toString()).commit();

                        }
                    }

                    startActivity(new Intent(SurveyActivity.this,OnboardingSuccess.class));
                    finish();

                }
                else if (reason == FinishReason.Discarded)
                {
                    finish();
                }
                else if (reason == FinishReason.Failed)
                {
                    finish();
                }


                return null;
            }


        });

        SurveyTheme theme = new SurveyTheme(ContextCompat.getColor(this,R.color.app_accent),
                ContextCompat.getColor(this,R.color.new_primary),
                ContextCompat.getColor(this,R.color.app_primary_darker));

        mSurveyView.start(task, theme);
    }

    private void enableStudy(String studyId,boolean enabled)
    {

        String key = Constants.KEY_ACTIVITY_UPDATES_REQUESTED + "_" + studyId;

        if (!enabled)
        {
            mPrefs.edit().remove(key).commit();
        }
        else
        {
            mPrefs.edit()
                    .putBoolean(key, enabled).commit();
        }

        StudyListing sl = ((SpotlightApp)getApplication()).getStudy(studyId);
        if (sl != null) {
            sl.isEnabled = enabled;

            if (enabled)
                startStudy(studyId);
        }
    }

    /**
     * Registers for activity recognition updates using
     * Registers success and failure callbacks.
     */
    public void startStudy(String studyId) {
        Intent intent = new Intent(this, TrackingService.class);
        intent.setAction(TrackingService.ACTION_START_TRACKING);
        intent.putExtra("studyid",studyId);
        ContextCompat.startForegroundService(this,intent);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if (item.getItemId() == android.R.id.home)
        {
            finish();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void initLocationPicker (int requestCode)
    {
        Intent locationPickerIntent = new LocationPickerActivity.Builder()
                .shouldReturnOkOnBackPressed()
                .withSatelliteViewHidden()
                .withGooglePlacesEnabled()
                .withGoogleTimeZoneEnabled()
                .withVoiceSearchHidden()
                .withUnnamedRoadHidden()
                .build(this);

        startActivityForResult(locationPickerIntent, requestCode);
    }

    private boolean checkForAppsPermission(Context context) {
        AppOpsManager appOps = (AppOpsManager) context.getSystemService(Context.APP_OPS_SERVICE);
        int mode = appOps.checkOpNoThrow(OPSTR_GET_USAGE_STATS,myUid(), context.getPackageName());
        return mode == MODE_ALLOWED;
    }

    private void enableUsageTrackingDialog () {

        if (!checkForAppsPermission(this)) {

            DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    switch (which){
                        case DialogInterface.BUTTON_POSITIVE:
                            //Yes button clicked
                            startActivity(new Intent(Settings.ACTION_USAGE_ACCESS_SETTINGS));
                            break;

                        case DialogInterface.BUTTON_NEGATIVE:
                            //No button clicked
                            break;
                    }
                }
            };

            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setMessage(getString(R.string.prompt_app_usage_permission)).setPositiveButton(R.string.ok_pf, dialogClickListener)
                    .setNegativeButton(R.string.cancel_pf, dialogClickListener).show();

       }
        else
        {
            Toast.makeText(this,"Permission is enabled",Toast.LENGTH_SHORT).show();
        }

    }

    private void chooseApps ()
    {
        startActivityForResult(new Intent(this,AppManagerActivity.class),APPS_REQUEST_CODE);

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == RESULT_OK)
        {
            if (requestCode == MAP_HOME_REQUEST_CODE || requestCode == MAP_WORK_REQUEST_CODE) {
                double latitude = data.getDoubleExtra(LATITUDE, 0.0);
                double longitude = data.getDoubleExtra(LONGITUDE, 0.0);
                String address = data.getStringExtra(LOCATION_ADDRESS);
                String postalcode = data.getStringExtra(ZIPCODE);

                String geoKey = "home";
                if (requestCode == MAP_WORK_REQUEST_CODE)
                    geoKey = "work";

                SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);

                prefs
                        .edit()
                        .putFloat("geo-" + geoKey + "-lat", (float) latitude)
                        .putFloat("geo-" + geoKey + "-long", (float) longitude)
                        .putString("geo-" + geoKey + "-address", address)
                        .putString("geo-" + geoKey + "-postalcode", postalcode)
                        .apply();


                startStudy("study5");

            }

        }
    }
}
