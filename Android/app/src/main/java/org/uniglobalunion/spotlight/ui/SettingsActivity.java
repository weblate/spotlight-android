package org.uniglobalunion.spotlight.ui;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.PowerManager;
import android.provider.Settings;
import android.text.format.DateFormat;
import android.view.MenuItem;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.view.ActionMode;
import androidx.fragment.app.FragmentActivity;
import androidx.preference.CheckBoxPreference;
import androidx.preference.Preference;
import androidx.preference.PreferenceFragmentCompat;
import androidx.preference.PreferenceManager;

import com.google.android.gms.dynamic.SupportFragmentWrapper;
import com.wdullaer.materialdatetimepicker.time.TimePickerDialog;

import org.uniglobalunion.spotlight.R;
import org.uniglobalunion.spotlight.SpotlightApp;
import org.uniglobalunion.spotlight.util.UnitLocale;

import java.util.Locale;

import static org.uniglobalunion.spotlight.Constants.PREF_END_TIME;
import static org.uniglobalunion.spotlight.Constants.PREF_HIGHRES_LOCATION;
import static org.uniglobalunion.spotlight.Constants.PREF_INSIGHT_TIME;
import static org.uniglobalunion.spotlight.Constants.PREF_LIMIT_LOG_TIME;
import static org.uniglobalunion.spotlight.Constants.PREF_START_TIME;

public class SettingsActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.settings_activity);
        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.settings, new SettingsFragment())
                .commit();
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
        }
    }

    @Override
    protected void onPause() {
        super.onPause();

        ((SpotlightApp)getApplication()).setWakeupAlarm();
        ((SpotlightApp)getApplication()).updateServicePrefs();
    }

    public static class SettingsFragment extends PreferenceFragmentCompat {
        @Override
        public void onCreatePreferences(Bundle savedInstanceState, String rootKey) {
            setPreferencesFromResource(R.xml.root_preferences, rootKey);

           final SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getActivity());

            Preference pref;
            pref = findPreference(PREF_HIGHRES_LOCATION);
            pref.setOnPreferenceChangeListener((preference, newValue) -> {

                CheckBoxPreference prefCb = (CheckBoxPreference)preference;

                prefs.edit().putBoolean(PREF_HIGHRES_LOCATION,prefCb.isChecked()).commit();

                return true;
            });

            pref = findPreference("pref_disable_battery");
            pref.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
                @Override
                public boolean onPreferenceClick(Preference preference) {
                    disableBatteryOptimization();
                    return true;
                }
            });

            pref = findPreference("pref_setup_wiz");
            pref.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
                @Override
                public boolean onPreferenceClick(Preference preference) {
                    startActivity(new Intent(getActivity(), OnboardingActivity.class));
                    return true;
                }
            });

            pref = findPreference(PREF_START_TIME);
            pref.setOnPreferenceClickListener(preference -> {

                String startTime = prefs.getString(PREF_START_TIME,"8:00");
                String[] timeSplit = startTime.split(":");
                if (timeSplit.length == 1)
                {
                    timeSplit = startTime.split(",");
                    if (timeSplit.length == 1)
                    {
                        startTime = "8:00";
                        timeSplit = startTime.split(":");
                    }
                }
                int hours = Integer.parseInt(timeSplit[0]);
                int mins = Integer.parseInt(timeSplit[1]);;
                int secs = 0;

                showTimePicker(getActivity(),hours, mins, secs, new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePickerDialog view, int hourOfDay, int minute, int second) {

                        if (hourOfDay == 0 && minute == 0)
                            minute = 1;

                        prefs.edit().putString(PREF_START_TIME,hourOfDay + ":" + minute).commit();

                    }
                });
                return true;
            });

            pref = findPreference(PREF_END_TIME);
            pref.setOnPreferenceClickListener(preference -> {

                String startTime = prefs.getString(PREF_END_TIME,"18:00");
                String[] timeSplit = startTime.split(":");
                if (timeSplit.length == 1)
                {
                    timeSplit = startTime.split(",");
                    if (timeSplit.length == 1)
                    {
                        startTime = "18:00";
                        timeSplit = startTime.split(":");
                    }
                }
                int hours = Integer.parseInt(timeSplit[0]);
                int mins = Integer.parseInt(timeSplit[1]);;
                int secs = 0;

                showTimePicker(getActivity(),hours, mins, secs, (view, hourOfDay, minute, second) -> {

                    if (hourOfDay == 0 && minute == 0) {
                        hourOfDay = 11;
                        minute = 59;
                    }

                    prefs.edit().putString(PREF_END_TIME,hourOfDay + ":" + minute).commit();

                });
                return true;
            });

            pref = findPreference(PREF_INSIGHT_TIME);
            pref.setOnPreferenceClickListener(preference -> {

                String startTime = prefs.getString(PREF_INSIGHT_TIME,"8:00");
                String[] timeSplit = startTime.split(":");
                if (timeSplit.length == 1)
                {
                    timeSplit = startTime.split(",");
                    if (timeSplit.length == 1)
                    {
                        startTime = "8:00";
                        timeSplit = startTime.split(":");
                    }
                }
                int hours = Integer.parseInt(timeSplit[0]);
                int mins = Integer.parseInt(timeSplit[1]);;
                int secs = 0;

                showTimePicker(getActivity(),hours, mins, secs, (view, hourOfDay, minute, second) -> {

                    if (hourOfDay == 0 && minute == 0) {
                        hourOfDay = 11;
                        minute = 59;
                    }

                    prefs.edit().putString(PREF_INSIGHT_TIME,hourOfDay + ":" + minute).commit();

                    ((SpotlightApp)getActivity().getApplication()).setInsightAlarm();

                });
                return true;
            });


        }

        private void disableBatteryOptimization ()
        {
            if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                Intent intent = new Intent();
                String packageName = getActivity().getPackageName();
                PowerManager pm = (PowerManager) getActivity().getSystemService(POWER_SERVICE);
                if (!pm.isIgnoringBatteryOptimizations(packageName)) {
                    intent.setAction(Settings.ACTION_REQUEST_IGNORE_BATTERY_OPTIMIZATIONS);
                    intent.setData(Uri.parse("package:" + packageName));
                    startActivity(intent);
                }
                else
                {
                    Toast.makeText(getActivity(),getActivity().getString(R.string.status_power_opt_disabled),Toast.LENGTH_LONG).show();
                }
            }
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home)
        {
            finish();
            return true;
        }


        return super.onOptionsItemSelected(item);
    }

    public static void showTimePicker(FragmentActivity act, int hours, int minutes, int seconds, TimePickerDialog.OnTimeSetListener listener) {

        //if metric, use 24 hour mode
        boolean use24HourClock = DateFormat.is24HourFormat(act.getApplicationContext());
        TimePickerDialog mTimePickerDialog = TimePickerDialog.newInstance(listener, hours, minutes, seconds, use24HourClock);

        mTimePickerDialog.enableSeconds(false);

        mTimePickerDialog.show(act.getSupportFragmentManager(), "TimeDelayPickerDialog");

    }


}