/*
 * Copyright 2017 Google Inc. All Rights Reserved.
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.uniglobalunion.spotlight.ui;

import android.Manifest;
import android.app.AppOpsManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.provider.Settings;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.widget.SwitchCompat;
import androidx.cardview.widget.CardView;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.lifecycle.Observer;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.Circle;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.android.material.snackbar.Snackbar;
import com.schibstedspain.leku.LocationPickerActivity;

import org.uniglobalunion.spotlight.Constants;
import org.uniglobalunion.spotlight.R;
import org.uniglobalunion.spotlight.SpotlightApp;
import org.uniglobalunion.spotlight.model.StudyEvent;
import org.uniglobalunion.spotlight.model.StudyListing;
import org.uniglobalunion.spotlight.model.StudySummary;
import org.uniglobalunion.spotlight.service.TrackingService;
import org.uniglobalunion.spotlight.ui.charts.BarResultFragment;
import org.uniglobalunion.spotlight.util.UnitLocale;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.StringTokenizer;
import java.util.concurrent.TimeUnit;

import static android.app.AppOpsManager.MODE_ALLOWED;
import static android.app.AppOpsManager.OPSTR_GET_USAGE_STATS;
import static android.os.Process.myUid;
import static org.uniglobalunion.spotlight.Constants.PREFS_KEY_APP_ENABLED;
import static org.uniglobalunion.spotlight.service.TrackingService.ACTION_ENVIRON_SNAPSHOT;
import static org.uniglobalunion.spotlight.service.TrackingService.getRepository;

public class FragmentStudyConfig extends Fragment {

    protected static final String TAG = "MainActivity";

    private Context mContext;


    private String studyTitle = "Study";
    private String studyId = "study1";

    private StudyListing studyListing = null;

  //  private StudyViewModel mStudyViewModel;
    private FragmentAppList mFragApps;

    boolean showActivities = false;
    boolean showEnviron = false;
    boolean showApps = false;
    boolean showGeo = false;
    boolean showCommute = false;
    boolean showEvent = false;

    Spinner spinnerTime;
    SwitchCompat switchStatus;
    TextView tvTitle;

    Date todayStart, todayEnd;

    Handler mHandler = new Handler();

    GoogleMap mGoogleMap;
    boolean mapSetup = false;

    Calendar mCal;

    boolean isAttached = false;
    static ArrayList<Integer> colors = null;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        isAttached = true;
        /**
         if (context instanceof OnFragmentInteractionListener) {
         mListener = (OnFragmentInteractionListener) context;
         } else {
         throw new RuntimeException(context.toString()
         + " must implement OnFragmentInteractionListener");
         }**/
    }

    @Override
    public void onDetach() {
        super.onDetach();


        isAttached = false;

    }

    public FragmentStudyConfig() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment FragmentStudyDaily.
     */
    // TODO: Rename and change types and number of parameters
    public static FragmentStudyConfig newInstance(String param1, String param2) {
        FragmentStudyConfig fragment = new FragmentStudyConfig();
        Bundle args = new Bundle();

        fragment.setArguments(args);
        return fragment;
    }

    View view;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_study_config, container, false);

        switchStatus = view.findViewById(R.id.request_activity_updates_switch);

        tvTitle = view.findViewById(R.id.txtStatus);

        initUI();
        return view;
    }

    @SuppressWarnings("unchecked")
    @Override
    public void onCreate(Bundle data) {
        super.onCreate(data);



    }


    private void initUI () {
        studyTitle = getActivity().getIntent().getStringExtra("title");

        studyId = getActivity().getIntent().getStringExtra("id");
        studyListing = ((SpotlightApp)getActivity().getApplication()).getStudy(studyId);


        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.HOUR_OF_DAY, 0);
        cal.set(Calendar.MINUTE, 1);

        todayStart = cal.getTime();

        cal.set(Calendar.HOUR_OF_DAY, 23);
        cal.set(Calendar.MINUTE, 59);

        todayEnd = cal.getTime();



        mContext = getActivity();

        showActivities = getActivity().getIntent().getBooleanExtra("activity", false);
        showEnviron = getActivity().getIntent().getBooleanExtra("environ", false);
        showApps = getActivity().getIntent().getBooleanExtra("apps", false);
        showGeo = getActivity().getIntent().getBooleanExtra("geo", false);
        showEvent = getActivity().getIntent().getBooleanExtra("event", false);
        showCommute = getActivity().getIntent().getBooleanExtra("commute", false);

        /**
        mStudyViewModel = ViewModelProviders.of(this).get(StudyViewModel.class);
        mStudyViewModel.getStudyEvents().observe(this, studyEvents -> {
            // Update the cached copy of the words in the adapter.
            //adapter.setWords(words);
            //updateDetectedActivitiesList(studyEvents);
        });**/

        ImageView ivStart = view.findViewById(R.id.get_started_image);
        TextView tvStart = view.findViewById(R.id.get_started_title);

        if (showActivities) {
            ivStart.setImageResource(R.drawable.humaaan2);
            tvStart.setText(getString(R.string.get_started_movement));
        }
        else if (showGeo) {
            ivStart.setImageResource(R.drawable.car);
            tvStart.setText(getString(R.string.get_started_geo));
        }
        else if (showEnviron) {
            ivStart.setImageResource(R.drawable.humaaan1);
            tvStart.setText(getString(R.string.get_started_atmosphere));
        }
        else if (showApps) {
            ivStart.setImageResource(R.drawable.humaaan3);
            tvStart.setText(getString(R.string.get_started_apps));
        }
        else if (showEvent) {
            ivStart.setImageResource(R.drawable.splash);
            tvStart.setText(getString(R.string.get_started_events));
        }

        if (isStudyEnabled())
        {
            view.findViewById(R.id.view_get_started).setVisibility(View.GONE);
            view.findViewById(R.id.view_data).setVisibility(View.VISIBLE);

            if (showApps)
                enableUsageTracking ();

        }
        else
        {
            view.findViewById(R.id.view_get_started).setVisibility(View.VISIBLE);
            view.findViewById(R.id.view_data).setVisibility(View.GONE);
        }

        initCalendar(cal,0);
    }

    public Calendar getCalendar ()
    {
        return mCal;
    }

    public void changeDay(int dayChange)
    {
        mCal.add(Calendar.DAY_OF_MONTH,dayChange);
        initCalendar(mCal,0);
    }

    public void getStartedClicked (View button)
    {
        setStudyEnabled(true);
        startStudy();
        view.findViewById(R.id.view_get_started).setVisibility(View.GONE);
        view.findViewById(R.id.view_data).setVisibility(View.VISIBLE);
    }

    public void initCalendar (Calendar cal, int position)
    {

        mCal = cal;

        tvTitle.setText(SimpleDateFormat.getDateInstance(SimpleDateFormat.LONG).format(cal.getTime()));

        cal.set(Calendar.HOUR_OF_DAY, 0);
        cal.set(Calendar.MINUTE, 0);

        if (position == 0) {

            todayStart = cal.getTime();

            cal.set(Calendar.HOUR_OF_DAY, 23);
            todayEnd = cal.getTime();


        } else if (position == 1) {

            cal.add(Calendar.DAY_OF_MONTH,-7);
            todayStart = cal.getTime();

            cal.add(Calendar.DAY_OF_MONTH,7);
            cal.set(Calendar.HOUR_OF_DAY, 23);
            cal.set(Calendar.MINUTE, 59);

            todayEnd = cal.getTime();


        } else if (position == 2) {


            cal.add(Calendar.DAY_OF_MONTH,-30);
            todayStart = cal.getTime();

            cal.add(Calendar.DAY_OF_MONTH,30);
            cal.set(Calendar.HOUR_OF_DAY, 23);
            cal.set(Calendar.MINUTE, 59);

            todayEnd = cal.getTime();

        }

        initData();
    }

    private void initData () {

        mapSetup = false;

        ((LinearLayout)view.findViewById(R.id.result_chart_fragment)).removeAllViews();


        CardView cardView = view.findViewById(R.id.result_chart);
        cardView.setVisibility(showActivities ? View.VISIBLE : View.GONE);

        if (showActivities) {


                ArrayList<String> mStudyIds = new ArrayList<>();
                mStudyIds.add(StudyEvent.EVENT_TYPE_MOTION);
                mStudyIds.add(StudyEvent.EVENT_TYPE_STEPS);

            getRepository(getActivity()).getStudyEvents(mStudyIds, todayStart.getTime(), todayEnd.getTime()).observe(this, new Observer<List<StudyEvent>>() {
                    @Override
                    public void onChanged(List<StudyEvent> studyEvents) {


                        String label = "motion";
                        long[] xvals = new long[24];
                        float[] yvals = new float[24];


                        for (int i = 0; i < xvals.length; i++) {
                            xvals[i] = i;
                            yvals[i] = 0;
                        }

                        for (StudyEvent event : studyEvents) {
                            Date dateEvent = new Date(event.getTimestamp());
                            int idx = dateEvent.getHours();
                            xvals[idx] = idx;
                            yvals[idx] = Math.max(yvals[idx], Float.valueOf(event.getEventValue()));
                        }

                        long minTotal = TimeUnit.MINUTES.convert(todayEnd.getTime() - todayStart.getTime(), TimeUnit.MILLISECONDS); //total minutes in a day

                        String txtTotalTime = "0 mins";
                        if (studyEvents.size() > 1) {
                            minTotal = studyEvents.get(0).getTimestamp() - studyEvents.get(studyEvents.size() - 1).getTimestamp();
                            minTotal = TimeUnit.MINUTES.convert(minTotal, TimeUnit.MILLISECONDS);
                            txtTotalTime = minTotal + " mins";
                        }

                        int minMoved = 0;
                        long lastTs = 0;

                        int totalSteps = 0;

                        for (StudyEvent event: studyEvents)
                        {
                            long diff = lastTs - event.getTimestamp();

                            if (diff > 60000)
                                minMoved++;

                            lastTs = event.getTimestamp();

                            if (event.getStudyId().equals(StudyEvent.EVENT_TYPE_STEPS)) {


                                totalSteps += (int)Float.parseFloat(event.getEventValue());
                            }
                        }


                        /**
                        ArrayList<Integer> colors = new ArrayList<>();
                        colors.add(getResources().getColor(R.color.holo_green_light));

                        BarResultFragment fragLine = BarResultFragment.newInstance(getString(R.string.title_study_results), label, xvals, yvals, colors);

                        // Begin the transaction
                        FragmentTransaction ft = getActivity().getSupportFragmentManager().beginTransaction();
                        // Replace the contents of the container with the new fragment
                        ft.replace(R.id.result_chart_fragment, fragLine);
                        // or ft.add(R.id.your_placeholder, new FooFragment());
                        // Complete the changes added above
                        ft.commit();
                        //findViewById(R.id.detected_activities).setVisibility(showActivities ? View.VISIBLE : View.GONE);
                         **/

                        String chartInfoStr = minMoved + " " + getString(R.string.minutes_label) + "\n" + totalSteps + " steps";

                        ((TextView) view.findViewById(R.id.chart_info)).setText(chartInfoStr);

                    }
                });

        }


        view.findViewById(R.id.detected_environment).setVisibility(showEnviron ? View.VISIBLE : View.GONE);

        if (showEnviron) {
            view.findViewById(R.id.detected_environment_action_button).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (checkEnvironPermissions())
                        sendAction(ACTION_ENVIRON_SNAPSHOT);
                }
            });


            getRepository(getActivity()).getStudyEvents(StudyEvent.EVENT_TYPE_TEMPERATURE, todayStart.getTime(), todayEnd.getTime()).observe(this, new Observer<List<StudyEvent>>() {
                @Override
                public void onChanged(List<StudyEvent> studyEvents) {
                    TextView tv = ((TextView)view.findViewById(R.id.detected_environment_temp_value));

                    float value = 0;
                    for (StudyEvent event : studyEvents)
                        value += Float.valueOf(event.getEventValue());

                    if (value > 0) {
                        value = value / studyEvents.size();

                        String dispValue = (int)value+"";
                        tv.setText(dispValue + " °C");
                    } else {
                        tv.setText(getString(R.string.sensor_not_available));
                    }
                }
            });

            getRepository(getActivity()).getStudyEvents(StudyEvent.EVENT_TYPE_LIGHT, todayStart.getTime(), todayEnd.getTime()).observe(this, new Observer<List<StudyEvent>>() {
                @Override
                public void onChanged(List<StudyEvent> studyEvents) {
                    TextView tv = ((TextView) view.findViewById(R.id.detected_environment_temp_light_value));

                    float value = 0;
                    for (StudyEvent event : studyEvents)
                        value += Float.valueOf(event.getEventValue());

                    value = value / studyEvents.size();

                    if (value > 0) {
                        value = value / studyEvents.size();

                        String dispValue = (int)value+"";
                        tv.setText(dispValue + " lux");
                    } else {
                        tv.setText(getString(R.string.sensor_not_available));
                    }

                    String txtTotalTime = "0 mins";
                    if (studyEvents.size() > 1) {
                        long diffInMillies = studyEvents.get(0).getTimestamp() - studyEvents.get(studyEvents.size() - 1).getTimestamp();
                        txtTotalTime = TimeUnit.MINUTES.convert(diffInMillies, TimeUnit.MILLISECONDS) + " mins";
                    }

                }
            });
        }
        else if (showEvent)
        {
            getRepository(getActivity()).getStudyEvents(StudyEvent.EVENT_TYPE_REPORT, todayStart.getTime(), todayEnd.getTime()).observe(this, new Observer<List<StudyEvent>>() {
                @Override
                public void onChanged(List<StudyEvent> studyEvents) {

                    TextView tv = view.findViewById(R.id.detected_events_summary);

                    StringBuffer sb = new StringBuffer();

                    for (StudyEvent event : studyEvents)
                    {
                        StringTokenizer st = new StringTokenizer(event.getEventValue());

                        while (st.hasMoreTokens())
                            sb.append(st.nextToken()).append("\n");

                        sb.append("________________\n");
                    }

                    tv.setText(sb.toString());
                }
            });

        }

        getRepository(getActivity()).getStudyEvents("environ_sound_max_amplitude", todayStart.getTime(), todayEnd.getTime()).observe(this, new Observer<List<StudyEvent>>() {
            @Override
            public void onChanged(List<StudyEvent> studyEvents) {
                TextView tv = ((TextView)view.findViewById(R.id.detected_environment_temp_sound_value));

                float value = 0;
                for (StudyEvent event : studyEvents)
                    value += Float.valueOf(event.getEventValue());

                value = value / studyEvents.size();

                if (value > 0) {
                    value = value / studyEvents.size();
                    String dispValue = (int)value+"";

                    tv.setText(dispValue + " db");
                }
                else
                {
                    tv.setText (getString(R.string.sensor_not_available));
                }

                /**
                String label = "sound";
                long[] xvals = new long[studyEvents.size()];
                float[] yvals = new float[studyEvents.size()];

                int i = 0;
                for (StudyEvent event: studyEvents)
                {
                    xvals[i] = event.getTimestamp();
                    yvals[i] = Float.valueOf(event.getEventValue());
                    i++;
                }

                LineResultFragment fragLine = LineResultFragment.newInstance("Sound", label, xvals, yvals);

                // Begin the transaction
                FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
                // Replace the contents of the container with the new fragment
                ft.replace(R.id.result_chart_sound, fragLine);
                // or ft.add(R.id.your_placeholder, new FooFragment());
                // Complete the changes added above
                ft.commit();
                **/
                //findViewById(R.id.detected_activities).setVisibility(showActivities ? View.VISIBLE : View.GONE);

            }
        });



        view.findViewById(R.id.detected_activities_apps).setVisibility(showApps ? View.VISIBLE : View.GONE);

        if (showApps)
        {
            mFragApps = FragmentAppList.newInstance();

            boolean showApps = true;

            if (showApps)
            {
                getRepository(getActivity()).getStudyEvents(StudyEvent.EVENT_TYPE_APP_USAGE, todayStart.getTime(), todayEnd.getTime()).observe(this, new Observer<List<StudyEvent>>() {
                    @Override
                    public void onChanged(List<StudyEvent> studyEvents) {



                        PackageManager pm = getContext().getPackageManager();

                        String label = getString(R.string.app_usage);

                        String tordAppString = PreferenceManager.getDefaultSharedPreferences(getContext())
                                .getString(PREFS_KEY_APP_ENABLED, "");
                        StringTokenizer st = new StringTokenizer(tordAppString,"|");
                        ArrayList<String> appPkgList = new ArrayList<>();
                        while (st.hasMoreTokens())
                            appPkgList.add(st.nextToken());


                        colors = new ArrayList<>();
                        /**
                        if (colors == null || colors.size()<appPkgList.size()) {
                            colors = new ArrayList<>();
                            ColorGenerator generator = ColorGenerator.MATERIAL; // or use DEFAULT

                            int colorSize = appPkgList.size();
                            if (colorSize == 0)
                                colorSize = 1;

                            for (int i = 0; i < colorSize; i++) {
                                int color = generator.getRandomColor();

                                while(colors.contains(color))
                                    color = generator.getRandomColor();

                                colors.add(color);
                            }
                        }**/


                        long[] xvals = new long[24];
                        HashMap<String,Long> perAppTotalTime = new HashMap<>();
                        HashMap<Integer,HashMap<String,Float>> perAppPerHourTotals = new HashMap<>();

                        for (int i = 0; i < xvals.length; i++) {
                            xvals[i] = i;
                            HashMap<String,Float> perAppHour = new HashMap<>();
                            for (String pkgId : appPkgList)
                            {
                                perAppHour.put(pkgId,0f);
                            }
                            perAppPerHourTotals.put(i,perAppHour);
                        }

                        long totalTimeUsage = 0;

                        for (StudyEvent event : studyEvents) {

                            st = new StringTokenizer(event.getEventValue(), ",");
                            String appPkg = st.nextToken();

                            if (!appPkgList.contains(appPkg))
                                continue;

                            long totalTime = -1;
                            long lastTimeUsed = -1;
                            long startTime = -1;
                            long endTime = -1;

                            if (st.hasMoreTokens()) {
                                totalTime = Long.parseLong(st.nextToken());
                                lastTimeUsed = Long.parseLong(st.nextToken());
                                startTime = Long.parseLong(st.nextToken());
                                endTime = Long.parseLong(st.nextToken());
                            }
                            else
                            {
                                totalTime = 30000;
                                lastTimeUsed = event.getTimestamp();
                                startTime = event.getTimestamp()-totalTime;
                                endTime = lastTimeUsed;
                            }

                            Date dateEvent = new Date(lastTimeUsed);

                            if (dateEvent.before(todayStart))
                                continue;

                            int idx = dateEvent.getHours();
                            xvals[idx] = idx;

                            HashMap<String, Float> perAppPerHour = perAppPerHourTotals.get(idx);
                            if (perAppPerHour == null) {
                                perAppPerHour = new HashMap<>();
                                perAppPerHourTotals.put(idx,perAppPerHour);
                            }

                            if (!perAppTotalTime.containsKey(appPkg)) {
                                perAppTotalTime.put(appPkg, totalTime);
                                totalTimeUsage += totalTime;

                                //  yvals[idx]++;
                                if (!perAppPerHour.containsKey(appPkg))
                                    perAppPerHour.put(appPkg,(float)totalTime);
                                else {
                                    perAppPerHour.put(appPkg, perAppPerHour.get(appPkg)+totalTime);
                                }

                            } else {

                                perAppTotalTime.put(appPkg, perAppTotalTime.get(appPkg) + totalTime);
                                totalTimeUsage += totalTime;
                                //yvals[idx]++;
                                if (!perAppPerHour.containsKey(appPkg))
                                    perAppPerHour.put(appPkg,(float)totalTime);
                                else {
                                    perAppPerHour.put(appPkg, perAppPerHour.get(appPkg)+totalTime);
                                }

                            }


                        }




                        float[][] yVals = new float[24][];
                        String[] yLabels = new String[appPkgList.size()];

                        int i = 0;
                        for (String pkgId : appPkgList)
                        {
                            String appName = pkgId;
                            try {
                                ApplicationInfo app = pm.getApplicationInfo(pkgId, 0);
                                appName = pm.getApplicationLabel(app).toString();
                            } catch (PackageManager.NameNotFoundException e) {
                                e.printStackTrace();
                            }

                            if (perAppTotalTime.containsKey(pkgId)) {
                                long appTotalTimeMins = TimeUnit.MINUTES.convert(perAppTotalTime.get(pkgId), TimeUnit.MILLISECONDS);
                                //appName += " (" + appTotalTimeMins + getString(R.string.label_mins) + ")";

                                mFragApps.setAppLabel(pkgId,appName + "\n" + appTotalTimeMins + getString(R.string.label_mins));

                            }


                            yLabels[i++] = appName;
                            colors.add(AppColorGenerator.getColorForApp(getActivity(),pkgId));
                        }


                        for (i = 0; i < perAppPerHourTotals.size(); i++)
                        {
                            HashMap<String,Float> perAppHour = perAppPerHourTotals.get(i);
                            yVals[i] = new float[perAppHour.size()];

                            for (int n = 0; n < appPkgList.size();n++)
                            {
                                String appKey = appPkgList.get(n);
                                float perAppHourValue = perAppHour.get(appKey);
                                yVals[i][n] = perAppHourValue;
                            }
                        }



                        //BarResultFragment fragChart = BarResultFragment.newInstance(getString(R.string.title_study_results), label, xvals, yVals, yLabels, colors);


                        // Begin the transaction
                        FragmentTransaction ft = getActivity().getSupportFragmentManager().beginTransaction();

                        ft.replace(R.id.select_apps_fragment, mFragApps);

                        // Replace the contents of the container with the new fragment
                        //ft.replace(R.id.result_chart_apps_fragment, fragChart);
                        // or ft.add(R.id.your_placeholder, new FooFragment());
                        // Complete the changes added above
                        ft.commit();

                        /**
                        long minTotal = TimeUnit.MINUTES.convert(totalTimeUsage, TimeUnit.MILLISECONDS); //total minutes in a day

                        StringBuffer sb = new StringBuffer ();

                        for (String key : perAppTotalTime.keySet()) {

                            String appName = key;
                            long appTotalTime = perAppTotalTime.get(key);

                            if (appTotalTime > 0) {

                                try {
                                    appName = pm.getApplicationLabel(pm.getApplicationInfo(key, 0)).toString();
                                } catch (PackageManager.NameNotFoundException e) {
                                    e.printStackTrace();
                                }

                                long appMinTotal = TimeUnit.MINUTES.convert(appTotalTime, TimeUnit.MILLISECONDS); //total minutes in a day

                                sb.append(appName).append(":");
                                sb.append(appMinTotal);
                                sb.append(getString(R.string.label_mins));
                                sb.append("  ");
                            }

                        }

                        ((TextView) view.findViewById(R.id.result_chart_apps_info)).setText(sb.toString());
                         **/

                        //findViewById(R.id.detected_activities).setVisibility(showActivities ? View.VISIBLE : View.GONE);

                        //   String chartInfoStr = minMoved + " " + getString(R.string.minutes_label);

                        // ((TextView) view.findViewById(R.id.chart_info)).setText(chartInfoStr);

                    }
                });
            }
            else {

                //show screen time

                getRepository(getActivity()).getStudyEvents(StudyEvent.EVENT_TYPE_SCREEN, todayStart.getTime(), todayEnd.getTime()).observe(this, new Observer<List<StudyEvent>>() {
                    @Override
                    public void onChanged(List<StudyEvent> studyEvents) {


                        String label = "screen";
                        long[] xvals = new long[24];
                        float[] yvals = new float[24];


                        for (int i = 0; i < xvals.length; i++) {
                            xvals[i] = i;
                            yvals[i] = 0;
                        }

                        for (StudyEvent event : studyEvents) {
                            Date dateEvent = new Date(event.getTimestamp());
                            int idx = dateEvent.getHours();
                            xvals[idx] = idx;
                            yvals[idx] += 1;
                        }

                        long minTotal = TimeUnit.MINUTES.convert(todayEnd.getTime() - todayStart.getTime(), TimeUnit.MILLISECONDS); //total minutes in a day

                        String txtTotalTime = "0 " + getString(R.string.label_mins);
                        if (studyEvents.size() > 1) {
                            minTotal = studyEvents.get(0).getTimestamp() - studyEvents.get(studyEvents.size() - 1).getTimestamp();
                            minTotal = TimeUnit.MINUTES.convert(minTotal, TimeUnit.MILLISECONDS);
                            txtTotalTime = minTotal + ' ' + getString(R.string.label_mins);;
                        }

                        int minMoved = 0;
                        long lastTs = 0;


                        for (StudyEvent event : studyEvents) {
                            long diff = lastTs - event.getTimestamp();

                            if (diff > 60000)
                                minMoved++;

                            lastTs = event.getTimestamp();

                        }


                        float maxMinPerHour = 60f;


                        ArrayList<Integer> colors = new ArrayList<>();
                        colors.add(getResources().getColor(R.color.holo_green_light));

                        BarResultFragment fragLine = BarResultFragment.newInstance(getString(R.string.title_study_results), label, xvals, yvals, 30000f, getString(R.string.label_mins), maxMinPerHour, colors);

                        // Begin the transaction
                        FragmentTransaction ft = getActivity().getSupportFragmentManager().beginTransaction();

                        ft.replace(R.id.select_apps_fragment, mFragApps);

                        // Replace the contents of the container with the new fragment
                        ft.replace(R.id.result_chart_apps_fragment, fragLine);
                        // or ft.add(R.id.your_placeholder, new FooFragment());
                        // Complete the changes added above
                        ft.commit();
                        //findViewById(R.id.detected_activities).setVisibility(showActivities ? View.VISIBLE : View.GONE);

                        //   String chartInfoStr = minMoved + " " + getString(R.string.minutes_label);

                        // ((TextView) view.findViewById(R.id.chart_info)).setText(chartInfoStr);

                    }
                });
            }

        }

        view.findViewById(R.id.detected_location).setVisibility(showGeo ? View.VISIBLE : View.GONE);

        view.findViewById(R.id.detected_activities_walking).setVisibility((getActivity().getIntent().getBooleanExtra("track_location",true)?View.VISIBLE : View.GONE));

        boolean trackActivity = getActivity().getIntent().getBooleanExtra("track_activity",true);
        if (trackActivity) {
            view.findViewById(R.id.detected_activities_walking).setVisibility((getActivity().getIntent().getBooleanExtra("track_walking", true) ? View.VISIBLE : View.GONE));
            view.findViewById(R.id.detected_activities_still).setVisibility((getActivity().getIntent().getBooleanExtra("track_still", true) ? View.VISIBLE : View.GONE));
        }
        else
        {
            view.findViewById(R.id.detected_activities_walking).setVisibility(View.GONE);
            view.findViewById(R.id.detected_activities_still).setVisibility(View.GONE);

        }

        view.findViewById(R.id.detected_events).setVisibility(showEvent ? View.VISIBLE : View.GONE);

        if (showEvent)
        {

            view.findViewById(R.id.detected_events_action_button).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    TextView tvEvent = view.findViewById(R.id.detected_events_what);
                    Spinner spinnerEvent = view.findViewById(R.id.spinner_events);

                    TrackingService.logEvent(getActivity(),StudyEvent.EVENT_TYPE_REPORT,spinnerEvent.getSelectedItem() + ": " + tvEvent.getText());

                    tvEvent.setText("");
                    spinnerEvent.setSelection(0);

                    Snackbar.make(view.findViewById(R.id.status_list),getString(R.string.status_event_logged),Snackbar.LENGTH_SHORT).show();
                }
            });
        }

        if (showGeo)
            setupMap();

        if (showCommute) {
            TextView tvCommuteInfo = view.findViewById(R.id.commute_info);

            if (tvCommuteInfo != null)
            {
                StudySummary.getLastCommuteTime(getActivity(), this, todayStart.getTime(), todayEnd.getTime(), new StudySummary.StudySummaryListener() {
                    @Override
                    public void handleResult(String result) {
                        tvCommuteInfo.setText(result);

                    }

                    @Override
                    public void handleResult(int total) {
                        //total minutes
                        //gCommute.setValue(total);
                    }
                });
            }
        }

    }


    MapFragment mapFragment;

    private void setupMap ()
    {
        //showApps means commute tracking

     //   final SupportMapFragment mapFragment = (SupportMapFragment) getActivity().getSupportFragmentManager()
       //         .findFragmentById(R.id.map);

        if (!showCommute) {

            view.findViewById(R.id.mapbox).setVisibility(View.VISIBLE);
            view.findViewById(R.id.geocontrols).setVisibility(View.GONE);

            if (mapFragment == null) {


                mapFragment = (MapFragment) getActivity().getFragmentManager().findFragmentById(R.id.map);

                if (mapFragment != null) {

                    mapFragment.getView().getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
                        @Override
                        public void onGlobalLayout() {

                            updateMapData();

                        }
                    });


                }

            } else {
                updateMapData();
            }
        }
        else
        {
            view.findViewById(R.id.geocontrols).setVisibility(View.VISIBLE);
            view.findViewById(R.id.mapbox).setVisibility(View.GONE);

        }

    }

    private void updateMapData ()
    {
        mapFragment.getMapAsync(new OnMapReadyCallback() {
            @Override
            public void onMapReady(final GoogleMap googleMap) {

                mGoogleMap = googleMap;

                SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getActivity());

                mGoogleMap.setOnCircleClickListener(new GoogleMap.OnCircleClickListener() {
                    @Override
                    public void onCircleClick(Circle circle) {

                        long eventTime = (long)circle.getTag();

                        Date dateTime = new Date(eventTime);

                        DateFormat df = SimpleDateFormat.getTimeInstance();
                        String time = df.format(dateTime);

                        googleMap.addMarker(new MarkerOptions().position(circle.getCenter())
                                .title(time)).showInfoWindow();


                    }
                });

                mGoogleMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
                    @Override
                    public boolean onMarkerClick(Marker marker) {
                        marker.setVisible(false);
                        return false;
                    }
                });

                if (!mapSetup) {
                    if (showGeo) {
                        getRepository(getActivity()).getStudyEvents(StudyEvent.EVENT_TYPE_GEO_LOGGING, todayStart.getTime(), todayEnd.getTime()).observe(getActivity(), new Observer<List<StudyEvent>>() {
                            @Override
                            public void onChanged(List<StudyEvent> studyEvents) {

                            //    mGoogleMap.clear();

                                if (studyEvents.size() > 0) {

                                    mapSetup = true;
                                    float totalDistanceInMeters = 0;
                                    Location lastLoc = null;
                                    boolean addedPoint = false;

                                    final LatLngBounds.Builder builder = LatLngBounds.builder();
                                    PolylineOptions pLine = new PolylineOptions();
                                    pLine.color(getResources().getColor(R.color.map_circle_color));

                                    for (StudyEvent event : studyEvents) {
                                        Location location = new Location("");

                                        String[] locParts = event.getEventValue().split(",");
                                        location.setLatitude(Double.parseDouble(locParts[0]));
                                        location.setLongitude(Double.parseDouble(locParts[1]));

                                        float accuracy = Float.parseFloat(locParts[2]);

                                        if (accuracy < 20) {

                                            String geoKey = new Date(event.getTimestamp()).toString();

                                            // Add a marker in Sydney, Australia,
                                            // and move the map's camera to the same location.
                                            LatLng newLoc = new LatLng(location.getLatitude(), location.getLongitude());
                                            //  mGoogleMap.addMarker(new MarkerOptions().position(newLoc).visible(false));
                                            builder.include(newLoc);
                                            addedPoint = true;
                                            pLine.add(newLoc);

                                            if (lastLoc != null) {
                                                totalDistanceInMeters += lastLoc.distanceTo(location);
                                            }

                                            lastLoc = location;

                                            if (mGoogleMap != null) {
                                                Circle circle = mGoogleMap.addCircle(new CircleOptions()
                                                        .center(newLoc)
                                                        .radius(accuracy)
                                                        .strokeColor(getResources().getColor(R.color.map_circle_color))
                                                        .fillColor(getResources().getColor(R.color.map_circle_color)));

                                                circle.setTag(event.getTimestamp());

                                                circle.setClickable(true);
                                            }
                                        }

                                    }

                                    mGoogleMap.addPolyline(pLine);

                                    if (addedPoint) {
                                        mHandler.postDelayed(new Runnable() {
                                            public void run() {
                                                if (isAttached && view.findViewById(R.id.view_data).getVisibility() != View.GONE
                                                ) {
                                                    if (studyEvents != null && studyEvents.size() > 0) {

                                                        LatLngBounds lBounds = builder.build();

                                                        int width = getResources().getDisplayMetrics().widthPixels;
                                                        int height = getResources().getDisplayMetrics().heightPixels;
                                                        int padding = (int) (width * .32); // offset from edges of the map 12% of screen

                                                        CameraUpdate cu = CameraUpdateFactory.newLatLngBounds(lBounds, width, height, padding);

                                                        mGoogleMap.animateCamera(cu, new GoogleMap.CancelableCallback() {
                                                            @Override
                                                            public void onFinish() {

                                                                float newZoom = mGoogleMap.getCameraPosition().zoom-2f;
                                                                mGoogleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(lBounds.getCenter(), newZoom));
                                                            }

                                                            @Override
                                                            public void onCancel() {

                                                            }
                                                        });


                                                    }

                                                }

                                            }
                                        }, 1500);
                                    }


                                    String distanceStr = "";

                                    if (UnitLocale.getDefault() == UnitLocale.Metric) {

                                        if (totalDistanceInMeters < 1000)
                                            distanceStr = ((int) totalDistanceInMeters) + " " + getString(R.string.label_meters);
                                        else
                                            distanceStr = ((int) totalDistanceInMeters / 1000) + getString(R.string.label_km);
                                    } else {
                                        double miles = UnitLocale.getDefault().getMiles(totalDistanceInMeters);
                                        distanceStr = String.format("%.2f", miles) + " " + getString(R.string.label_miles);
                                    }

                                    ((TextView) view.findViewById(R.id.location_info)).setText(distanceStr);


                                }
                            }
                        });
                    }

                }
            }
        });
    }




    private void showData ()
    {

        Intent intent = new Intent(getActivity(), StudyResultsActivity.class);
        intent.putExtra("studyId",studyId);

        startActivity(intent);

    }


    private boolean checkForAppsPermission(Context context) {
        AppOpsManager appOps = (AppOpsManager) context.getSystemService(Context.APP_OPS_SERVICE);
        int mode = appOps.checkOpNoThrow(OPSTR_GET_USAGE_STATS,myUid(), context.getPackageName());
        return mode == MODE_ALLOWED;
    }

    private void enableUsageTracking () {

        if (!checkForAppsPermission(getActivity())) {

            DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    switch (which){
                        case DialogInterface.BUTTON_POSITIVE:
                            //Yes button clicked
                            startActivity(new Intent(Settings.ACTION_USAGE_ACCESS_SETTINGS));
                            break;

                        case DialogInterface.BUTTON_NEGATIVE:
                            //No button clicked
                            break;
                    }
                }
            };

            AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
            builder.setMessage(getString(R.string.prompt_app_usage_permission)).setPositiveButton(R.string.ok_pf, dialogClickListener)
                    .setNegativeButton(R.string.cancel_pf, dialogClickListener).show();

        }

    }

    @Override
    public void onResume() {
        super.onResume();

        mapSetup = false;



        TextView tvWaypoints = view.findViewById(R.id.waypoints_info);

        StringBuffer sb = new StringBuffer();
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getActivity());
        if (prefs.contains("geo-home-address"))
        {
            sb.append(getString(R.string.home)).append(": ");
            sb.append(prefs.getString("geo-home-address",""));
            sb.append("\n\n");
        }

        if (prefs.contains("geo-work-address"))
        {
            sb.append(getString(R.string.work)).append(": ");
            sb.append(prefs.getString("geo-work-address",""));
        }

        tvWaypoints.setText(sb.toString());

    }



    /**
     * Registers for activity recognition updates using
     * Registers success and failure callbacks.
     */
    public void startStudy() {
        Intent intent = new Intent(getActivity(), TrackingService.class);
        intent.setAction(TrackingService.ACTION_START_TRACKING);
        intent.putExtra("studyid",studyId);
        ContextCompat.startForegroundService(getActivity(),intent);
    }

    /**
     * Registers for activity recognition updates using
     * Registers success and failure callbacks.
     */
    public void sendAction(String action) {
        Intent intent = new Intent(getActivity(), TrackingService.class);
        intent.setAction(action);
        intent.putExtra("studyid",studyId);
        ContextCompat.startForegroundService(getActivity(),intent);
    }


    /**
     * Removes activity recognition updates using
     * failure callbacks.
     */
    public void stopStudy () {

        Intent intent = new Intent(getActivity(),TrackingService.class);
        intent.setAction(TrackingService.ACTION_STOP_TRACKING);
        intent.putExtra("studyid",studyId);
        getActivity().startService(intent);

        setStudyEnabled(false);
        view.findViewById(R.id.view_get_started).setVisibility(View.VISIBLE);

    }

    public void resetStudy ()
    {

    }



    /**
     * Retrieves the boolean from SharedPreferences that tracks whether we are requesting activity
     * updates.
     */
    private boolean isStudyEnabled() {
        return PreferenceManager.getDefaultSharedPreferences(getActivity())
                .getBoolean(Constants.KEY_ACTIVITY_UPDATES_REQUESTED + "_" + studyId, false);
    }

    private void setStudyEnabled(boolean enabled) {
        PreferenceManager.getDefaultSharedPreferences(getActivity()).edit()
                .putBoolean(Constants.KEY_ACTIVITY_UPDATES_REQUESTED + "_" + studyId, enabled).commit();

        studyListing.isEnabled = enabled;

        if (studyListing.isEnabled)
        {
            if (studyListing.trackApps)
                enableUsageTracking();
            else if (studyListing.trackGeo)
                checkLocationPermissions();
        }

    }


    private boolean checkForPermission(Context context) {
        AppOpsManager appOps = (AppOpsManager) context.getSystemService(Context.APP_OPS_SERVICE);
        int mode = appOps.checkOpNoThrow(OPSTR_GET_USAGE_STATS,myUid(), context.getPackageName());
        return mode == MODE_ALLOWED;
    }

    public void viewDataClicked (View view)
    {
        showData();
    }

    public void setHomeClicked (View view)
    {
      //  Intent intent = new Intent(this, TrackingService.class);
      //  intent.setAction(TrackingService.ACTION_SET_GEO_HOME);
       // startService(intent);
        initLocationPicker (MAP_HOME_REQUEST_CODE);
    }


    public void setWorkClicked (View view)
    {
        //Intent intent = new Intent(this, TrackingService.class);
        //intent.setAction(TrackingService.ACTION_SET_GEO_WORK);
        //startService(intent);
        initLocationPicker (MAP_WORK_REQUEST_CODE);
    }

    public void chooseAppsClicked (View view)
    {
        startActivityForResult(new Intent(getActivity(),AppManagerActivity.class),APPS_REQUEST_CODE);
    }

    public static final int MAP_HOME_REQUEST_CODE = 1001;
    public static final int MAP_WORK_REQUEST_CODE = MAP_HOME_REQUEST_CODE+1;
    public static final int APPS_REQUEST_CODE = MAP_WORK_REQUEST_CODE+1;

    private void initLocationPicker (int requestCode)
    {
        Intent locationPickerIntent = new LocationPickerActivity.Builder()
                .withDefaultLocaleSearchZone()
                .shouldReturnOkOnBackPressed()
                .withSatelliteViewHidden()
                .withGooglePlacesEnabled()
                .withGoogleTimeZoneEnabled()
                .withVoiceSearchHidden()
                .withUnnamedRoadHidden()
                .build(getActivity());

        getActivity().startActivityForResult(locationPickerIntent, requestCode);
    }


    private int REQUEST_RECORD_AUDIO = 1234;

    private boolean checkEnvironPermissions ()
    {
        if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.RECORD_AUDIO) != PackageManager.PERMISSION_GRANTED) {

            ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.RECORD_AUDIO},
                    REQUEST_RECORD_AUDIO);

            return false;
        }
        else
            return true;
    }

    private boolean checkLocationPermissions ()
    {
        if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

            ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.ACCESS_FINE_LOCATION,Manifest.permission.ACCESS_COARSE_LOCATION},
                    REQUEST_RECORD_AUDIO);

            return false;
        }
        else
            return true;
    }

    public FragmentAppList getAppListFragment ()
    {
        return mFragApps;
    }

    public void sendAction(String studyId, String action) {
        Intent intent = new Intent(getActivity(), TrackingService.class);
        intent.setAction(action);
        intent.putExtra("studyid",studyId);
        ContextCompat.startForegroundService(getActivity(),intent);
    }
}