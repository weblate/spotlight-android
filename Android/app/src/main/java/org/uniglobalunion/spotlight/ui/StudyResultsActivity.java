package org.uniglobalunion.spotlight.ui;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;


import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.cardview.widget.CardView;
import androidx.core.content.FileProvider;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.Observer;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.snackbar.Snackbar;

import org.uniglobalunion.spotlight.R;
import org.uniglobalunion.spotlight.SpotlightApp;
import org.uniglobalunion.spotlight.model.StudyEvent;
import org.uniglobalunion.spotlight.model.StudyListing;
import org.uniglobalunion.spotlight.model.StudyRepository;
import org.uniglobalunion.spotlight.model.StudyViewModel;
import org.uniglobalunion.spotlight.service.TrackingService;

import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintStream;
import java.io.PrintWriter;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import java.util.StringTokenizer;
import java.util.TimeZone;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import ru.slybeaver.slycalendarview.SlyCalendarDialog;

import static java.security.AccessController.getContext;
import static org.uniglobalunion.spotlight.service.TrackingService.getRepository;


public class StudyResultsActivity extends AppCompatActivity {

    private RecyclerView rv;

    private StudyListing mStudyListing;
    private ArrayList<String> mStudyIdFilter;

    Date dateStart, dateEnd;
    TextView tvTimeFilter;

    Handler mHandler = new Handler();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_study_results);
        Toolbar toolbar = findViewById(R.id.toolbar);

        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        tvTimeFilter = findViewById(R.id.timefilter);


        initDates();

        String studyId = getIntent().getStringExtra("studyId");

        mStudyIdFilter = new ArrayList<>();

        if (!TextUtils.isEmpty(studyId)) {
            mStudyListing = ((SpotlightApp) getApplication()).getStudy(studyId);

            if (mStudyListing != null)
            {
                if (mStudyListing.trackActivities) {
                    mStudyIdFilter.add(StudyEvent.EVENT_TYPE_MOTION);
                    mStudyIdFilter.add(StudyEvent.EVENT_TYPE_STEPS);
                }

                if (mStudyListing.trackEnviron)
                {
                    mStudyIdFilter.add(StudyEvent.EVENT_TYPE_TEMPERATURE);
                    mStudyIdFilter.add(StudyEvent.EVENT_TYPE_LIGHT);
                    mStudyIdFilter.add(StudyEvent.EVENT_TYPE_HUMIDITY);
                    mStudyIdFilter.add(StudyEvent.EVENT_TYPE_SOUND);
                }


                if (mStudyListing.trackCommute)
                {
                    mStudyIdFilter.add(StudyEvent.EVENT_TYPE_COMMUTE);
                    mStudyIdFilter.add(StudyEvent.EVENT_TYPE_GEO_FENCE);
                }
                else if (mStudyListing.trackGeo)
                {
                    mStudyIdFilter.add(StudyEvent.EVENT_TYPE_GEO_LOGGING);
                }

                if (mStudyListing.trackEvent)
                {
                    mStudyIdFilter.add(StudyEvent.EVENT_TYPE_REPORT);
                }

                if (mStudyListing.trackApps)
                {
                    mStudyIdFilter.add(StudyEvent.EVENT_TYPE_SCREEN);
                    mStudyIdFilter.add(StudyEvent.EVENT_TYPE_APP_USAGE);
                    mStudyIdFilter.add(StudyEvent.EVENT_TYPE_DEVICE_POWER);
                }

                if (mStudyListing.trackTime)
                {
                    mStudyIdFilter.add(StudyEvent.EVENT_TYPE_TIME);
                }

            }
        }

        rv = (RecyclerView)findViewById(R.id.rv);
        LinearLayoutManager llm = new LinearLayoutManager(this);
        rv.setLayoutManager(llm);

        initializeData();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.study_result_menu, menu);
        return true;
    }



    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if (item.getItemId() == android.R.id.home)
        {
            finish();
            return true;
        }
        else if (item.getItemId() == R.id.menu_share) {

            shareText();


        }
        else if (item.getItemId() == R.id.menu_delete) {
            deleteAllData();
        }
        else if (item.getItemId() == R.id.menu_delete_selected) {
            deleteSelectedData();
        }

        return super.onOptionsItemSelected(item);
    }

    public void changeDates (View view)
    {
        showDatePicker();
    }

    private void showDatePicker ()
    {
        new SlyCalendarDialog()
                .setSingle(false)
                .setCallback(new SlyCalendarDialog.Callback() {
                    @Override
                    public void onCancelled() {

                    }

                    @Override
                    public void onDataSelected(Calendar firstDate, Calendar secondDate, int hours, int minutes) {

                        firstDate.set(Calendar.HOUR_OF_DAY, 0);
                        firstDate.set(Calendar.MINUTE, 0);
                        dateStart = firstDate.getTime();

                        if (secondDate == null)
                            secondDate = firstDate;

                        secondDate.set(Calendar.HOUR_OF_DAY, 23);
                        secondDate.set(Calendar.MINUTE, 59);
                        dateEnd = secondDate.getTime();

                        isDataLoaded = false;
                        initializeData();
                    }
                })
                .show(getSupportFragmentManager(), "TAG_SLYCALENDAR");
    }

    private void shareText ()
    {
        if (mStudyEvents != null) {

            TimeZone tz = TimeZone.getTimeZone("UTC");
            DateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm'Z'"); // Quoted "Z" to indicate UTC, no timezone offset
            df.setTimeZone(tz);

            File fileReportDir = new File(getFilesDir(), "reports");
            String reportFileName = "report" + new Date().getTime() + ".csv";
            File fileReport = new File(fileReportDir, reportFileName + ".zip");
            fileReport.getParentFile().mkdirs();

            try {

                ZipOutputStream out = new ZipOutputStream(new FileOutputStream(fileReport));
                ZipEntry e = new ZipEntry(reportFileName);
                out.putNextEntry(e);

                for (StudyEvent event : mStudyEvents) {

                    StringBuffer sb = new StringBuffer();

                    sb.append(event.getId()).append(",");
                    sb.append("\"").append(event.getStudyId()).append("\",");
                    sb.append("\"").append(df.format(new Date(event.getTimestamp()))).append("\",");

                    String formatValue = event.getEventValue();

                    if (event.getStudyId().equals(StudyEvent.EVENT_TYPE_GEO_LOGGING)) {
                        StringTokenizer st = new StringTokenizer(formatValue, ",");

                        sb.append("\"").append(st.nextToken()).append(",").append(st.nextToken()).append("\"").append(",");
                        while (st.hasMoreTokens()) {
                            sb.append(st.nextToken());

                            if (st.hasMoreTokens())
                                sb.append(",");
                        }

                        sb.append("\n");
                    } else if (event.getStudyId().equals(StudyEvent.EVENT_TYPE_APP_USAGE)) {
                        StringTokenizer st = new StringTokenizer(formatValue, ",");

                        sb.append("\"").append(st.nextToken()).append(",").append(st.nextToken()).append("\"").append(",");
                        while (st.hasMoreTokens()) {
                            sb.append(st.nextToken());

                            if (st.hasMoreTokens())
                                sb.append(",");
                        }

                        sb.append("\n");
                    } else {
                        sb.append("\"").append(formatValue).append("\"\n");
                    }

                    out.write(sb.toString().getBytes());
                }

                out.closeEntry();
                out.close();

                Uri contentUri = FileProvider.getUriForFile(StudyResultsActivity.this, TrackingService.FILE_AUTHORITY, fileReport);

                Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
                sharingIntent.setFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                sharingIntent.setType("application/zip");
                sharingIntent.putExtra(Intent.EXTRA_STREAM, contentUri);

                // validate that the device can open your File!
                PackageManager pm = getPackageManager();
                if (sharingIntent.resolveActivity(pm) != null) {
                    startActivity(Intent.createChooser(sharingIntent, getString(R.string.share_results_prompt)));
                }


            } catch (Exception ioe) {
                ioe.printStackTrace();
            }
        }


    }

    class StudyResult {
        String name;
        String desc;
        int studyIconId;

        StudyResult(String name, String desc, int studyIconId) {
            this.name = name;
            this.desc = desc;
            this.studyIconId = studyIconId;
        }
    }

    public class RVAdapter extends RecyclerView.Adapter<RVAdapter.StudyResultViewHolder>{

        private List<StudyResult> mResults;

        public RVAdapter (List<StudyResult> results)
        {
            mResults = results;
        }

        public class StudyResultViewHolder extends RecyclerView.ViewHolder {
            CardView cv;
            TextView studyName;
            TextView studyDesc;
           // ImageView studyIcon;

            StudyResultViewHolder(View itemView) {
                super(itemView);
                cv = itemView.findViewById(R.id.cv);
                studyName = itemView.findViewById(R.id.study_name);
                studyDesc = itemView.findViewById(R.id.study_desc);
              //  studyIcon = itemView.findViewById(R.id.study_icon);
            }

        }

        @Override
        public int getItemCount() {
            return mResults.size();
        }

        @Override
        public StudyResultViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
            View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.study_card_result, viewGroup, false);
            final StudyResultViewHolder pvh = new StudyResultViewHolder(v);
            pvh.cv.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                }
            });

            return pvh;
        }

        @Override
        public void onBindViewHolder(StudyResultViewHolder studyResultViewHolder, int i) {
            studyResultViewHolder.studyName.setText(mResults.get(i).name);
            studyResultViewHolder.studyDesc.setText(mResults.get(i).desc);
           // studyResultViewHolder.studyIcon.setImageResource(mResults.get(i).studyIconId);
        }

        @Override
        public void onAttachedToRecyclerView(RecyclerView recyclerView) {
            super.onAttachedToRecyclerView(recyclerView);
        }

    }

    private void deleteAllData () {

        AlertDialog.Builder builder = new AlertDialog.Builder(this);

        builder.setTitle(getString(R.string.title_delete_all_data));
        builder.setMessage(getString(R.string.prompt_are_you_sure));

        builder.setPositiveButton(getString(R.string.answer_yes), new DialogInterface.OnClickListener() {

            public void onClick(DialogInterface dialog, int which) {
                // Do nothing but close the dialog

                StudyViewModel svm = new StudyViewModel(getApplication());
                svm.deleteStudyEvents(mStudyIdFilter,-1,-1);

                dialog.dismiss();

                refresh();
            }
        });

        builder.setNegativeButton(getString(R.string.answer_no), new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {

                // Do nothing
                dialog.dismiss();
            }
        });

        AlertDialog alert = builder.create();
        alert.show();


    }

    private void deleteSelectedData () {

        AlertDialog.Builder builder = new AlertDialog.Builder(this);

        builder.setTitle(getString(R.string.title_delete_selected));
        builder.setMessage(getString(R.string.prompt_are_you_sure));

        builder.setPositiveButton(getString(R.string.answer_yes), new DialogInterface.OnClickListener() {

            public void onClick(DialogInterface dialog, int which) {
                // Do nothing but close the dialog

                StudyViewModel svm = new StudyViewModel(getApplication());
                svm.deleteStudyEvents(mStudyIdFilter, dateStart.getTime(), dateEnd.getTime());



                dialog.dismiss();

                refresh();
            }
        });

        builder.setNegativeButton(getString(R.string.answer_no), new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {

                // Do nothing
                dialog.dismiss();
            }
        });

        AlertDialog alert = builder.create();
        alert.show();

    }

    boolean isDataLoaded = false;
    List<StudyEvent> mStudyEvents = null;

    public void refresh ()
    {
        mHandler.postDelayed(new Runnable() {

            public void run ()
            {
                initializeData();
            }
        },2000);
    }

    private void initializeData(){

        DateFormat sdf = SimpleDateFormat.getDateInstance(SimpleDateFormat.SHORT);
        StringBuffer sb = new StringBuffer();
        sb.append(sdf.format(dateStart));
        sb.append(" - ");
        sb.append(sdf.format(dateEnd));

        tvTimeFilter.setText(sb.toString());

        //first clear old studies
        List<StudyResult> studies = new ArrayList<>();
        RVAdapter adapter = new RVAdapter(studies);
        rv.setAdapter(adapter);

        final Snackbar sbar = Snackbar.make(rv,getString(R.string.status_loading),Snackbar.LENGTH_INDEFINITE);

        sbar.show();

        StudyViewModel svm = new StudyViewModel(getApplication());
        svm.getStudyEvents(mStudyIdFilter, dateStart.getTime(), dateEnd.getTime()).observe(this, new Observer<List<StudyEvent>>() {
            @Override
            public void onChanged(List<StudyEvent> studyEvents) {

                if (isDataLoaded)
                    return;

                isDataLoaded = true;

                mStudyEvents = studyEvents;

                List<StudyResult> studies = new ArrayList<>();

                for (StudyEvent event : mStudyEvents) {
                    StudyResult result = new StudyResult(event.getStudyId() + ": " + event.getEventValue(),new Date(event.getTimestamp())+"", R.drawable.study_sitting);
                    studies.add(result);
                }

                RVAdapter adapter = new RVAdapter(studies);
                rv.setAdapter(adapter);

                sbar.dismiss();
            }
        });



    }

    private static StudyRepository sRepository;

    private static synchronized StudyRepository getRepository (Context context)
    {
        if (sRepository == null)
            sRepository = new StudyRepository(context);

        return sRepository;
    }

    private long baseTime = -1;

    private void initDates ()
    {


        Calendar cal = Calendar.getInstance();

        baseTime = getIntent().getLongExtra("studyDate",-1L);

        if (baseTime != -1L)
            cal.setTimeInMillis(baseTime);
        else
            baseTime = cal.getTimeInMillis();

        cal.set(Calendar.HOUR_OF_DAY, 0);
        cal.set(Calendar.MINUTE, 0);

        dateStart = cal.getTime();

        cal.set(Calendar.HOUR_OF_DAY, 23);
        cal.set(Calendar.MINUTE, 59);

        dateEnd = cal.getTime();


    }


}
