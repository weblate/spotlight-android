/*
 * Copyright 2017 Google Inc. All Rights Reserved.
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.uniglobalunion.spotlight.ui;

import static android.app.AppOpsManager.MODE_ALLOWED;
import static android.app.AppOpsManager.OPSTR_GET_USAGE_STATS;
import static android.os.Process.myUid;
import static com.schibstedspain.leku.LocationPickerActivityKt.LATITUDE;
import static com.schibstedspain.leku.LocationPickerActivityKt.LOCATION_ADDRESS;
import static com.schibstedspain.leku.LocationPickerActivityKt.LONGITUDE;
import static com.schibstedspain.leku.LocationPickerActivityKt.ZIPCODE;
import static org.uniglobalunion.spotlight.service.TrackingService.ACTION_APP_SNAPSHOT;
import static org.uniglobalunion.spotlight.ui.FragmentStudyStatus.APPS_REQUEST_CODE;
import static org.uniglobalunion.spotlight.ui.FragmentStudyStatus.MAP_HOME_REQUEST_CODE;
import static org.uniglobalunion.spotlight.ui.FragmentStudyStatus.MAP_WORK_REQUEST_CODE;

import android.app.AppOpsManager;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.viewpager.widget.ViewPager;

import com.schibstedspain.leku.LocationPickerActivity;

import org.uniglobalunion.spotlight.CleanInsightsManager;
import org.uniglobalunion.spotlight.R;
import org.uniglobalunion.spotlight.SpotlightApp;
import org.uniglobalunion.spotlight.model.StudyListing;
import org.uniglobalunion.spotlight.service.TrackingService;
import org.uniglobalunion.spotlight.util.SharingUtils;

import java.util.Calendar;

public class StudyExplorerActivity extends AppCompatActivity {

    protected static final String TAG = "MainActivity";

    private String studyTitle = "Study";
    private String studyId = "study1";

    private StudyListing studyListing = null;


    FragmentStudyStatus mFragDay;
    FragmentStudyTotal mFragTotal;
    ViewPager mPager;
 //   TabLayout tabLayout;

    @SuppressWarnings("unchecked")
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.study_explore_activity);

        if (getIntent().hasExtra("title")) {
            studyTitle = getIntent().getStringExtra("title");
            setTitle(studyTitle);
        }

        if (getIntent().hasExtra("id")) {
            studyId = getIntent().getStringExtra("id");
            studyListing = ((SpotlightApp) getApplication()).getStudy(studyId);
        }

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        initUI();


    }

    public void setCalendar (Calendar calendar)
    {
       // tabLayout.selectTab(tabLayout.getTabAt(0));
        mPager.setCurrentItem(0);
        mFragDay.initCalendar(calendar,0);
    }

    private void initUI () {

        mPager = findViewById(R.id.view_pager);
        MyPagerAdapter myPagerAdapter = new MyPagerAdapter(getSupportFragmentManager());
        mPager.setAdapter(myPagerAdapter);

       // tabLayout = findViewById(R.id.tablayout);
       // tabLayout.setupWithViewPager(mPager,true);
    }


    private void showMeasurementUI () {

        ((SpotlightApp)getApplication()).getCleanInsights().showSurvey(this,studyTitle);

    }

    class MyPagerAdapter extends FragmentPagerAdapter
    {

        public MyPagerAdapter (FragmentManager m)
        {
            super(m);
        }

        @NonNull
        @Override
        public Fragment getItem(int position) {
            if (position == 0) {
                mFragDay = FragmentStudyStatus.newInstance("foo" + position, "bar");
                return mFragDay;
            }
            else if (position == 1) {
                mFragTotal = FragmentStudyTotal.newInstance("foo" + position, "bar");
                return mFragTotal;
            }
            else
                return null;
        }

        @Override
        public int getCount() {
            return 2;
        }


        // Returns the page title for the top indicator
        @Override
        public CharSequence getPageTitle(int position) {
            if (position == 0)
                return getString(R.string.title_day);
            else
                return getString(R.string.title_total);
        }
    }




    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.study_status_menu, menu);
        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if (item.getItemId() == android.R.id.home)
        {
            finish();
            return true;
        }
        else if (item.getItemId() == R.id.menu_report) {

            showData();

        }
        else if (item.getItemId() == R.id.menu_stop_study) {
            mFragDay.stopStudy();
        }
        else if (item.getItemId() == R.id.menu_share) {

            View viewP = mFragDay.getPrimaryView();
            Bitmap vBitmap = SharingUtils.loadBitmapFromView(viewP);
            SharingUtils.shareBitmapWithText(this,vBitmap,"", false);

        }

        return super.onOptionsItemSelected(item);
    }

    private void showData ()
    {

        Intent intent = new Intent(this, StudyResultsActivity.class);
        intent.putExtra("studyId",studyId);

        if (mFragDay != null && mFragDay.getCalendar() != null)
            intent.putExtra("studyDate",mFragDay.getCalendar().getTime().getTime());

        startActivity(intent);

    }

    public void getStartedClicked (View view)
    {
        mFragDay.getStartedClicked(view);
    }

    private boolean checkForPermission(Context context) {
        AppOpsManager appOps = (AppOpsManager) context.getSystemService(Context.APP_OPS_SERVICE);
        int mode = appOps.checkOpNoThrow(OPSTR_GET_USAGE_STATS,myUid(), context.getPackageName());
        return mode == MODE_ALLOWED;
    }

    public void onDateClicked (View view)
    {
        mPager.setCurrentItem(1);
    }

    public void goBackDayClicked (View view)
    {
        mFragDay.changeDay(-1);
    }

    public void goForwardDayClicked (View view)
    {
        mFragDay.changeDay(1);
    }

    public void setHomeClicked (View view)
    {
        //  Intent intent = new Intent(this, TrackingService.class);
        //  intent.setAction(TrackingService.ACTION_SET_GEO_HOME);
        // startService(intent);
        initLocationPicker (MAP_HOME_REQUEST_CODE);
    }

    public void onFeedbackClicked (View view)
    {
        showMeasurementUI();
    }

    public void setWorkClicked (View view)
    {
        //Intent intent = new Intent(this, TrackingService.class);
        //intent.setAction(TrackingService.ACTION_SET_GEO_WORK);
        //startService(intent);
        initLocationPicker (MAP_WORK_REQUEST_CODE);
    }

    public void chooseAppsClicked (View view)
    {
        startActivityForResult(new Intent(this,AppManagerActivity.class),APPS_REQUEST_CODE);
    }

    private void initLocationPicker (int requestCode)
    {
        Intent locationPickerIntent = new LocationPickerActivity.Builder()
                .withDefaultLocaleSearchZone()
                .shouldReturnOkOnBackPressed()
                .withSatelliteViewHidden()
                .withGooglePlacesEnabled()
                .withGoogleTimeZoneEnabled()
                .withVoiceSearchHidden()
                .withUnnamedRoadHidden()
                .build(this);

        startActivityForResult(locationPickerIntent, requestCode);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == RESULT_OK)
        {
            if (requestCode == MAP_HOME_REQUEST_CODE || requestCode == MAP_WORK_REQUEST_CODE) {
                double latitude = data.getDoubleExtra(LATITUDE, 0.0);
                double longitude = data.getDoubleExtra(LONGITUDE, 0.0);
                String address = data.getStringExtra(LOCATION_ADDRESS);
                String postalcode = data.getStringExtra(ZIPCODE);

                String geoKey = "home";
                if (requestCode == MAP_WORK_REQUEST_CODE)
                    geoKey = "work";

                SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);

                prefs
                        .edit()
                        .putFloat("geo-" + geoKey + "-lat", (float) latitude)
                        .putFloat("geo-" + geoKey + "-long", (float) longitude)
                        .putString("geo-" + geoKey + "-address", address)
                        .putString("geo-" + geoKey + "-postalcode", postalcode)
                        .apply();

                //restart geo tracking
                startStudy("study5");

            }
            else if (requestCode == APPS_REQUEST_CODE)
            {
                if (mFragDay != null && mFragDay.getAppListFragment() != null) {
                    mFragDay.getAppListFragment().reloadApps(this);
                    mFragDay.initCalendar(mFragDay.getCalendar(),0);
                    sendAction("apps",ACTION_APP_SNAPSHOT);

                }
            }

        }
    }

    /**
     * Registers for activity recognition updates using
     * Registers success and failure callbacks.
     */
    public void startStudy(String studyId) {
        Intent intent = new Intent(this, TrackingService.class);
        intent.setAction(TrackingService.ACTION_START_TRACKING);
        intent.putExtra("studyid",studyId);
        ContextCompat.startForegroundService(this,intent);
    }


    private int REQUEST_RECORD_AUDIO = 1234;


    /**
     * Registers for activity recognition updates using
     * Registers success and failure callbacks.
     */
    public void sendAction(String studyId, String action) {
        Intent intent = new Intent(this, TrackingService.class);
        intent.setAction(action);
        intent.putExtra("studyid",studyId);
        ContextCompat.startForegroundService(this,intent);
    }
}