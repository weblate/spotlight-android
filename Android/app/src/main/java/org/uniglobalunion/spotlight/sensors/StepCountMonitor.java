package org.uniglobalunion.spotlight.sensors;

import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.util.Log;

import androidx.appcompat.app.AppCompatActivity;

import org.uniglobalunion.spotlight.model.StudyEvent;
import org.uniglobalunion.spotlight.service.TrackingService;

/**
 * Created by n8fr8 on 3/10/17.
 */
public class StepCountMonitor implements SensorEventListener {

    // For shake motion detection.
    private SensorManager sensorMgr;

    /**
     * Accelerometer sensor
     */

    /**
     * Last update of the accelerometer
     */
    private long lastUpdate = -1;

    /**
     * Text showing accelerometer values
     */
    private int maxAlertPeriod = 30;
    private int remainingAlertPeriod = 0;
    private boolean alert = false;

    private TrackingService mContext;

    public StepCountMonitor(TrackingService context) {

        mContext = context;

        sensorMgr = (SensorManager) context.getSystemService(AppCompatActivity.SENSOR_SERVICE);
        Sensor sensor = sensorMgr.getDefaultSensor(Sensor.TYPE_STEP_DETECTOR);

        if (sensor == null) {
            Log.i("StepCounter", "Warning: no step sensor");
        } else {

            int delay = SensorManager.SENSOR_DELAY_UI;
            if (mContext.doHighAccuracy())
                delay = SensorManager.SENSOR_DELAY_FASTEST;


            boolean success = sensorMgr.registerListener(this, sensor, delay);

            Log.i("StepCounter", "step sensor registration: " + success);

        }


    }

    public void onAccuracyChanged(Sensor sensor, int accuracy) {
        // Safe not to implement

    }

    public void onSensorChanged(SensorEvent event) {
        long curTime = System.currentTimeMillis();
        // only allow one update every 100ms.
        if (event.sensor.getType() == Sensor.TYPE_STEP_DETECTOR) {
            lastUpdate = curTime;
            float[] values = event.values.clone();
            int currentSteps = (int)values[0];
            mContext.logEvent(StudyEvent.EVENT_TYPE_STEPS,currentSteps+"");

        }
    }

    public void stop(Context context) {
        sensorMgr.unregisterListener(this);
    }



}
