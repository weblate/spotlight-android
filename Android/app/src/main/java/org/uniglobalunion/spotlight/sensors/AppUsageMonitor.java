package org.uniglobalunion.spotlight.sensors;

import android.app.usage.UsageEvents;
import android.app.usage.UsageStats;
import android.app.usage.UsageStatsManager;
import android.content.Context;

import org.uniglobalunion.spotlight.model.StudyEvent;
import org.uniglobalunion.spotlight.service.TrackingService;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class AppUsageMonitor {

    public static void trackUsage (Context context, ArrayList<String> appPkgIds, long beginTime, long endTime)
    {
        UsageStatsManager mUsageStatsManager = (UsageStatsManager)context
                .getSystemService(Context.USAGE_STATS_SERVICE);

        List<UsageStats> listStats = mUsageStatsManager.queryUsageStats(UsageStatsManager.INTERVAL_BEST,beginTime,endTime);

        for (UsageStats stats :  listStats)
        {
            if (appPkgIds.contains(stats.getPackageName())) {
                StringBuffer sb = new StringBuffer();
                sb.append(stats.getPackageName()).append(',');
                sb.append(stats.getTotalTimeInForeground()).append(',');
                sb.append(stats.getLastTimeUsed()).append(',');
                sb.append(stats.getFirstTimeStamp()).append(',');
                sb.append(stats.getLastTimeStamp());

                TrackingService.logEvent(context,StudyEvent.EVENT_TYPE_APP_USAGE,sb.toString());


            }
        }
    }
}
