package org.uniglobalunion.spotlight.ui;


import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.github.appintro.AppIntro2;

import org.uniglobalunion.spotlight.R;

public class OnboardingActivity extends AppIntro2 {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setTitle("");
        getSupportActionBar().hide();
        setWizardMode(true);

        String title = null;
        String description = null;

        addSlide(CustomIntroSlide.newInstance(R.layout.slide_custom_bigtext,getString(R.string.ob1_title),getString(R.string.ob1_desc)));
        addSlide(CustomIntroSlide.newInstance(R.layout.slide_custom_bigtext,getString(R.string.ob2_title),getString(R.string.ob2_desc)));


        /**
        title = getString(R.string.ob1_title);
        description = getString(R.string.ob1_desc);
        addSlide(AppIntroFragment.newInstance(title, null, description,  null, R.drawable.onboarding1, getResources().getColor(R.color.leku_white),getResources().getColor(R.color.app_text),getResources().getColor(R.color.app_text_light)));

        title = getString(R.string.ob2_title);
        description = getString(R.string.ob2_desc);
        addSlide(AppIntroFragment.newInstance(title, null, description,  null, R.drawable.onboarding2, getResources().getColor(R.color.leku_white),getResources().getColor(R.color.app_text),getResources().getColor(R.color.app_text_light)));
        **/
    }

    @Override
    protected void onDonePressed(@Nullable Fragment currentFragment) {
        super.onDonePressed(currentFragment);

        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
        prefs.edit().putBoolean("onboarding",true).commit();

        // Do something when users tap on Done button.
        finish();

        startActivity(new Intent(this,SurveyActivity.class));

        //startActivity(new Intent(this, LoadingActivity.class));
    }


}
