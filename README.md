# Spotlight (aka "Weclock") App

Learn more at https://weclock.it

How can we ensure workers and their organizations can capture and benefit from the signals and data they are generating throughout their day? From drivers, to warehouse workers, farm workers, house cleaners, and phone support techs... we can help!

Learn more on the project wiki: https://gitlab.com/guardianproject/spotlight/wikis/home

Based on our work with CameraV, ProofMode and our "Just Pay Phone" worker app: [View the Slides](https://docs.google.com/presentation/d/15J4tnkDy_NKf4rIkphIzmDFvgAgu_PvgwoOB_Z2SZDo/edit?usp=sharing)

# Sponsors

* UNI Global Union
* ACV Puls

# Artwork

Work Group by Gerald Wildmoser from the Noun Project
https://thenounproject.com/term/working-together/126707/

people group by Maxim Kulikov from the Noun Project
https://thenounproject.com/maxim221/collection/teamwork/?i=1161017

Location by Numero Uno from the Noun Project
https://thenounproject.com/search/?q=location&i=1570755

worker appreciation by Binpodo from the Noun Project
https://thenounproject.com/search/?q=worker&i=2050610

Spotlight by chabib ali machbubi from the Noun Project
https://thenounproject.com/search/?q=spotlight&i=1316100
